//
//  UITextField+ShoppingListStyle.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/22/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "UITextField+ShoppingListStyle.h"

@implementation UITextField(ShoppingListStyle)

-(void)shoppingListGreenFullRoundedCorner
{
    self.layer.cornerRadius=BUTTON_CORNER_RADIUS;
    self.layer.borderWidth=1;
    self.layer.borderColor=APP_THEME_LIGHT_GREEN.CGColor;
    self.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:16];
    self.borderStyle = UITextBorderStyleNone;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
    self.rightView = paddingView;
    self.rightViewMode = UITextFieldViewModeAlways;
}
-(void)shoppingListGreenRoundedCorner
{
    self.layer.cornerRadius=8;
    self.layer.borderWidth=1;
    self.layer.borderColor=APP_THEME_LIGHT_GREEN.CGColor;
    self.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:16];
    self.borderStyle = UITextBorderStyleNone;
    
}
-(void)showErrorTextField:(BOOL)isShow
{
    if(isShow)
        self.layer.borderColor=APP_DARK_RED_CLOUR.CGColor;
    else
        self.layer.borderColor=APP_THEME_LIGHT_GREEN.CGColor;
}
@end

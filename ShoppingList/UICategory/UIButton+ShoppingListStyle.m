//
//  UIButton+ShoppingListStyle.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/5/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "UIButton+ShoppingListStyle.h"

@implementation UIButton(TonicStyle)

-(void)shoppingTransparentRoundedCorner:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    self.layer.cornerRadius=BUTTON_CORNER_RADIUS;
    self.layer.borderWidth=1;
    self.layer.borderColor=[UIColor whiteColor].CGColor;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backgroundColor = [UIColor clearColor];
    [self.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:BUTTON_FONT_SIZE]];
    self.userInteractionEnabled = YES;
}
-(void)shoppingClearRoundedCorner:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    self.layer.cornerRadius=BUTTON_CORNER_RADIUS;
    self.layer.borderWidth=1;
    self.layer.borderColor = APP_THEME_LIGHT_GREEN.CGColor;
    [self setTitleColor:APP_THEME_LIGHT_GREEN forState:UIControlStateNormal];
    self.backgroundColor = [UIColor clearColor];
    
    [self.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:BUTTON_FONT_SIZE]];
    self.userInteractionEnabled = YES;
}
-(void)shoppingGreenRoundedCorner:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    self.layer.cornerRadius=BUTTON_CORNER_RADIUS;
    self.layer.borderWidth=0;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backgroundColor = APP_THEME_LIGHT_GREEN;
    [self.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:BUTTON_FONT_SIZE]];
    self.userInteractionEnabled = YES;
}
-(void)shoppingOrangeRoundedCorner:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    self.layer.cornerRadius=BUTTON_CORNER_RADIUS;
    self.layer.borderWidth=0;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:@"orangeButtonBg"] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:BUTTON_FONT_SIZE]];
    self.layer.masksToBounds= YES;
    self.userInteractionEnabled = YES;
}
-(void)shoppingGreenSquare:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    self.layer.cornerRadius=0;
    self.layer.borderWidth=0;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backgroundColor = APP_THEME_LIGHT_GREEN;
    [self.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:BUTTON_FONT_SIZE]];
    self.userInteractionEnabled = YES;
}

-(void)shoppingOrangeSquare:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    self.layer.cornerRadius=0;
    self.layer.borderWidth=0;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:@"orangeButtonBg"] forState:UIControlStateNormal]; 
    [self.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:BUTTON_FONT_SIZE]];
    self.userInteractionEnabled = YES;
}
-(void)shoppingClearGrayBorder:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    self.layer.cornerRadius=6;
    self.layer.borderWidth=1;
    self.layer.borderColor = [UIColor darkGrayColor].CGColor;
    [self setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:BUTTON_FONT_SIZE]];
    self.userInteractionEnabled = YES;
}
-(void)shoppingGrayRoundedCorner:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    self.layer.cornerRadius=BUTTON_CORNER_RADIUS;
    self.layer.borderWidth=0;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backgroundColor = [UIColor grayColor];
    [self.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:BUTTON_FONT_SIZE]];
    self.userInteractionEnabled = YES;
}
-(void)shoppingTransparentDisableRoundedCorner:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    self.layer.cornerRadius=BUTTON_CORNER_RADIUS;
    self.layer.borderWidth=1;
    self.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.backgroundColor = [UIColor clearColor];
    [self.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:BUTTON_FONT_SIZE]];
    self.userInteractionEnabled= NO;
}
-(void)shoppingDisableRoundedCorner:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    self.layer.cornerRadius=BUTTON_CORNER_RADIUS;
    self.layer.borderWidth=1;
    self.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backgroundColor = [UIColor lightGrayColor];
    [self.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:BUTTON_FONT_SIZE]];
    self.userInteractionEnabled= NO;
}
-(void)setBadge:(NSString *)value
{
    UILabel *badgeLabel;
    for (id view in self.subviews) {
        if([view isKindOfClass:[UILabel class]])
        {
            UILabel *lblView = (UILabel *)view;
            if(lblView.tag == 101)
            {
                badgeLabel = lblView;
                break;
            }
        }
    }
    if(badgeLabel == nil){
        badgeLabel = [[UILabel alloc] init];
        badgeLabel.tag = 101;
        badgeLabel.textColor = [UIColor whiteColor];
        badgeLabel.backgroundColor = APP_THEME_LIGHT_GREEN;
        badgeLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0];
        [badgeLabel sizeToFit];
        badgeLabel.textAlignment = NSTextAlignmentCenter;
        [badgeLabel setFrame:CGRectMake(self.frame.size.width - 10,-7, 26, 26)];
        badgeLabel.layer.borderWidth = 2.0f;
        badgeLabel.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
        badgeLabel.layer.cornerRadius = 13;
        badgeLabel.layer.masksToBounds = YES;
        [self addSubview:badgeLabel];
    }
    
    if([value isEqualToString:@"0"])
    {
        [badgeLabel removeFromSuperview];
    }else{
        badgeLabel.text = value;
    }
}
@end

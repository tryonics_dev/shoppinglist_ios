//
//  UILable+ShoppingListStyle.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/22/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel(ShoppingListStyle)

-(void)shoppingTitleStyle:(NSString *)title;
-(void)shoppingSubTitleStyle:(NSString *)title;
-(void)shoppingTextLableStyle:(NSString *)title;
-(void)shoppingTextLableGerayStyle:(NSString *)title;
-(void)shoppingTextItalicGerayStyle:(NSString *)title;
-(void)shoppingTextErrorStyle:(NSString *)title;
-(void)shoppingListProfileNameStyle:(NSString *)title;
-(void)shoppingListProfilePhoneNumberStyle:(NSString *)title;
@end

NS_ASSUME_NONNULL_END

//
//  UIButton+ShoppingListStyle.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/5/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton(ShoppingListStyle)

-(void)shoppingTransparentRoundedCorner:(NSString *)title;
-(void)shoppingGreenRoundedCorner:(NSString *)title;
-(void)shoppingOrangeRoundedCorner:(NSString *)title;
-(void)shoppingClearRoundedCorner:(NSString *)title;
-(void)shoppingGreenSquare:(NSString *)title;
-(void)shoppingOrangeSquare:(NSString *)title;
-(void)shoppingClearGrayBorder:(NSString *)title;
-(void)shoppingGrayRoundedCorner:(NSString *)title;

-(void)shoppingDisableRoundedCorner:(NSString *)title;
-(void)shoppingTransparentDisableRoundedCorner:(NSString *)title;

-(void)setBadge:(NSString *)value;
@end

NS_ASSUME_NONNULL_END

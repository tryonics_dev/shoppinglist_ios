//
//  UILable+ShoppingListStyle.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/22/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "UILable+ShoppingListStyle.h"

@implementation UILabel(ShoppingListStyle)

-(void)shoppingTitleStyle:(NSString *)title
{
    self.textColor = [UIColor scrollViewTexturedBackgroundColor];
    if(IS_IPHONE_5)
        [self setFont:[UIFont fontWithName:@"SFCompactText-Bold" size:24]];
    else
        [self setFont:[UIFont fontWithName:@"SFCompactText-Bold" size:30]];
    self.text = title;
    self.numberOfLines = 0;
}
-(void)shoppingSubTitleStyle:(NSString *)title
{
    self.textColor = [UIColor scrollViewTexturedBackgroundColor];
    [self setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:16]];
    self.text = title;
    self.numberOfLines = 0;
}
-(void)shoppingTextLableStyle:(NSString *)title
{
    self.textColor = APP_THEME_LIGHT_GREEN;
    [self setFont:[UIFont fontWithName:@"SFUIDisplay-Regular" size:16]];
    self.text = title;
    self.numberOfLines = 0;
}
-(void)shoppingTextLableGerayStyle:(NSString *)title
{
    self.textColor =  [UIColor scrollViewTexturedBackgroundColor];
    [self setFont:[UIFont fontWithName:@"SFUIDisplay-Regular" size:16]];
    self.text = title;
    self.numberOfLines = 0;
}
-(void)shoppingTextItalicGerayStyle:(NSString *)title
{
    self.textColor =  [UIColor scrollViewTexturedBackgroundColor];
    [self setFont:[UIFont fontWithName:@"SFUIText-Italic" size:12]];
    self.text = title;
    self.numberOfLines = 0;
}
-(void)shoppingTextErrorStyle:(NSString *)title
{
    self.textColor =  APP_DARK_RED_CLOUR;
    [self setFont:[UIFont fontWithName:@"SFUIText-Italic" size:12]];
    self.text = title;
    self.numberOfLines = 0;
}
-(void)shoppingListProfileNameStyle:(NSString *)title
{
    self.textColor = [UIColor darkGrayColor];
    [self setFont:[UIFont fontWithName:@"SFCompactText-Bold" size:22]];
    self.text = title;
    self.numberOfLines = 0;
}
-(void)shoppingListProfilePhoneNumberStyle:(NSString *)title
{
    self.textColor = [UIColor darkGrayColor];
    [self setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:16]];
    self.text = title;
    self.numberOfLines = 0;
}
@end

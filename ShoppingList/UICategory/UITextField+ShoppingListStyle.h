//
//  UITextField+ShoppingListStyle.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/22/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField(ShoppingListStyle) 

-(void)shoppingListGreenFullRoundedCorner;
-(void)shoppingListGreenRoundedCorner;
-(void)showErrorTextField:(BOOL)isShow;
@end

NS_ASSUME_NONNULL_END

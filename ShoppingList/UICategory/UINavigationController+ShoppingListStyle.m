//
//  UINavigationController+ShoppingListStyle.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/6/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "UINavigationController+ShoppingListStyle.h"
#import "UINavigationBar+Addition.h"

@implementation UINavigationController(ShoppingListStyle)

-(void)initShoppingListStyle
{
    [self.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self.navigationBar setTranslucent:NO];
    self.navigationBar.tintColor = APP_THEME_LIGHT_GREEN;
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Geeza Pro" size:24.0],NSFontAttributeName,[UIColor darkGrayColor],NSForegroundColorAttributeName, nil];
    
    self.navigationBar.titleTextAttributes = size;
    [self.navigationBar hideBottomHairline];
    
}

-(void)setLeftTitleWithString:(NSString *)title navigationItem:(UINavigationItem *)navigationItem
{
    UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-40, 40)];
    [lable setText:title];
    [lable setTextAlignment:NSTextAlignmentLeft];
    [lable setTextColor:[UIColor scrollViewTexturedBackgroundColor]];
    [lable setBackgroundColor: [UIColor clearColor]];
    if(IS_IPHONE_5)
         [lable setFont:[UIFont fontWithName:@"SFCompactText-Bold" size:24]];
    else
        [lable setFont:[UIFont fontWithName:@"SFCompactText-Bold" size:28]];
    lable.adjustsFontSizeToFitWidth= true;
    [lable minimumScaleFactor];
    lable.center =CGPointMake(SCREEN_WIDTH/2, 20);
    navigationItem.titleView = lable;
    
//    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popController)];
//    gestureRecognizer.numberOfTapsRequired = 1;
//    [lable.v addGestureRecognizer:gestureRecognizer];
}

//-(void)popController
//{
//    [self popViewControllerAnimated:YES];
//}
@end

//
//  UINavigationController+ShoppingListStyle.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/6/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UINavigationController(ShoppingListStyle)

-(void)initShoppingListStyle;
-(void)setLeftTitleWithString:(NSString *)title navigationItem:(UINavigationItem *)navigationItem;
@end

NS_ASSUME_NONNULL_END

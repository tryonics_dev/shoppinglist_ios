//
//  AppDelegate.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "AppDelegate.h"
#import "CustomerModel.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize colourBackground,tabBarController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [IQKeyboardManager sharedManager].enable=true;
    
    UIGraphicsBeginImageContext(self.window.frame.size);
    [[UIImage imageNamed:@"background"] drawInRect:self.window.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    colourBackground =  [UIColor colorWithPatternImage:image];
    
        if (@available(iOS 13.0, *)) {
            UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
            statusBar.backgroundColor = colourBackground;
            [[UIApplication sharedApplication].keyWindow addSubview:statusBar];
        } else {
            // Fallback on earlier versions
            UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
            if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                statusBar.backgroundColor = colourBackground;
            }
        }
    
   
    [GMSPlacesClient provideAPIKey:@"AIzaSyBmog2aGAuw_KN5ko8OCYTKEycYaJqh_uw"];
    [GMSServices provideAPIKey:@"AIzaSyBmog2aGAuw_KN5ko8OCYTKEycYaJqh_uw"];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    [self configureFireBase:application];
   
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    return YES;
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [FIRMessaging messaging].APNSToken = deviceToken;
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"User %@",userInfo);
    NSString *srtMessage = userInfo[@"aps"][@"alert"][@"body"];
    [AGPushNoteView showWithNotificationMessage:srtMessage];
    [[NSNotificationCenter defaultCenter] postNotificationName:ORDER_STATUS_UPDATED_NOTIFICATION object:self];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Userdefine methods

-(void)loadMainTab
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"MainTabController"];
    [self.tabBarController.tabBar.items[2] setBadgeColor:APP_THEME_LIGHT_GREEN];
    if([[CartManager sharedManager]cartItemsCount]>0)
        [self.tabBarController.tabBar.items[2] setBadgeValue:[NSString stringWithFormat:@"%li",[[CartManager sharedManager]cartItemsCount]]];
    else
        [self.tabBarController.tabBar.items[2] setBadgeValue:nil];
    
    NSMutableArray *controllers = [[NSMutableArray alloc]initWithArray:self.tabBarController.viewControllers];
    
     (self.tabBarController.tabBar.items)[0].titlePositionAdjustment=UIOffsetMake(0.0, -5.0);
    (self.tabBarController.tabBar.items)[1].titlePositionAdjustment=UIOffsetMake(0.0, -5.0);
    (self.tabBarController.tabBar.items)[2].titlePositionAdjustment=UIOffsetMake(0.0, -5.0);
    (self.tabBarController.tabBar.items)[3].titlePositionAdjustment=UIOffsetMake(0.0, -5.0);
   
    CustomerModel *customerModel = [CustomerModel  getSavedCustomerObject];
    if(customerModel == nil)
    {
         UINavigationController *nav_vc4=[[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"WelcomeViewController"]];
        controllers[4] = nav_vc4;
        
        (self.tabBarController).viewControllers = controllers;
    }
    else
    {
        [self getDeviceToken];
    }
    (self.tabBarController.tabBar.items)[4].titlePositionAdjustment=UIOffsetMake(0.0, -5.0);
    
    [UIView transitionWithView:self.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{self.window.rootViewController=self->tabBarController; }
                    completion:nil];
}

-(void)loadLanguageSelectionController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [UIView transitionWithView:self.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{self.window.rootViewController=[storyboard instantiateViewControllerWithIdentifier:@"LanguageSelectionViewController"]; }
                    completion:nil];
}
-(void)loadProfileTab
{
     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     NSMutableArray *controllers = [[NSMutableArray alloc]initWithArray:self.tabBarController.viewControllers];
    controllers[4] = [storyboard instantiateViewControllerWithIdentifier:@"ProfileNavigationController"];
    (self.tabBarController).viewControllers = controllers;
    (self.tabBarController.tabBar.items)[4].titlePositionAdjustment=UIOffsetMake(0.0, -5.0);
    [self getDeviceToken];
}
-(void)customerLogout
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NSMutableArray *controllers = [[NSMutableArray alloc]initWithArray:self.tabBarController.viewControllers];
    UINavigationController *nav_vc4=[[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"WelcomeViewController"]];
    controllers[4] = nav_vc4;
    
    (self.tabBarController).viewControllers = controllers;
    (self.tabBarController.tabBar.items)[4].titlePositionAdjustment=UIOffsetMake(0.0, -5.0);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:LOGGED_USER_KEY];
    [defaults synchronize];
    
    [[CartManager sharedManager] setDeliveryAddress:nil];
    [[CartManager sharedManager] setBillingAddress:nil];
    
    
}

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    if (@available(iOS 13.0, *)) {
               UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
               statusBar.backgroundColor = color;
               [[UIApplication sharedApplication].keyWindow addSubview:statusBar];
           } else {
               // Fallback on earlier versions
               UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
               if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                   statusBar.backgroundColor = color;
               }
           }
}
-(void)configureFireBase:(UIApplication *)application
{
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    }
//    } else {
//        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
//        UIUserNotificationType allNotificationTypes =
//        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
//        UIUserNotificationSettings *settings =
//        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
//        [application registerUserNotificationSettings:settings];
//    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

-(void)getDeviceToken
{
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                        NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error fetching remote instance ID: %@", error);
        } else {
            NSLog(@"Remote instance ID token: %@", result.token);
            [self registerDevideTokenWithAppServer:result.token];
        }
    }];
}
-(void)registerDevideTokenWithAppServer:(NSString *)token
{
    [[NetworkManager sharedManager] registerDeviceToken:token phoneNumber:[CustomerModel  getSavedCustomerObject].telephone success:^(id  _Nonnull responseData) {
          BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success)
        {
             NSLog(@"Token registration success with server");
        }
        else
        {
             NSLog(@"Token registration failed with server");
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
          NSLog(@"Token registration failed with server");
    }];
}



#pragma mark - location manager delegates

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Location error %@",error.localizedDescription);
}

#pragma mark - FIRMessagingDelegate

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
   
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}


@end

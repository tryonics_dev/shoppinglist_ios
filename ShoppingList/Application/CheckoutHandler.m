//
//  CheckoutHandler.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/1/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "CheckoutHandler.h"

@implementation CheckoutHandler
@synthesize delegate;

- (instancetype)initWithDelegate:(id)dele {
    if ((self = [super init])) {
        // put initialization code here
        self.delegate = dele;
        networkManager = [NetworkManager sharedManager];
    }
    return self;
}

-(void)startOrderOverview
{
    [self.delegate orderOverviewStatus:@"Initiating payment address"];
    [networkManager getPaymentAddres:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setPaymentAddress];
            });
        }else
        {
            [self.delegate isOrderOverviewSuccess:false message:@"Initiating payment address failed."];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [self.delegate isOrderOverviewSuccess:false message:@"Initiating payment address failed."];
    }];
}

-(void)setPaymentAddress
{
    __weak typeof(self) weakSelf = self;
    [self.delegate orderOverviewStatus:@"Processing payment address"];
    [networkManager setPaymentAddres:self.paymentAddressId landmark:(NSString *)self.billingAddressLandMark latitude:(NSString *)self.billingdAddressLatitude longitude:(NSString *)self.billingAddressLongitude success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf getPaymentMethods];
            });
        }else
        {
           [weakSelf.delegate isOrderOverviewSuccess:false message:@"Processing payment address failed."];
          
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
         [weakSelf.delegate isOrderOverviewSuccess:false message:@"Processing payment address failed."];
    }];
}
-(void)getPaymentMethods
{
    [self.delegate orderOverviewStatus:@"Initiating payment methods"];
    [networkManager getPaymentMethods:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self getShippingAddress];
            });
        }else
        {
            [self->delegate isOrderOverviewSuccess:false message:[responseData objectForKey:@"error"][0]];
            
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
         [self.delegate isOrderOverviewSuccess:false message:@"Initiating payment methods failed."];
    }];
}
-(void)getShippingAddress
{
    [self.delegate orderOverviewStatus:@"Initiating shipping address"];
    [networkManager getShippingAddres:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setShippingAddress];
            });
        }else
        {
            [self.delegate isOrderOverviewSuccess:false message:@"Initiating shipping address failed."];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [self.delegate isOrderOverviewSuccess:false message:@"Initiating shipping address failed."];
    }];
}
-(void)setShippingAddress
{
    __weak typeof(self) weakSelf = self;
    [self.delegate orderOverviewStatus:@"Processing shipping address"];
    [networkManager setShippingAddres:self.shippingAddressId landmark:(NSString *)self.shippingAddressLandMark latitude:(NSString *)self.shippingdAddressLatitude longitude:(NSString *)self.shippingAddressLongitude success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf getShippingMethod];
            });
        }else
        {
            [weakSelf.delegate isOrderOverviewSuccess:false message:@"Processing shipping address failed."];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [weakSelf.delegate isOrderOverviewSuccess:false message:@"Processing shipping address failed."];
    }];
}
-(void)getShippingMethod
{
    __weak typeof(self) weakSelf = self;
    [self.delegate orderOverviewStatus:@"Initiating delivery methods"];
    [networkManager getShippingMethods:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf setShippingMethods];
            });
        }else
        {
            [weakSelf.delegate isOrderOverviewSuccess:false message:@"Initiating delivery methods failed."];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [weakSelf.delegate isOrderOverviewSuccess:false message:@"Initiating delivery methods failed."];
    }];
}
-(void)setShippingMethods
{
    __weak typeof(self) weakSelf = self;
    [self.delegate orderOverviewStatus:@"Processing delivery methods"];
    [networkManager setShippingMethod:self.shippingMethod success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf setPaymentMethods];
            });
        }else
        {
            [weakSelf.delegate isOrderOverviewSuccess:false message:@"Processing delivery methods failed."];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [weakSelf.delegate isOrderOverviewSuccess:false message:@"Processing delivery methods failed."];
    }];
}
-(void)setPaymentMethods
{
    __weak typeof(self) weakSelf = self;
    [self.delegate orderOverviewStatus:@"Processing payment methods"];
    [networkManager setPaymentMethod:self.paymentMethod success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf getConfirmOrderOverview];
            });
        }else
        {
            [weakSelf.delegate isOrderOverviewSuccess:false message:@"Processing payment methods failed."];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
         [weakSelf.delegate isOrderOverviewSuccess:false message:@"Processing payment methods failed."];
    }];
}
-(void)getConfirmOrderOverview
{
    __weak typeof(self) weakSelf = self;
    [self.delegate orderOverviewStatus:@"Processing your order..."];
    [networkManager getOrderOverView:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            [weakSelf.delegate didOrderOverViewConfirm:[NSString stringWithFormat:@"%@",responseData[@"data"][@"order_id"]]];
            [weakSelf.delegate isOrderOverviewSuccess:true message:@"Processing your order success."];
        }else
        {
            [weakSelf.delegate isOrderOverviewSuccess:false message:@"Processing your order failed."];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [weakSelf.delegate isOrderOverviewSuccess:false message:@"Processing your order failed."];
    }];
}

-(void)confirmOrder
{
     [self.delegate orderOverviewStatus:@"Finalizing your order..."];
    [networkManager confirmOrder:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            [self.delegate isOrderConfirmSuccess:true message:@"Your order has been placed."];
        }else
        {
            [self.delegate isOrderConfirmSuccess:false message:[responseData objectForKey:@"error"][0]];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [self.delegate isOrderConfirmSuccess:false message:@"Order confirmation failed."];
    }];
}
@end

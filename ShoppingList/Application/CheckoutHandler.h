//
//  CheckoutHandler.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/1/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CheckoutHandlerDelegate <NSObject>
@required
- (void)isOrderOverviewSuccess:(BOOL)isOverviewSuccess message:(NSString *_Nullable)message;
- (void)isOrderConfirmSuccess:(BOOL)isConfirmSuccess message:(NSString *_Nullable)message;
-(void)didOrderOverViewConfirm:(NSString *_Nonnull)orderId;
@optional
- (void)orderOverviewStatus:(NSString *_Nullable)statusMessage;
@end

NS_ASSUME_NONNULL_BEGIN

@interface CheckoutHandler : NSObject
{
    NetworkManager *networkManager;
}
@property(nonatomic,strong)id<CheckoutHandlerDelegate> delegate;
@property(nonatomic)int paymentAddressId,shippingAddressId;
@property(nonatomic,strong)NSString *shippingMethod,*paymentMethod,*billingAddressLandMark,*billingdAddressLatitude,*billingAddressLongitude,*shippingAddressLandMark,*shippingdAddressLatitude,*shippingAddressLongitude;

- (instancetype)initWithDelegate:(id)dele;
-(void)startOrderOverview;
-(void)confirmOrder;
@end

NS_ASSUME_NONNULL_END

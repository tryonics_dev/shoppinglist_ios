//
//  HandleFeedback.m
//  Tonic
//
//  Created by Mohamed Roshan on 11/16/17.
//  Copyright © 2017 Mohamed Roshan. All rights reserved.
//

#import "HandleFeedback.h"

@implementation HandleFeedback
@synthesize viewController,feedbackController;

-(instancetype)initWithView:(UINavigationController *)controller delegate:(id)delegate
{
    self=[super init];
    if(self)
    {
        viewController=controller;
        feedbackController=[[FeedbackViewController alloc] initWithNibName:@"FeedbackViewController" bundle:nil];
        feedbackController.delegate = delegate;
    }
    return self;
}
-(void)showFeedBackController:(NSInteger )orderId isReturn:(BOOL)isReturn
{
    feedbackController.orderId = orderId;
    feedbackController.isReturn = isReturn;
    (feedbackController.view).frame = [UIScreen mainScreen].bounds;
    [viewController addChildViewController:feedbackController];
    [viewController.view addSubview:feedbackController.view];
    (feedbackController.view).center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    if ([viewController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        viewController.interactivePopGestureRecognizer.enabled = NO;
    }
   
}


@end

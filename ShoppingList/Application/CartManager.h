//
//  CartManager.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/14/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CartModel.h"
#import "CustomerModel.h"
#import "CheckOutViewController.h"
#import "AddressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CartManager : NSObject
{
    NetworkManager *networkManager;
}
@property (strong,nonatomic) CartModel *cartModel;
@property (nonatomic) NSInteger cartItemsCount;
@property (nonatomic)BOOL isCheckOutClicked;
@property (nonatomic,strong)AddressModel *billingAddressModel,*deliveryAddressModel;

+ (id)sharedManager;

-(void)addItemToCart:(NSString *)product_id quantity:(NSString *)quantity;
typedef void(^getCartCompletion)(bool success , CartModel *cartModel);
-(void)getCart:(getCartCompletion)completionBlock;
typedef void(^updateCartItemCompletion)(bool success);
-(void)updateCartItem:(NSString *)key quantity:(NSString *)quantity completionBlock:(updateCartItemCompletion)completionBlock;
typedef void(^deleteCartItemCompletion)(bool success);
-(void)deleteCartItem:(NSString *)key completionBlock:(deleteCartItemCompletion)completionBlock;
-(void)checkOutValidate:(UIViewController *)cartViewController;

-(void)setDeliveryAddress:(nullable )deliveryAddressModel;
-(void)setBillingAddress:(nullable )billingAddressModel;
@end

NS_ASSUME_NONNULL_END

//
//  AppDelegate.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQKeyboardManager.h"
#import <CoreLocation/CoreLocation.h>
@import GooglePlaces;
@import Firebase;
@import GoogleMaps;

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,FIRMessagingDelegate,UNUserNotificationCenterDelegate>
{
    CLLocationManager *locationManager;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) UIColor *colourBackground;
@property (weak,nonatomic)UITabBarController *tabBarController;


-(void)loadMainTab;
-(void)loadLanguageSelectionController;
- (void)setStatusBarBackgroundColor:(UIColor *)color;
-(void)loadProfileTab;
-(void)customerLogout;
-(void)getDeviceToken;

@end


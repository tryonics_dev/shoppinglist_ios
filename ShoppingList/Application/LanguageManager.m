//
//  LanguageManager.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/6/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "LanguageManager.h"

static LanguageManager *sharedManager = nil;

@implementation LanguageManager

+ (LanguageManager*)sharedManager {
    
    static dispatch_once_t once;
    dispatch_once(&once, ^
                  {
                      sharedManager = [[LanguageManager alloc] init];
                  });
    return sharedManager;
}

- (id)init {
    if ((self = [super init])) {
        
        [self updateLanguage];
    }
    return self;
}
-(void)updateLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *selectedLanguage = [defaults objectForKey:SELECTED_LANGUAGE_KEY];
    
    NSString *path;
    if([selectedLanguage isEqualToString:LANGUAGE_CODE_SINHALA])
        path = [[NSBundle mainBundle] pathForResource:@"si-LK" ofType:@"lproj"];
    else if([selectedLanguage isEqualToString:LANGUAGE_CODE_TAMIL])
        path = [[NSBundle mainBundle] pathForResource:@"ta" ofType:@"lproj"];
    else
        path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
    
    languageBundle = [NSBundle bundleWithPath:path];
}
-(NSString*) languageStringForKey:(NSString*) key
{
    NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
    return str;
}
@end

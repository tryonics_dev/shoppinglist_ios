//
//  LanguageManager.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/6/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LanguageManager : NSObject
{
    NSBundle* languageBundle;
}

+ (id)sharedManager;

-(void)updateLanguage;
-(NSString*) languageStringForKey:(NSString*) key;
@end

NS_ASSUME_NONNULL_END

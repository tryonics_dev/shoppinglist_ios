//
//  HandleFeedback.h
//  Tonic
//
//  Created by Mohamed Roshan on 11/16/17.
//  Copyright © 2017 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FeedbackViewController.h"
#import "HandleFeedback.h"

@interface HandleFeedback : NSObject
{

}
@property(nonatomic,strong)UINavigationController *viewController;
@property(nonatomic,strong)FeedbackViewController *feedbackController;

-(instancetype)initWithView:(UINavigationController *)controller delegate:(id)delegate NS_DESIGNATED_INITIALIZER;
-(void)showFeedBackController:(NSInteger )orderId isReturn:(BOOL)isReturn;

@end

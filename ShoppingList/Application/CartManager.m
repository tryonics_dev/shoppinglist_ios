//
//  CartManager.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/14/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "CartManager.h"

static CartManager *sharedManager = nil;

@implementation CartManager
@synthesize cartModel,cartItemsCount;

+ (CartManager *)sharedManager {
    
    static dispatch_once_t once;
    dispatch_once(&once, ^
                  {
                      sharedManager = [[CartManager alloc] init];
                  });
    return sharedManager;
}

- (id)init {
    if ((self = [super init])) {
        networkManager = [NetworkManager sharedManager];
        self.isCheckOutClicked = NO;
    }
    return self;
}

-(void)updateCartBadge
{
    
}
-(void)setBillingAddress:(nullable ) billingAddressModel
{
    self.billingAddressModel = billingAddressModel;
}
-(void)setDeliveryAddress:(nullable )deliveryAddressModel
{
    self.deliveryAddressModel = deliveryAddressModel;
}

-(void)checkOutValidate:(UIViewController *)cartViewController
{
    CustomerModel *customerModel = [CustomerModel  getSavedCustomerObject];
    if(customerModel == nil)
    {
        cartViewController.tabBarController.selectedIndex = 4;
        self.isCheckOutClicked = YES;
    }
    else
    {
        CheckOutViewController *ctrlCheckOut = [[CheckOutViewController alloc] initWithNibName:@"CheckOutViewController" bundle:nil];
        ctrlCheckOut.sBoard = cartViewController.storyboard;
        ctrlCheckOut.cartModel = self.cartModel;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:ctrlCheckOut];
        [cartViewController presentViewController:navigationController animated:YES completion:nil];
    }
}

#pragma mark - Network calls

-(void)addItemToCart:(NSString *)product_id quantity:(NSString *)quantity
{
    __weak typeof(self) weakSelf = self;
    [networkManager addItemToCart:product_id quantity:quantity success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
          self->cartItemsCount = [[[responseData objectForKey:@"data"] objectForKey:@"total_product_count"] integerValue];
            [[myAppDelegate tabBarController].tabBar.items[2] setBadgeValue:[NSString stringWithFormat:@"%li",weakSelf.cartItemsCount]];
            [AGPushNoteView showWithNotificationMessage:[[LanguageManager sharedManager] languageStringForKey:@"Product successfully added to cart"] completion:^{
                [AGPushNoteView close];
            }];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
    }];
}

typedef void(^getCartCompletion)(bool success , CartModel *cartModel);
-(void)getCart:(getCartCompletion)completionBlock
{
    [networkManager getCurrentCart:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            NSError *error;
            self->cartModel = [[CartModel alloc] initWithDictionary:[responseData objectForKey:@"data"] error:&error];
            self->cartItemsCount = self->cartModel.total_product_count;
            completionBlock(success,self->cartModel);
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        completionBlock(false,nil);
    }];
}

typedef void(^updateCartItemCompletion)(bool success);
-(void)updateCartItem:(NSString *)key quantity:(NSString *)quantity completionBlock:(updateCartItemCompletion)completionBlock
{
    [networkManager updateCartItemWithKey:key quantity:quantity success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        completionBlock(success);
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        completionBlock(NO);
    }];
}

typedef void(^deleteCartItemCompletion)(bool success);
-(void)deleteCartItem:(NSString *)key completionBlock:(deleteCartItemCompletion)completionBlock
{
    [networkManager deleteCartItemWithKey:key  success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        completionBlock(success);
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        completionBlock(NO);
    }];
}



@end

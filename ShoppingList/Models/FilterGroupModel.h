//
//  FilterGroupModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/9/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductFilterModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FilterGroupModel;

@interface FilterGroupModel : JSONModel

@property (nonatomic) NSInteger filter_group_id;
@property (nonatomic) NSString <Optional>*name;
@property (nonatomic)NSArray <ProductFilterModel> *filter;

@end

NS_ASSUME_NONNULL_END

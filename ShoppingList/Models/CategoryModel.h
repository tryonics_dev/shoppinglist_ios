//
//  CategoryModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@protocol CategoryModel;

@interface CategoryModel : JSONModel

@property (nonatomic) NSInteger category_id;
@property (nonatomic) NSString <Optional>*image;
@property (nonatomic) NSString *name;

@end

NS_ASSUME_NONNULL_END

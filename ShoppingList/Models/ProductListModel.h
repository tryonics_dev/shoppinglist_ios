//
//  ProductListModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/8/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductListModel : JSONModel

@property (nonatomic) NSArray <ProductModel>  *data;
@end

NS_ASSUME_NONNULL_END

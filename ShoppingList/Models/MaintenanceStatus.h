//
//  MaintenanceStatus.h
//  ShoppingList
//
//  Created by Dimal Govinnage on 11/19/20.
//  Copyright © 2020 Mohamed Roshan. All rights reserved.
//

#import <JSONModel/JSONModel.h>

NS_ASSUME_NONNULL_BEGIN
//@protocol MaintenanceStatus;
//
//@interface MaintenanceStatusData : JSONModel
//@property (nonatomic) BOOL *is_maintenance_on;
//@property (nonatomic) NSString *message;
//@property (nonatomic) NSString *title;
//@end

//@protocol MaintenanceStatusData;

@interface MaintenanceStatus : JSONModel
//@property (nonatomic) MaintenanceStatusData *data;
@property (nonatomic) BOOL is_maintenance_on;
@property (nonatomic) NSString *message;
@property (nonatomic) NSString *title;
@end

NS_ASSUME_NONNULL_END



//
//  ProductDiscountModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/10/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ProductDiscountModel;

@interface ProductDiscountModel : JSONModel

@property(nonatomic)NSInteger quantity;
@property(nonatomic)double price_excluding_tax;
@property(nonatomic)double price;
@property(nonatomic)NSString <Optional>*price_excluding_tax_formated;
@property(nonatomic)NSString <Optional>*price_formated;

@end

NS_ASSUME_NONNULL_END

//
//  CostData.h
//  ShoppingList
//
//  Created by Dimal Govinnage on 11/12/20.
//  Copyright © 2020 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CostData : JSONModel
@property (nonatomic) NSString *cost;
@end

NS_ASSUME_NONNULL_END

//
//  ProductFilter.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/9/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ProductFilterModel;

@interface ProductFilterModel : JSONModel

@property (nonatomic) NSInteger filter_id;
@property (nonatomic) NSString <Optional>*name;
@end

NS_ASSUME_NONNULL_END

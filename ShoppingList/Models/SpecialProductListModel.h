//
//  SpecialProductListModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/11/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FeaturedProductModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SpecialProductListModel :  JSONModel
@property (nonatomic) NSArray <FeaturedProductModel>  *data;

@end

NS_ASSUME_NONNULL_END

//
//  CustomerModel.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/24/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "CustomerModel.h"

//static CustomerModel *sharedManager = nil;

@implementation CustomerModel

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeInteger:self.address_id forKey:@"address_id"];
    [encoder encodeInteger:self.cart_count_products forKey:@"cart_count_products"];
    [encoder encodeObject:self.code forKey:@"code"];
    [encoder encodeInteger:self.customer_group_id forKey:@"customer_group_id"];
    [encoder encodeInteger:self.customer_id forKey:@"customer_id"];
    [encoder encodeObject:self.date_added forKey:@"date_added"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.fax forKey:@"fax"];
    [encoder encodeObject:self.firstname forKey:@"firstname"];
    [encoder encodeObject:self.ip forKey:@"ip"];
    [encoder encodeInteger:self.language_id forKey:@"language_id"];
    [encoder encodeObject:self.lastname forKey:@"lastname"];
    [encoder encodeInteger:self.newsletter forKey:@"newsletter"];
    [encoder encodeBool:self.safe forKey:@"safe"];
    [encoder encodeBool:self.status forKey:@"status"];
    [encoder encodeInteger:self.store_id forKey:@"store_id"];
    [encoder encodeObject:self.telephone forKey:@"telephone"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.address_id = [decoder decodeIntegerForKey:@"address_id"];
        self.cart_count_products = [decoder decodeIntegerForKey:@"cart_count_products"];
        self.code = [decoder decodeObjectForKey:@"code"];
        self.customer_group_id = [decoder decodeIntegerForKey:@"customer_group_id"];
        self.customer_id = [decoder decodeIntegerForKey:@"customer_id"];
        self.date_added = [decoder decodeObjectForKey:@"date_added"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.fax = [decoder decodeObjectForKey:@"fax"];
        self.firstname = [decoder decodeObjectForKey:@"firstname"];
        self.ip = [decoder decodeObjectForKey:@"ip"];
        self.language_id = [decoder decodeIntegerForKey:@"language_id"];
        self.lastname = [decoder decodeObjectForKey:@"lastname"];
        self.newsletter = [decoder decodeIntegerForKey:@"newsletter"];
        self.safe = [decoder decodeBoolForKey:@"safe"];
        self.status = [decoder decodeBoolForKey:@"status"];
        self.store_id = [decoder decodeIntegerForKey:@"store_id"];
        self.telephone = [decoder decodeObjectForKey:@"telephone"];
        
        self.fullName = [NSString stringWithFormat:@"%@ %@", self.firstname ,self.lastname];
        self.phoneWithCountryCode =  [NSString stringWithFormat:@"+94 %@",self.telephone];
    
        if([self.email isEqualToString:self.telephone])
            self.email = @"";
    }
    return self;
}
+(CustomerModel *)getSavedCustomerObject
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:LOGGED_USER_KEY];
    return [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
}
+(void)updateLocalCustomerData:(CustomerModel *)customer
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:customer];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:LOGGED_USER_KEY];
    [defaults synchronize];
}
@end

//
//  CategoriesModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CategoriesModel : JSONModel
@property (nonatomic) NSArray <CategoryModel>  *data;
@end

NS_ASSUME_NONNULL_END

//
//  FilterGroupListModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/9/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilterGroupModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FilterGroupListModel : JSONModel

@property (nonatomic) NSArray <FilterGroupModel> *filter_groups;

@end

NS_ASSUME_NONNULL_END

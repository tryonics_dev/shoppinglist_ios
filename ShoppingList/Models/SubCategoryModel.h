//
//  SubCategory.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/8/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FilterGroupListModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SubCategoryModel;

@interface SubCategoryModel : JSONModel

@property (nonatomic) NSInteger category_id;
@property (nonatomic) NSInteger parent_id;
@property (nonatomic) NSString <Optional>*image;
@property (nonatomic) NSString <Optional>*original_image;
@property (nonatomic) NSString <Optional>*seo_url;
@property (nonatomic) NSString *name;
@property (nonatomic) FilterGroupListModel *filters;
@end

NS_ASSUME_NONNULL_END

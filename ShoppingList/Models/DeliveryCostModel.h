//
//  DeliveryCostModel.h
//  ShoppingList
//
//  Created by Dimal Govinnage on 11/12/20.
//  Copyright © 2020 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CostData.h"
//@class Data;


#pragma mark - Object interfaces


//@protocol ShippingMethod;
//@protocol Data;
//@protocol Data;

@interface DeliveryCostModel : JSONModel
@property (nonatomic) NSInteger *success;
@property (nonatomic) NSArray *error;
@property (nonatomic) CostData *data;
@end

//@interface Data : JSONModel
//@property (nonatomic) NSString *cost;
//@end




//
//  ProductModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/8/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductDiscountModel.h"
#import "ProductFreeGiftModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ProductModel;

@interface ProductModel : JSONModel

@property (nonatomic) NSInteger id;
@property (nonatomic) NSInteger product_id;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString <Ignore> *manufacturer;
@property (nonatomic) NSString <Optional>*sku;
@property (nonatomic) NSString <Optional>*model;
@property (nonatomic) NSString <Optional>*image;
@property (nonatomic) NSArray <NSString *><Optional> *images ;
@property (nonatomic) NSString <Optional>*original_image;
@property (nonatomic) NSArray <NSString *> <Optional>*original_images ;
@property (nonatomic) double price_excluding_tax;
@property (nonatomic) NSString *price_excluding_tax_formated;
@property (nonatomic) double price;
@property (nonatomic) NSString *price_formated;
@property (nonatomic) NSInteger rating;
@property (nonatomic) NSString <Optional>*description;
@property (nonatomic) BOOL special;
@property (nonatomic) double special_excluding_tax;
@property (nonatomic) NSString <Optional>*special_excluding_tax_formated;
@property (nonatomic) NSString <Optional>*special_formated;
@property (nonatomic) NSString <Optional>*special_start_date;
@property (nonatomic) NSString <Optional>*special_end_date;
@property (nonatomic) NSInteger minimum;
@property (nonatomic) NSString <Optional>*meta_title;
@property (nonatomic) NSString <Optional>*meta_description;
@property (nonatomic) NSString <Optional>*meta_keyword;
@property (nonatomic) NSString <Optional>*seo_url;
@property (nonatomic) NSString <Optional>*tag;
@property (nonatomic) NSString <Optional>*upc;
@property (nonatomic) NSString <Optional>*ean;
@property (nonatomic) NSString <Optional>*jan;
@property (nonatomic) NSString <Optional>*isbn;
@property (nonatomic) NSString <Optional>*mpn;
@property (nonatomic) NSString <Optional>*location;
@property (nonatomic) NSString <Optional>*stock_status;
@property (nonatomic) NSInteger stock_status_id;
@property (nonatomic) NSInteger manufacturer_id;
@property (nonatomic) NSInteger tax_class_id;
@property (nonatomic) NSString <Optional>*date_available;
@property (nonatomic) double weight;
@property (nonatomic) NSInteger weight_class_id;
@property (nonatomic) double length;
@property (nonatomic) double width;
@property (nonatomic) double height;
@property (nonatomic) NSInteger length_class_id;
@property (nonatomic) NSInteger subtract;
@property (nonatomic) NSInteger sort_order;
@property (nonatomic) BOOL status;
@property (nonatomic) NSString <Optional>*date_added;
@property (nonatomic) NSString <Optional>*date_modified;
@property (nonatomic) NSInteger viewed;
@property (nonatomic) NSString <Optional>*weight_class;
@property (nonatomic) NSString <Optional>*length_class;
@property (nonatomic) NSString <Optional>*shipping;
@property (nonatomic) double points;
@property (nonatomic) NSInteger quantity;
@property (nonatomic) BOOL gift_flag;
@property (nonatomic) NSArray <ProductDiscountModel> *discounts;
@property (nonatomic) NSArray <ProductFreeGiftModel> *free_gifts_list;

@end

NS_ASSUME_NONNULL_END

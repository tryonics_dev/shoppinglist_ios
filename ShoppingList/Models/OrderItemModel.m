//
//  OrderItem.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/2/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "OrderItemModel.h"

@implementation OrderItemModel

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    if ([propertyName isEqualToString:@"total_raw"] || [propertyName isEqualToString:@"products"] || [propertyName isEqualToString:@"order_status_id"] || [propertyName isEqualToString:@"status_id"])
        return YES;
    
    return NO;
}

@end

//
//  CartModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/15/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CartAmountModel.h"
#import "CartItemModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CartModel : JSONModel

@property (nonatomic)NSString <Optional>*weight;
@property (nonatomic)BOOL coupon_status;
@property (nonatomic)NSString <Optional>*coupon;
@property (nonatomic)BOOL voucher_status;
@property (nonatomic)NSString <Optional>*voucher;
@property (nonatomic)BOOL reward_status;
@property (nonatomic)NSString <Optional>*reward;
@property (nonatomic)NSInteger total;
@property (nonatomic)double total_raw;
@property (nonatomic)NSInteger total_product_count;
@property (nonatomic)BOOL has_shipping;
@property (nonatomic)BOOL has_download;
@property (nonatomic)BOOL has_recurring_products;
@property (nonatomic)NSArray <CartAmountModel> *totals;
@property (nonatomic)NSArray <CartItemModel> *products;

@end

NS_ASSUME_NONNULL_END

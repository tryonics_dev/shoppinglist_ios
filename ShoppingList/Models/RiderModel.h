//
//  RiderModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 10/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RiderModel  : JSONModel

@property(nonatomic)NSString <Optional>*driver_mobile;
@property(nonatomic)NSString <Optional>*name;
@property(nonatomic)NSString <Optional>*vehicle_model;
@property(nonatomic)NSString <Optional>*vehicle_number;

@end

NS_ASSUME_NONNULL_END

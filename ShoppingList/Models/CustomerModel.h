//
//  CustomerModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/24/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerModel : JSONModel

+(CustomerModel *)getSavedCustomerObject;
+(void)updateLocalCustomerData:(CustomerModel *)customer;

@property (nonatomic)NSInteger address_id;
@property (nonatomic)NSInteger cart_count_products;
@property (nonatomic)NSString <Optional>*code;
@property (nonatomic)NSInteger customer_group_id;
@property (nonatomic)NSInteger customer_id;
@property (nonatomic)NSString <Optional>*date_added;
@property (nonatomic)NSString <Optional>*email;
@property (nonatomic)NSString <Optional>*fax;
@property (nonatomic)NSString <Optional>*firstname;
@property (nonatomic)NSString <Optional>*ip;
@property (nonatomic)NSInteger language_id;
@property (nonatomic)NSString <Optional>*lastname;
@property (nonatomic)NSInteger newsletter;
@property (nonatomic)BOOL safe;
@property (nonatomic)BOOL status;
@property (nonatomic)NSInteger store_id;
@property (nonatomic) NSString <Optional>*telephone;

@property (nonatomic) NSString <Ignore> *fullName;
@property (nonatomic) NSString <Ignore> *phoneWithCountryCode;

@end

NS_ASSUME_NONNULL_END

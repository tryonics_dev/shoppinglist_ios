//
//  AddressModel.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/30/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "AddressModel.h"

@implementation AddressModel

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    if ([propertyName isEqualToString:@"landmark_lat"] || [propertyName isEqualToString:@"landmark_lon"])
        return YES;
    
    return NO;
}

+ (JSONKeyMapper *)keyMapper
{
    
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                @"is_default": @"default"
                                                                  }];
}

@end

//
//  OrderItem.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/2/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OrderItemModel;

NS_ASSUME_NONNULL_BEGIN

@interface OrderItemModel : JSONModel

@property(nonatomic)NSString <Optional>*currency_code;
@property(nonatomic)NSString <Optional>*currency_value;
@property(nonatomic)NSString <Optional>*date_added;
@property(nonatomic)NSString <Optional>*name;
@property(nonatomic)NSString <Optional>*order_id;
@property(nonatomic)NSInteger order_status_id;
@property(nonatomic)NSInteger status_id;
@property(nonatomic)NSInteger products;
@property(nonatomic)NSString <Optional>*status;
@property(nonatomic)NSString <Optional>*timestamp;
@property(nonatomic)NSString <Optional>*total;
@property(nonatomic)NSString <Optional>*payment_method;
@property(nonatomic)double total_raw;



@end

NS_ASSUME_NONNULL_END

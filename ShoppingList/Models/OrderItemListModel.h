//
//  OrderItemListModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/2/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderItemModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderItemListModel : JSONModel

@property (nonatomic) NSArray <OrderItemModel>  *data;
@end

NS_ASSUME_NONNULL_END

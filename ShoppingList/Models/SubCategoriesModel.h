//
//  SubCategoriesModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/8/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubCategoryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SubCategoriesModel : JSONModel

@property (nonatomic) NSArray <SubCategoryModel>  *sub_categories;

@end

NS_ASSUME_NONNULL_END

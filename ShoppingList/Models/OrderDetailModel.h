//
//  OrderDetailModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol OrderDetailProductModel;


@interface OrderDetailProductModel : JSONModel

@property(nonatomic)NSString <Optional>*model;
@property(nonatomic)NSString <Optional>*name;
@property(nonatomic)NSInteger order_product_id;
@property(nonatomic)NSString <Optional>*price;
@property(nonatomic)double price_raw;
@property(nonatomic)NSString <Optional>*product_id;
@property(nonatomic)NSInteger quantity;
@property(nonatomic)NSString <Optional>*total;
@property(nonatomic)double total_raw;

@end

@protocol OrderCostModel;

@interface OrderCostModel : JSONModel

@property(nonatomic)NSString <Optional>*order_total_id;
@property(nonatomic)NSString <Optional>*order_id;
@property(nonatomic)NSString <Optional>*code;
@property(nonatomic)NSString <Optional>*title;
@property(nonatomic)NSString <Optional>*value;
@property(nonatomic)NSString <Optional>*sort_order;

@end

@interface OrderDetailModel : JSONModel

@property(nonatomic)NSString <Optional>*comment;
@property(nonatomic)NSString <Optional>*currency_code;
@property(nonatomic)NSInteger currency_id;
@property(nonatomic)double currency_value;
@property(nonatomic)NSInteger customer_id;
@property(nonatomic)NSString <Optional>*date_added;
@property(nonatomic)NSString <Optional>*date_modified;
@property(nonatomic)NSString <Optional>*email;
@property(nonatomic)NSString <Optional>*firstname;
@property(nonatomic)NSString <Optional>*invoice_no;
@property(nonatomic)NSString <Optional>*invoice_prefix;
@property(nonatomic)NSInteger language_id;
@property(nonatomic)NSString <Optional>*lastname;
@property(nonatomic)NSInteger order_id;
@property(nonatomic)NSInteger order_status_id;
@property(nonatomic)NSString <Optional>*payment_address;
@property(nonatomic)NSString <Optional>*payment_address_1;
@property(nonatomic)NSString <Optional>*payment_address_2;
@property(nonatomic)NSString <Optional>*payment_address_format;
@property(nonatomic)NSString <Optional>*payment_city;
@property(nonatomic)NSString <Optional>*payment_company;
@property(nonatomic)NSString <Optional>*payment_country;
@property(nonatomic)NSInteger payment_country_id;
@property(nonatomic)NSString <Optional>*payment_firstname;
@property(nonatomic)NSString <Optional>*payment_iso_code_2;
@property(nonatomic)NSString <Optional>*payment_iso_code_3;
@property(nonatomic)NSString <Optional>*payment_lastname;
@property(nonatomic)NSString <Optional>*payment_method;
@property(nonatomic)NSString <Optional>*payment_postcode;
@property(nonatomic)NSString <Optional>*payment_zone;
@property(nonatomic)NSString <Optional>*payment_zone_code;
@property(nonatomic)NSInteger payment_zone_id;
@property(nonatomic)NSString <Optional>*shipping_address;
@property(nonatomic)NSString <Optional>*shipping_address_1;
@property(nonatomic)NSString <Optional>*shipping_address_2;
@property(nonatomic)NSString <Optional>*shipping_address_format;
@property(nonatomic)NSString <Optional>*shipping_city;
@property(nonatomic)NSString <Optional>*shipping_company;
@property(nonatomic)NSString <Optional>*shipping_country;
@property(nonatomic)NSString <Optional>*shipping_country_id;
@property(nonatomic)NSString <Optional>*shipping_firstname;
@property(nonatomic)NSString <Optional>*shipping_iso_code_2;
@property(nonatomic)NSString <Optional>*shipping_iso_code_3;
@property(nonatomic)NSString <Optional>*shipping_lastname;
@property(nonatomic)NSString <Optional>*shipping_method;
@property(nonatomic)NSString <Optional>*shipping_postcode;
@property(nonatomic)NSString <Optional>*shipping_zone;
@property(nonatomic)NSString <Optional>*shipping_zone_code;
@property(nonatomic)NSInteger shipping_zone_id;
@property(nonatomic)NSInteger store_id;
@property(nonatomic)NSString <Optional>*store_name;
@property(nonatomic)NSString <Optional>*store_url;
@property(nonatomic)NSString <Optional>*telephone;
@property(nonatomic)NSString <Optional>*timestamp;
@property(nonatomic)double total;
@property(nonatomic)NSArray <OrderDetailProductModel> *products;
@property(nonatomic)NSArray <OrderCostModel> *totals;
@end




NS_ASSUME_NONNULL_END

//
//  CartItemModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/15/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CartItemModel ;

@interface CartItemModel : JSONModel

@property (nonatomic)NSString <Optional>*key;
@property (nonatomic)NSString <Optional>*thumb;
@property (nonatomic)NSString <Optional>*name;
@property (nonatomic)NSInteger points;
@property (nonatomic)NSInteger product_id;
@property (nonatomic)NSString <Optional>*model;
@property (nonatomic)NSString <Optional>*quantity;
@property (nonatomic)NSString <Optional>*recurring;
@property (nonatomic)BOOL stock;
@property (nonatomic)NSString <Optional>*reward;
@property (nonatomic)NSString <Optional>*price;
@property (nonatomic)NSString <Optional>*total;
@property (nonatomic)double price_raw;
@property (nonatomic)double total_raw;

@end

NS_ASSUME_NONNULL_END

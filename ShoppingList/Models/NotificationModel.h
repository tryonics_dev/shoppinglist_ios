//
//  NotificationModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/18/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol NotificationModel;

@interface NotificationModel : JSONModel

@property (nonatomic)NSInteger notification_id;
@property (nonatomic)NSInteger product_id;
@property (nonatomic)NSString <Optional>*start_date;
@property (nonatomic)NSString <Optional>*end_date;
@property (nonatomic)NSString <Optional>*message;
@property (nonatomic)NSString <Optional>*product_name;
@property (nonatomic)NSString <Optional>*image;
@property (nonatomic)NSInteger type;
@end

@interface NotificationListModel : JSONModel

@property (nonatomic)NSArray <NotificationModel> *data;

@end

NS_ASSUME_NONNULL_END

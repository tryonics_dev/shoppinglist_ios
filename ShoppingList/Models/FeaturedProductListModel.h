//
//  ProductListModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FeaturedProductModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FeaturedProductListModel : JSONModel
@property (nonatomic) NSArray <FeaturedProductModel>  *products;

@end

NS_ASSUME_NONNULL_END

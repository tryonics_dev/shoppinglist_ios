//
//  AddressModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/30/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AddressModel;

@interface AddressModel : JSONModel

@property(nonatomic)NSInteger address_id;
@property(nonatomic)NSString <Optional>*firstname;
@property(nonatomic)NSString <Optional>*lastname;
@property(nonatomic)NSString <Optional>*city;
@property(nonatomic)NSString <Optional>*address_1;
@property(nonatomic)NSString <Optional>*address_2;
@property(nonatomic)NSString <Optional>*country;
@property(nonatomic)NSInteger country_id;
@property(nonatomic)NSString <Optional>*postcode;
@property(nonatomic)NSInteger zone_id;
@property(nonatomic)NSString <Optional>*company;
@property(nonatomic)BOOL is_default;
@property(nonatomic)NSString <Optional>*landmark;
@property(nonatomic)double landmark_lat;
@property(nonatomic)double landmark_lon;


@end

NS_ASSUME_NONNULL_END

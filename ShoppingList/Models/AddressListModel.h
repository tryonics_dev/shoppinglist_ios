//
//  AddressListModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/30/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddressListModel : JSONModel

@property (nonatomic) NSArray <AddressModel>  *data;

@end

NS_ASSUME_NONNULL_END

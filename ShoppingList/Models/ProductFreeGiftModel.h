//
//  ProductFreeGiftModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/10/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ProductFreeGiftModel;

@interface ProductFreeGiftModel : JSONModel

@property (nonatomic) NSInteger product_id;
@property (nonatomic) NSString <Optional>*name;
@property (nonatomic) NSString <Optional>*image;
@property (nonatomic) NSString <Optional>*start_date;
@property (nonatomic) NSString <Optional>*end_date;
@property (nonatomic) NSString <Optional>*condition;
@end

NS_ASSUME_NONNULL_END

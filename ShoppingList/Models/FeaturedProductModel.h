//
//  ProductModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FeaturedProductModel;

@interface FeaturedProductModel : JSONModel
@property (nonatomic) NSInteger product_id;
@property (nonatomic) NSString <Optional>*thumb;
@property (nonatomic) NSString *name;
@property (nonatomic) NSInteger quantity;
@property (nonatomic) NSString <Optional>*seo_url;
@property (nonatomic) BOOL status;
@property (nonatomic) NSString <Optional>*stock_status;
@property (nonatomic) double price_excluding_tax;
@property (nonatomic) NSString *price_excluding_tax_formated;
@property (nonatomic) double price;
@property (nonatomic) NSString *price_formated;
@property (nonatomic) double special;
@property (nonatomic) NSString <Optional>*special_formated;
@property (nonatomic) double special_excluding_tax;
@property (nonatomic) NSString <Optional>*special_excluding_tax_formated;
@property (nonatomic) NSInteger rating;
@property (nonatomic) NSString <Optional> *special_start_date;
@property (nonatomic) BOOL gift_flag;
@property (nonatomic) NSString <Optional>*description;
@end

NS_ASSUME_NONNULL_END

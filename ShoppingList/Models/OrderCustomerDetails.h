//
//  OrderCustomerDetails.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 10/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderCustomerDetails : JSONModel

@property(nonatomic)NSString <Optional>*customer_address;
@property(nonatomic)NSInteger customer_id;
@property(nonatomic)NSString <Optional>*customer_landmark;
@property(nonatomic)NSString <Optional>*customer_lat;
@property(nonatomic)NSString <Optional>*customer_long;
@property(nonatomic)NSString <Optional>*customer_mobile_number;
@property(nonatomic)NSString <Optional>*customer_name;

@end

NS_ASSUME_NONNULL_END

//
//  CartTotalModel.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/15/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CartAmountModel;

@interface CartAmountModel : JSONModel

@property (nonatomic)NSString <Optional>*title;
@property (nonatomic)NSString <Optional>*text;
//@property (nonatomic)double *value;

@end

NS_ASSUME_NONNULL_END

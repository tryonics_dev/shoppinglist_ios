//
//  NetworkManager.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "Reachability.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkManager : NSObject
{
    NSString *API_KEY;
    NSString *BASE_URL;
    
    NSString *URL_ROUTE_FEED;
    NSString *URL_ROUTE_CART;
    NSString *strLanguageCode;
    
    Reachability *networkReachability;
}
typedef void (^NetworkManagerSuccess)(id responseData);
typedef void (^NetworkManagerFailure)(NSString *failureReason, NSInteger statusCode);

@property (nonatomic,strong)NSString *SESSION_KEY;
@property (nonatomic, strong) AFHTTPSessionManager *networkingManager;
@property (nonatomic, strong) MBProgressHUD *progressHUD;
@property (nonatomic, strong) MBProgressHUD *noInternetHUD;

+ (id)sharedManager;
-(BOOL)isReachable;
- (void)showProgressHUD;
- (void)hideProgressHUD;
-(void)updateLanguage;

-(void)getAPISession:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)getHomeScreenBanners :(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)getTopCategories:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)getTopFeaturedProducts:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)getAllCategories:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure ;
- (void)getSubCategoriesByParentID:(NSInteger)parentId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)getProductListByCategoryID:(NSInteger)categoryId page:(NSInteger)page filters:(NSString *)filters success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)getProductDetailsById:(NSInteger)productId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)getLatestProductList:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)getSpecialsProductList:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure ;
- (void)searchProduct:(NSString *)searchText filters:(NSString *)filters categoryId:(NSString *)categoryId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)getSearchProductFilters:(NSString *)searchText  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;


- (void)getNotifications:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure ;

-(void)addItemToCart:(NSString *)product_id quantity:(NSString *)quantity success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)getCurrentCart:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)updateCartItemWithKey:(NSString *)cart_item_key quantity:(NSString *)quantity success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)deleteCartItemWithKey:(NSString *)cart_item_key success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;


-(void)getOTP:(NSString *)phoneNumber type:(int)type success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)confirmOTP:(NSString *)phoneNumber type:(int)type otp:(NSString *)otp success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)registerCustomer:(NSDictionary *)postObject  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)customerLogin:(NSString *)phoneNumber password:(NSString *)password success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)customerLogout:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure ;
-(void)updatePassword:(NSString *)password confirmPassword:(NSString *)confirmPassword success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)updateCustomer:(NSDictionary *)postObject  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)addAddress:(NSDictionary *)postObject success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)getCustomerAddressList:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)updateAddress:(NSDictionary *)postObject id:(int)addressId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)deleteAddress:(int)addressId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)forgotPassword:(NSString *)password confirmPassword:(NSString *)confirmPassword telephone:(NSString *)telephone success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;

#pragma mark - checkout
//Step 1
-(void)getPaymentAddres:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure ;
//Step 2
-(void)setPaymentAddres:(int)address_id landmark:(NSString *)landmark latitude:(NSString *)latitude longitude:(NSString *)longitude success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
//Step 3
-(void)getPaymentMethods:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
//Step 4
-(void)getShippingAddres:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
//Step 5
-(void)setShippingAddres:(int)address_id landmark:(NSString *)landmark latitude:(NSString *)latitude longitude:(NSString *)longitude success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
//Step 6
-(void)getShippingMethods:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
//Step 7
-(void)setShippingMethod:(NSString *)shippingMethod  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
//Step 8
-(void)setPaymentMethod:(NSString *)paymentMethod  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
//Step 9
-(void)getOrderOverView:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
//Step 10
-(void)confirmOrder:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;

#pragma mark - order

-(void)getOderList:(NSInteger)page success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)getOrderById:(NSInteger)orderId  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)updateOrderStatusById:(NSInteger)orderId comment:(NSString *)comment status:(int )statusId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)updateOrderStatusById:(NSInteger)orderId comment:(NSString *)comment status:(int )statusId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)productReorder:(NSString *)prodcutListJson success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
-(void)getCustomerOrderById:(NSInteger)orderId  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
#pragma mark - Register device token with server

-(void)registerDeviceToken:(NSString *)token phoneNumber:(NSString *)phoneNumber success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;

-(void)getServerDate:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;

-(void)getDeliveryCost:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;

@end

NS_ASSUME_NONNULL_END

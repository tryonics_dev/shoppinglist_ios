//
//  NetworkManager.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "NetworkManager.h"


@implementation NetworkManager

#pragma mark - Constructors

static NetworkManager *sharedManager = nil;
@synthesize SESSION_KEY;
+ (NetworkManager*)sharedManager {
    
        static dispatch_once_t once;
        dispatch_once(&once, ^
                      {
                          sharedManager = [[NetworkManager alloc] init];
                      });
        return sharedManager;
    
}

- (id)init {
    if ((self = [super init])) {
        
        NSString *path = [[NSBundle mainBundle] pathForResource: @"APIConfig" ofType: @"plist"];
        NSDictionary *dictAPIConfig = [NSDictionary dictionaryWithContentsOfFile: path];
        API_KEY = [dictAPIConfig objectForKey: @"API_KEY"];
        BASE_URL = [dictAPIConfig objectForKey: @"BASE_URL"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        SESSION_KEY = [defaults objectForKey:APP_SESSION_KEY];
        
        [self updateLanguage];
        
        networkReachability = [Reachability reachabilityForInternetConnection];
    }
    return self;
}
-(void)updateLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *selectedLanguage = [defaults objectForKey:SELECTED_LANGUAGE_KEY];
   
    if([selectedLanguage isEqualToString:LANGUAGE_CODE_SINHALA])
        strLanguageCode = @"si";
    else if([selectedLanguage isEqualToString:LANGUAGE_CODE_TAMIL])
        strLanguageCode = @"ta";
    else
        strLanguageCode = @"en-gb";
}
- (void)showProgressHUD {
    [self hideProgressHUD];
    self.progressHUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] delegate].window animated:YES];
    [self.progressHUD removeFromSuperViewOnHide];
    self.progressHUD.bezelView.color = [UIColor clearColor];
    self.progressHUD.bezelView.backgroundColor = [UIColor clearColor];
    self.progressHUD.contentColor =APP_THEME_LIGHT_GREEN;
    self.progressHUD.label.text = [[LanguageManager sharedManager] languageStringForKey:@"Loading…"];
    [self.progressHUD setAnimationType:MBProgressHUDAnimationFade];
}

- (void)hideProgressHUD {
    if (self.progressHUD != nil) {
        [self.progressHUD hideAnimated:YES];
        [self.progressHUD removeFromSuperview];
        self.progressHUD = nil;
    }
}
-(void)showNointernetMessage
{
    [self hideNointernetMessage];
    self.noInternetHUD = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] delegate].window animated:YES];
    [self.noInternetHUD removeFromSuperViewOnHide];
    self.noInternetHUD.bezelView.color = [UIColor clearColor];
    self.noInternetHUD.bezelView.backgroundColor = [UIColor clearColor];
    self.noInternetHUD.contentColor =  APP_THEME_LIGHT_GREEN;
    self.noInternetHUD.activityIndicatorColor= [UIColor clearColor];
    self.noInternetHUD.label.text = [[LanguageManager sharedManager] languageStringForKey:@"Network Error! Please check your internet connection"];
    [self.noInternetHUD setAnimationType:MBProgressHUDAnimationFade];
    [self performSelector:@selector(hideNointernetMessage) withObject:nil afterDelay:3];
}
-(void)hideNointernetMessage
{
    if (self.noInternetHUD != nil) {
        [self.noInternetHUD hideAnimated:YES];
        [self.noInternetHUD removeFromSuperview];
        self.noInternetHUD = nil;
    }
}
-(BOOL)isReachable
{
    NetworkStatus networkStatus = networkReachability.currentReachabilityStatus;
    if(networkStatus==NotReachable)
        return false;
    else
        return true;
}

- (AFHTTPSessionManager*)getNetworkingManager {
    if([self isReachable]){
    if (self.networkingManager == nil) {
   
        self.networkingManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        self.networkingManager.requestSerializer =  [AFJSONRequestSerializer serializer];
        AFHTTPResponseSerializer * responseSerializer =[AFCompoundResponseSerializer serializer]; //[AFHTTPResponseSerializer serializer];
        [responseSerializer.acceptableContentTypes setByAddingObjectsFromArray:@[@"application/json"]];
        self.networkingManager.responseSerializer = responseSerializer;
//        [self.networkingManager.requestSerializer setTimeoutInterval:240];
        
        if (API_KEY != nil && [API_KEY length] > 0) {
            NSString *headerToken = [NSString stringWithFormat:@"%@", API_KEY];
            [self.networkingManager.requestSerializer setValue:headerToken forHTTPHeaderField:@"X-Oc-Merchant-Id"];
        }
       
        [self.networkingManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self.networkingManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"accept"];
        self.networkingManager.responseSerializer.acceptableContentTypes = [self.networkingManager.responseSerializer.acceptableContentTypes setByAddingObjectsFromArray:@[@"application/json"]];
        self.networkingManager.securityPolicy = [self getSecurityPolicy];
    
    }
    if(SESSION_KEY != nil && SESSION_KEY.length>0)
        [self.networkingManager.requestSerializer setValue:SESSION_KEY forHTTPHeaderField:@"X-Oc-Session"];
    
    if(strLanguageCode !=nil && strLanguageCode.length > 0)
        [self.networkingManager.requestSerializer setValue:strLanguageCode forHTTPHeaderField:@"X-Oc-Merchant-Language"];
    
    return self.networkingManager;
    }
    else{
        [self hideProgressHUD];
        [self showNointernetMessage];
        return nil;
    }
}

- (id)getSecurityPolicy {
    return [AFSecurityPolicy defaultPolicy];
    /* Example - AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
     [policy setAllowInvalidCertificates:YES];
     [policy setValidatesDomainName:NO];
     return policy; */
}

- (NSString*)getError:(NSError*)error {
    if (error != nil) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if(errorData != nil){
            NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
            if (responseObject != nil && [responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"error"] != nil && [[responseObject objectForKey:@"error"] isKindOfClass:[NSArray class]]) {
                return [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"error"][0]];
            }
            else
            {
               return @"Something went wrong. Please try again later";
            }
        }
        else
        {
            return @"Something went wrong. Please try again later";
        }
    }
    return @"Server Error. Please try again later";
}
-(id _Nonnull)getData:(id)responseObject
{
    if(responseObject != nil)
    {
        NSString *responsString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *jsonData=[responsString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        NSLog(@"error - %@",error.localizedDescription);
        NSLog(@"responseObject - %@",responseDic);
        return  responseDic;
    }
    return [[NSDictionary alloc] init];
}
#pragma mark  - GET

-(void)getWithURL:(NSString *)url success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure
{
    [[self getNetworkingManager] GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if (success != nil) {
            success([self getData:responseObject]);
        }
        [self hideProgressHUD];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self hideProgressHUD];
        NSString *errorMessage = [self getError:error];
        if (failure != nil) {
            failure(errorMessage, ((NSHTTPURLResponse*)operation.response).statusCode);
        }
    }];
}

#pragma mark  - POST

-(void)postJsonWithURL:(NSString *)url postBody:(id )jsonBody success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure
{
    
    [[self getNetworkingManager] POST:url parameters:jsonBody progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success != nil) {
            success([self getData:responseObject]);
        }
        [self hideProgressHUD];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self hideProgressHUD];
        NSString *errorMessage = [self getError:error];
        if (failure != nil) {
            failure(errorMessage, ((NSHTTPURLResponse*)task.response).statusCode);
        }
    }];

}
#pragma mark  - PUT

-(void)putJsonWithURL:(NSString *)url postBody:(id )jsonBody success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure
{
    [[self getNetworkingManager] PUT:url parameters:jsonBody success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideProgressHUD];
        if (success != nil) {
            success([self getData:responseObject]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSString *errorMessage = [self getError:error];
        [self hideProgressHUD];
        if (failure != nil) {
            failure(errorMessage, ((NSHTTPURLResponse*)task.response).statusCode);
        }
    }];
    
}

#pragma mark - DELETE

-(void)deleteJsonWithURL:(NSString *)url postBody:(id)jsonBody success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure
{
    [[self getNetworkingManager] DELETE:url parameters:jsonBody success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self hideProgressHUD];
        if (success != nil) {
            success([self getData:responseObject]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSString *errorMessage = [self getError:error];
        [self hideProgressHUD];
        if (failure != nil) {
            failure(errorMessage, ((NSHTTPURLResponse*)task.response).statusCode);
        }
    }];
    
}

#pragma mark - App API Calls

#pragma mark - feed/rest_api

-(void)getAPISession:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure{
    [self getWithURL:@"?route=feed/rest_api/session" success:success failure:failure];
}
- (void)getHomeScreenBanners :(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure{
    [self getWithURL:@"?route=feed/rest_api/banners&id=7" success:success failure:failure];
}
- (void)getTopCategories:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
     [self getWithURL:@"?route=feed/rest_api/top_categories" success:success failure:failure];
}
- (void)getTopFeaturedProducts:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
     [self getWithURL:@"?route=feed/rest_api/featured" success:success failure:failure];
}

- (void)getAllCategories:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    [self getWithURL:@"?route=feed/rest_api/categories" success:success failure:failure];
}
- (void)getSubCategoriesByParentID:(NSInteger)parentId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    [self getWithURL:[NSString stringWithFormat:@"?route=feed/rest_api/categories&id=%li",(long)parentId] success:success failure:failure];
}
- (void)getProductListByCategoryID:(NSInteger)categoryId page:(NSInteger)page filters:(NSString *)filters success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:[NSString stringWithFormat:@"?route=feed/rest_api/products&category=%li&limit=%i&page=%li&filters=%@",(long)categoryId,ITEMS_PER_PAGE,page,filters] success:success failure:failure];
}
- (void)getProductDetailsById:(NSInteger)productId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    [self getWithURL:[NSString stringWithFormat:@"?route=feed/rest_api/products&id=%li",(long)productId] success:success failure:failure];
}
- (void)getLatestProductList:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:[NSString stringWithFormat:@"?route=feed/rest_api/latest&limit=10"] success:success failure:failure];
}
- (void)getSpecialsProductList:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:[NSString stringWithFormat:@"?route=feed/rest_api/specials&limit=0"]  success:success failure:failure];
}
- (void)searchProduct:(NSString *)searchText filters:(NSString *)filters categoryId:(NSString *)categoryId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:[NSString stringWithFormat:@"?route=feed/rest_api/products/search&search=%@&filters=%@&category=%@",searchText,filters,categoryId]   success:success failure:failure];
}
- (void)getSearchProductFilters:(NSString *)searchText  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:[NSString stringWithFormat:@"?route=feed/rest_api/products_custom_search&search=%@",searchText]   success:success failure:failure];
}
#pragma mark - rest_api/notification

- (void)getNotifications:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    [self getWithURL:[NSString stringWithFormat:@"?route=feed/rest_api/get_notification"]   success:success failure:failure];
}

#pragma mark - rest/cart

-(void)addItemToCart:(NSString *)product_id quantity:(NSString *)quantity success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:product_id,@"product_id",quantity,@"quantity", nil];
   [self postJsonWithURL:@"?route=rest/cart/cart" postBody:postObject  success:success failure:failure];
   
}

-(void)getCurrentCart:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:[NSString stringWithFormat:@"?route=rest/cart/cart"]  success:success failure:failure];
}
-(void)updateCartItemWithKey:(NSString *)cart_item_key quantity:(NSString *)quantity success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:cart_item_key,@"key",quantity,@"quantity", nil];
    [self putJsonWithURL:@"?route=rest/cart/cart" postBody:postObject success:success failure:failure];
}
-(void)deleteCartItemWithKey:(NSString *)cart_item_key success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:cart_item_key,@"key", nil];
    [self deleteJsonWithURL:@"?route=rest/cart/cart" postBody:postObject success:success failure:failure];
}

#pragma mark  - registration

-(void)getOTP:(NSString *)phoneNumber type:(int)type success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"0%@",phoneNumber],@"telephone",[NSString stringWithFormat:@"%i",type],@"type", nil];
    [self postJsonWithURL:@"?route=feed/rest_api/otp_send" postBody:postObject success:success failure:failure];
}
-(void)confirmOTP:(NSString *)phoneNumber type:(int)type otp:(NSString *)otp success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    
    [self showProgressHUD];
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"0%@",phoneNumber],@"telephone",[NSString stringWithFormat:@"%i",type],@"type",otp,@"code", nil];
     [self postJsonWithURL:@"?route=feed/rest_api/otp_send" postBody:postObject success:success failure:failure];
    
}
-(void)registerCustomer:(id )postObject  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    
    [self showProgressHUD];
    [self postJsonWithURL:@"?route=rest/register/register" postBody:postObject  success:success failure:failure];
    
}
-(void)customerLogin:(NSString *)phoneNumber password:(NSString *)password success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    
    [self showProgressHUD];
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"0%@",phoneNumber],@"email",password,@"password", nil];
    [self postJsonWithURL:@"?route=rest/login/login" postBody:postObject  success:success failure:failure];
    
}
-(void)customerLogout:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    
    [self showProgressHUD];
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"",nil];
    [self postJsonWithURL:@"?route=rest/logout/logout" postBody:postObject success:success failure:failure];

}

-(void)updatePassword:(NSString *)password confirmPassword:(NSString *)confirmPassword success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:password,@"password",confirmPassword,@"confirm", nil];
     [self putJsonWithURL:@"?route=rest/account/password" postBody:postObject success:success failure:failure];
}
-(void)updateCustomer:(id )postObject  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    
    [self showProgressHUD];
    [self putJsonWithURL:@"?route=rest/account/account" postBody:postObject success:success failure:failure];

}
-(void)addAddress:(id)postObject success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    
    [self showProgressHUD];
    [self postJsonWithURL:@"?route=rest/account/address" postBody:postObject success:success failure:failure];

}

-(void)getCustomerAddressList:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure{
    
    [self showProgressHUD];
    [self getWithURL:@"?route=rest/account/address" success:success failure:failure];
    
}
-(void)updateAddress:(id)postObject id:(int)addressId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    
    [self showProgressHUD];
    [self putJsonWithURL:[NSString stringWithFormat:@"?route=rest/account/address&id=%i",addressId]  postBody:postObject success:success failure:failure];
    
}
-(void)deleteAddress:(int)addressId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"",nil];
    [self deleteJsonWithURL:[NSString stringWithFormat:@"?route=rest/account/address&id=%i",addressId] postBody:postObject success:success failure:failure];
}
-(void)forgotPassword:(NSString *)password confirmPassword:(NSString *)confirmPassword telephone:(NSString *)telephone success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    
    [self showProgressHUD];
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:password,@"password",confirmPassword,@"confirm",[NSString stringWithFormat:@"0%@",telephone],@"telephone", nil];
    [self postJsonWithURL:@"?route=rest/forgotten/forgotten" postBody:postObject success:success failure:failure];
    
}

#pragma mark - checkout

//Step 1
-(void)getPaymentAddres:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:@"?route=rest/payment_address/paymentaddress" success:success failure:failure];
}
//Step 2
-(void)setPaymentAddres:(int)address_id landmark:(NSString *)landmark latitude:(NSString *)latitude longitude:(NSString *)longitude success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:address_id],@"address_id", landmark,@"landmark",latitude,@"landmark_lat",longitude,@"landmark_lon" ,nil];
    [self postJsonWithURL:@"?route=rest/payment_address/paymentaddress&existing=1" postBody:postObject success:success failure:failure];
}
//Step 3
-(void)getPaymentMethods:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:@"?route=rest/payment_method/payments" success:success failure:failure];
}
//Step 4
-(void)getShippingAddres:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:@"?route=rest/shipping_address/shippingaddress" success:success failure:failure];
}
//Step 5
-(void)setShippingAddres:(int)address_id landmark:(NSString *)landmark latitude:(NSString *)latitude longitude:(NSString *)longitude success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:address_id],@"address_id", landmark,@"landmark",latitude,@"landmark_lat",longitude,@"landmark_lon" ,nil];
    [self postJsonWithURL:@"?route=rest/shipping_address/shippingaddress&existing=1" postBody:postObject success:success failure:failure];
}
//Step 6
-(void)getShippingMethods:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:@"?route=rest/shipping_method/shippingmethods" success:success failure:failure];
}
//Step 7
-(void)setShippingMethod:(NSString *)shippingMethod  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:shippingMethod,@"shipping_method",@"free delivery",@"comment", nil];
    [self postJsonWithURL:@"?route=rest/shipping_method/shippingmethods"  postBody:postObject success:success failure:failure];
}
//Step 8
-(void)setPaymentMethod:(NSString *)paymentMethod  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:paymentMethod,@"payment_method",@"free delivery",@"comment",@"1",@"agree", nil];
    [self postJsonWithURL:@"?route=rest/payment_method/payments" postBody:postObject success:success failure:failure];
}
//Step 9
-(void)getOrderOverView:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil];
    [self postJsonWithURL:@"?route=rest/confirm/confirm" postBody:postObject  success:success failure:failure];
}
//Step 10
-(void)confirmOrder:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
   
     NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil];
    [self putJsonWithURL:@"?route=rest/confirm/confirm" postBody:postObject success:success failure:failure];
}

#pragma mark - order

-(void)getOderList:(NSInteger)page success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    NSString *strURL = [NSString stringWithFormat:@"?route=rest/order/orders&limit=%i&page=%li",ITEMS_PER_PAGE,page];
    [self getWithURL:strURL success:success failure:failure];
}

-(void)getOrderById:(NSInteger)orderId  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    NSString *strURL = [NSString stringWithFormat:@"?route=rest/order/orders&id=%li",(long)orderId];
    [self getWithURL:strURL success:success failure:failure];
}
-(void)updateOrderStatusById:(NSInteger)orderId comment:(NSString *)comment status:(int )statusId success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    
    [self showProgressHUD];
    //NSString *strURL = @"feed/rest_api/changeOrderStatus";
    
    //NSString* statusIdN = [NSString stringWithFormat:@"%d", statusId];
    //NSString* orderIdN = [NSString stringWithFormat:@"%li", orderId];

    //NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:statusIdN,@"status",comment,@"comment",orderIdN,@"order_id", nil];

    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:statusId],@"status",comment,@"comment",[NSString stringWithFormat:@"%li",(long)orderId],@"order_id", nil];
    
    
    [self postJsonWithURL:@"?route=feed/rest_api/changeOrderStatus" postBody:postObject  success:success failure:failure];
    
}
-(void)productReorder:( id)prodcutListJson success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    [self postJsonWithURL:@"?route=rest/cart/bulkcart" postBody:prodcutListJson success:success failure:failure];
}
-(void)getCustomerOrderById:(NSInteger)orderId  success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    NSString *strURL = [NSString stringWithFormat:@"?route=feed/rest_api/getOrderCustomerDataByOrderID&order_id=%li",(long)orderId];
    [self getWithURL:strURL success:success failure:failure];
}
#pragma mark - Register device token with server

-(void)registerDeviceToken:(NSString *)token phoneNumber:(NSString *)phoneNumber success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure
{
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:phoneNumber,@"telephone",token,@"device_id", nil];
    [self postJsonWithURL:@"?route=feed/rest_api/device_register" postBody:postObject success:success failure:failure];
}
-(void)getServerDate:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self showProgressHUD];
    NSString *strURL = [NSString stringWithFormat:@"?route=feed/rest_api/getTime"];
    [self getWithURL:strURL success:success failure:failure];
}


-(void)getDeliveryCost:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self getWithURL:@"?route=rest/shipping_method/shippingmethods&info=1" success:success failure:failure];
}

-(void)getMaintain:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    [self postJsonWithURL:@"http://124.43.12.185:998/FCMexample/apiStatus.php" postBody:nil  success:success failure:failure];
}

@end

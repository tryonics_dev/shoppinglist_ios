//
//  SplashViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/14/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface SplashViewController : UIViewController
{
    NetworkManager *networkManager;
    NSUserDefaults *defaults ;
    NSString *sessionKey;
    LanguageManager *languageManager;
    

}
@property (weak,nonatomic)IBOutlet UIActivityIndicatorView *activityIndicator;
@end

NS_ASSUME_NONNULL_END

//
//  SplashViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/14/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "SplashViewController.h"
#import "MaintenanceStatus.h"

@interface SplashViewController ()

@end

@implementation SplashViewController
@synthesize activityIndicator;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    networkManager = [NetworkManager sharedManager];
    languageManager = [LanguageManager sharedManager];
    
    defaults = [NSUserDefaults standardUserDefaults];
    sessionKey = [defaults objectForKey:APP_SESSION_KEY];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
     [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [self getMaintain];
    /*if(sessionKey == nil || [sessionKey isEqualToString:@""])
    {
        [self getAppSession];
    }
    else
    {
        [self loadActiveCart];
    }*/
     //[myAppDelegate loadMainTab];
}
#pragma mark - Userdefine methods

-(void)showErrorAlertView
{
    [Utility showErrorAlertView:self.navigationController];
}

-(void)handleInitialScreen
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *selectedLanguage = [defaults objectForKey:SELECTED_LANGUAGE_KEY];
    if(selectedLanguage == nil || [selectedLanguage isEqualToString:@""])
    {
        [myAppDelegate loadLanguageSelectionController];
    }
    else
    {
        [myAppDelegate loadMainTab];
       
    }
}
#pragma mark - Network calls



-(void)getMaintain{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer =  [AFJSONRequestSerializer serializer];
    AFHTTPResponseSerializer * responseSerializer =[AFCompoundResponseSerializer serializer];
    [responseSerializer.acceptableContentTypes setByAddingObjectsFromArray:@[@"application/json"]];
    manager.responseSerializer = responseSerializer;
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"accept"];
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObjectsFromArray:@[@"application/json"]];
    
     NSDictionary *parameters = @{@"auth_key": @"ohg37yk0TlsbCIHJG0aSXCcBcA1LYGoQ"};

    [manager POST:@"http://124.43.12.185:998/FCMexample/apiStatus.php?" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:nil success:^(NSURLSessionDataTask *task, id responseData) {
        
        NSString *responsString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSData *jsonData=[responsString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        
        NSLog(@"error - %@",error.localizedDescription);
        NSLog(@"responseObject - %@",responseDic);
        
        BOOL success = [[responseDic objectForKey:@"success"] boolValue];
        
        if(success){
            
            MaintenanceStatus *status =  [[MaintenanceStatus alloc] initWithDictionary:[responseDic objectForKey:@"data"] error:&error];
            
            if(status.is_maintenance_on){
                NSLog(@"%@",status.message);
                [self showMaintenanceMessage : status.message];
            }else{
                if(self->sessionKey == nil || [self->sessionKey isEqualToString:@""])
                {
                    [self getAppSession];
                }
                else
                {
                    [self loadActiveCart];
                }
            }

        }else{
            
        }
        

        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"Error: %@", error);
        }];

}

-(void)showMaintenanceMessage : (NSString*) message
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
   
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:[languageManager languageStringForKey:@"Cancel"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                        exit(0);
                                   }];

    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}

-(id _Nonnull)getDataMain:(id)responseObject
{
    if(responseObject != nil)
    {
        NSString *responsString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *jsonData=[responsString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
        NSLog(@"error - %@",error.localizedDescription);
        NSLog(@"responseObject - %@",responseDic);
        return  responseDic;
    }
    return [[NSDictionary alloc] init];
}


-(void)getAppSession{
    
    [networkManager getAPISession:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            self->sessionKey = [[responseData objectForKey:@"data"] objectForKey:@"session"];
            self->networkManager.SESSION_KEY = self -> sessionKey;
            [self->defaults setObject:self->sessionKey forKey:APP_SESSION_KEY];
            [self->defaults synchronize];
            [self handleInitialScreen];
            [self->activityIndicator setHidden:YES];
        }
        else
        {
            [self->activityIndicator setHidden:YES];
            [self showErrorAlertView];
        }
    
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
    }];
}

-(void)loadActiveCart{
    
    [[CartManager sharedManager] getCart:^(bool success, CartModel * cartModel) {
        if(success)
        {
           [self handleInitialScreen];
        }
    }];
    
}

@end

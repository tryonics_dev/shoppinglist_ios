//
//  ProfileViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerModel.h"
#import "ProfileInfoTableViewCell.h"
#import "LanguageSelectionViewController.h"
#import "ChangePasswordViewController.h"
#import "AccountInfoViewController.h"
#import "AddressListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProfileViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    LanguageManager *languageManager;
}
@property(nonatomic,weak)IBOutlet UIButton *btnSignOut;
@property(nonatomic,weak)IBOutlet UILabel *lblName,*lblPhone;
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@end

NS_ASSUME_NONNULL_END

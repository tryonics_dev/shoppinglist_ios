//
//  CartViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartItemTableViewCell.h"
#import "EmptyCartViewController.h"


NS_ASSUME_NONNULL_BEGIN

@interface CartViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    LanguageManager *languageManager;
    EmptyCartViewController *emptyCartView;
    CartManager *cartManager;
    CartModel *cartModel;
    UIRefreshControl *refreshController;
}
@property (weak,nonatomic)IBOutlet UITableView *tableView;
@property (weak,nonatomic)IBOutlet UILabel *lblTotal;
@property (weak,nonatomic)IBOutlet UIButton *btnCheckOut;

@end

NS_ASSUME_NONNULL_END

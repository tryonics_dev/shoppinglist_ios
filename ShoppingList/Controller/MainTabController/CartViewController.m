//
//  CartViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "CartViewController.h"

@interface CartViewController ()

@end

@implementation CartViewController
@synthesize tableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initUI];
    [self getCart];
}

#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(cartModel!=nil)
        return cartModel.products.count;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *IdentifierContent = @"IdentifierContent"; //Set Identifier for cell
    CartItemTableViewCell *cell = (CartItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier: IdentifierContent];
    
    if (cell == nil) {
        NSArray *nib =nil;
        if(IS_IPHONE_5)
            nib= [[NSBundle mainBundle] loadNibNamed:@"CartItemTableViewCell"
                                                     owner:self options:nil];
        else
            nib = [[NSBundle mainBundle] loadNibNamed:@"CartItemLargeTableViewCell" owner:self options:nil];
        
        for (id oneObject in nib) if ([oneObject isKindOfClass:[CartItemTableViewCell class]])
            cell = (CartItemTableViewCell *)oneObject;
        
        cell.backgroundColor = [UIColor clearColor];
        
        [cell.btnUp addTarget:self action:@selector(increaseItem:) forControlEvents:UIControlEventTouchUpInside];
         [cell.btnDown addTarget:self action:@selector(decreaseItem:) forControlEvents:UIControlEventTouchUpInside];
    }
    cell.btnDown.tag= indexPath.row;
    cell.btnUp.tag = indexPath.row;
    CartItemModel *item = cartModel.products[indexPath.row];
    
    cell.lblTitle.text= item.name;
    cell.lblItemCount.text = item.quantity;
    cell.lblUnitPrice.text =[NSString stringWithFormat:@"%@ : %@",[languageManager languageStringForKey:@"Unit Price"],item.price] ;
    cell.lblTotal.text = item.total;
    [cell.imgThumb setShowActivityIndicatorView:YES];
    [cell.imgThumb sd_setImageWithURL:[NSURL URLWithString:item.thumb]
                        placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                                 options:SDWebImageRefreshCached];
    if(item.price_raw == 0)
    {
        [cell.lblUnitPrice setHidden:YES];
        [cell.lblTotal setHidden:YES];
        [cell.lblTotalTitle setHidden:YES];
        [cell.lblItemCount setHidden:YES];
        [cell.btnUp setHidden:YES];
        [cell.btnDown setHidden:YES];
    }
    return cell;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 125;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [languageManager languageStringForKey:@"Remove"];
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        CartItemModel *item = cartModel.products[indexPath.row];
        [self itemDeleteConfirmation:item.key];
    }
}

#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnCheckOut)
    {
        CartManager *cartManager = [CartManager sharedManager];
        [cartManager setCartModel:cartModel];
        [cartManager checkOutValidate:self];
    }
}
#pragma mark - Userdefine methods

-(void)initUI
{
    [TabTitleHandler handleTabTitle:self.tabBarController selectedIndex:2];
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"Cart"] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    [self.btnCheckOut shoppingOrangeRoundedCorner:[languageManager languageStringForKey:@"Checkout"]];
    
    cartManager = [CartManager sharedManager];
    
    [self.tableView setHidden:YES];
    [self.btnCheckOut setHidden:YES];
    [self.lblTotal setHidden:YES];
    
    emptyCartView = [[EmptyCartViewController alloc] initWithNibName:@"EmptyCartViewController" bundle:nil];
    emptyCartView.view.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
    emptyCartView.view.tag = 100;
    [emptyCartView.btnRetry addTarget:self action:@selector(continueShopping) forControlEvents:UIControlEventTouchUpInside];
    
   // [self.navigationController.view addSubview:self.lblTotal];
   // [self.navigationController.navigationBar.inputView addSubview:self.lblTotal];
    [self.navigationItem.titleView addSubview:self.lblTotal];
    [self.lblTotal setFrame: CGRectMake(SCREEN_WIDTH-230, 10, 200, 21)];
    
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor = APP_THEME_LIGHT_GREEN;
    [refreshController addTarget:self action:@selector(handlePullToRefresh:) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.refreshControl = refreshController;
}

-(void)handleEmptyView:(BOOL)isShow
{
    if(isShow)
    {
        [self.tableView setHidden:YES];
        [self.btnCheckOut setHidden:YES];
        [self.lblTotal setHidden:YES];
      
        BOOL viewFound = NO;
        for (id view in self.view.subviews) {
            if([view isKindOfClass:[UIView class]])
            {
                UIView *lblView = (UIView *)view;
                if(lblView.tag == 100)
                {
                    viewFound = YES;
                    break;
                }
            }
        }
        if(!viewFound)
        {
            [self.view addSubview:emptyCartView.view];
        }
    }
    else
    {
        [self.tableView setHidden:NO];
        [self.btnCheckOut setHidden:NO];
        [self.lblTotal setHidden:NO];
        
        for (id view in self.view.subviews) {
            if([view isKindOfClass:[UIView class]])
            {
                UIView *lblView = (UIView *)view;
                if(lblView.tag == 100)
                {
                    [lblView removeFromSuperview];
                   
                }
            }
        }
    }
}
-(void)continueShopping
{
    [myAppDelegate tabBarController].selectedIndex = 0;
}
-(void)setTotal
{
    for(CartAmountModel *amount in cartModel.totals)
    {
        if([amount.title.lowercaseString isEqualToString:@"total"])
        {
            self.lblTotal.text =[NSString stringWithFormat:@"%@ : %@",[languageManager languageStringForKey:@"Total"], amount.text] ;
        
            break;
        }
    }
}
-(void)increaseItem:(id)sender
{
    CartItemModel *item = cartModel.products[((UIButton *)sender).tag];
    NSInteger qty = [item.quantity integerValue]+1;
    [self updateCartItem:item.key quantity:qty];
}
-(void)decreaseItem:(id)sender
{
    CartItemModel *item = cartModel.products[((UIButton *)sender).tag];
    NSInteger qty = [item.quantity integerValue];
    if(qty>0)
    {
        qty = qty-1;
    }
    [self updateCartItem:item.key quantity:qty];
}
-(void)itemDeleteConfirmation:(NSString *)key
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:[languageManager languageStringForKey:@"Do you want to remove this item from the cart ?"]
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[languageManager languageStringForKey:@"Yes"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [self deleCartItem:key];
                               }];
    UIAlertAction* cancelButton = [UIAlertAction
                               actionWithTitle:[languageManager languageStringForKey:@"No"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark - Handle pull to refresh

-(void)handlePullToRefresh : (id)sender
{
    [self getCart];
}
#pragma mark - CartManger calls

-(void)getCart
{
    [[NetworkManager sharedManager] showProgressHUD];
    [cartManager getCart:^(bool success, CartModel * _Nonnull cartModel) {
        if(success)
        {
            if(cartModel !=nil)
            {
                self->cartModel = cartModel;
                [self handleEmptyView:NO];
                [self->tableView reloadData];
                [[myAppDelegate tabBarController].tabBar.items[2] setBadgeValue:[NSString stringWithFormat:@"%li",self->cartModel.total_product_count]];
                [self setTotal];
            }
            else
            {
                 [[myAppDelegate tabBarController].tabBar.items[2] setBadgeValue:nil];
                 [self handleEmptyView:YES];
                 [self->tableView reloadData];
            }
           
        }
        else
        {
            [self handleEmptyView:YES];
            [self->tableView reloadData];
        }
        [self->refreshController endRefreshing];
    }];
}
-(void)updateCartItem:(NSString *)key quantity:(NSInteger)quantity
{
    [cartManager updateCartItem:key quantity:[NSString stringWithFormat:@"%li",quantity] completionBlock:^(bool success) {
        if(success)
            [self getCart];
    }];
}
-(void)deleCartItem:(NSString *)key
{
    [cartManager deleteCartItem:key completionBlock:^(bool success) {
        if(success)
            [self getCart];
    }];
}
@end

//
//  ProfileViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initUI];
    [self loadProfileDetails];
}

#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileInfoTableViewCell *cell = (ProfileInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ProfileInfoTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row== 0)
    {
        [cell.lblTitle shoppingListProfilePhoneNumberStyle:[languageManager languageStringForKey:@"Account Info"]];
        [cell.imgLogo setImage:[UIImage imageNamed:@"accountIcon"]];
    }
    else if(indexPath.row== 1)
    {
        [cell.lblTitle shoppingListProfilePhoneNumberStyle:[languageManager languageStringForKey:@"Change Password"]];
        [cell.imgLogo setImage:[UIImage imageNamed:@"passowrdIcon"]];
    }
    else if(indexPath.row== 2)
    {
        [cell.lblTitle shoppingListProfilePhoneNumberStyle:[languageManager languageStringForKey:@"Address List"]];
        [cell.imgLogo setImage:[UIImage imageNamed:@"addressIcon"]];
    }
    else if(indexPath.row== 3)
    {
        [cell.lblTitle shoppingListProfilePhoneNumberStyle:[languageManager languageStringForKey:@"Change Language"]];
        [cell.imgLogo setImage:[UIImage imageNamed:@"languageLogoGreen"]];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (indexPath.row == 0)
    {
        AccountInfoViewController *ctrlAccount = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountInfoViewController"];
        [self.navigationController pushViewController:ctrlAccount animated:YES];
    }
    else if (indexPath.row == 1)
    {
        ChangePasswordViewController *ctrlPassword = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
         [self.navigationController pushViewController:ctrlPassword animated:YES];
    }
    else if (indexPath.row == 2)
    {
        AddressListViewController *ctrlAddress = [self.storyboard instantiateViewControllerWithIdentifier:@"AddressListViewController"];
          [self.navigationController pushViewController:ctrlAddress animated:YES];
    }
    else if(indexPath.row == 3)
    {
        LanguageSelectionViewController *ctrlLanguage = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageSelectionViewController"];
        ctrlLanguage.IsFromProfile = YES;
        [self.navigationController pushViewController:ctrlLanguage animated:YES];
    }

}
#pragma mark - Userdefine methods

-(void)initUI
{
    [TabTitleHandler handleTabTitle:self.tabBarController selectedIndex:4];
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"User Profile"] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    [self.btnSignOut shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Logout"]];
}
-(void)loadProfileDetails
{
    CustomerModel *customerModel = [CustomerModel getSavedCustomerObject];
    [self.lblName shoppingListProfileNameStyle:customerModel.fullName];
    [self.lblPhone shoppingListProfilePhoneNumberStyle:customerModel.phoneWithCountryCode ];
}


#pragma mark - Action method

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnSignOut)
    {
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@""
                                    message:[languageManager languageStringForKey:@"Are you sure you want to Logout?"]
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:[languageManager languageStringForKey:@"Yes"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [self logOutCustomer];
                                   }];
        UIAlertAction* cancelButton = [UIAlertAction
                                       actionWithTitle:[languageManager languageStringForKey:@"No"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }];
        [alert addAction:okButton];
        [alert addAction:cancelButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Network calls

-(void)logOutCustomer
{
    [[NetworkManager sharedManager] customerLogout:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            [myAppDelegate customerLogout];
        }else
        {
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
         [Utility showErrorAlertView:self.navigationController];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

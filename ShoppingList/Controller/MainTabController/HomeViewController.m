//
//  HomeViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

@synthesize tableView;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initUI];
    [self.tableView reloadData];
}

#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        static NSString *IdentifierContent = @"IdentifierContent"; //Set Identifier for cell
        HomeBannerTableViewCell *cell = (HomeBannerTableViewCell *)[tableView dequeueReusableCellWithIdentifier: IdentifierContent];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeBannerTableViewCell"
                                                         owner:self options:nil];
            for (id oneObject in nib) if ([oneObject isKindOfClass:[HomeBannerTableViewCell class]])
                cell = (HomeBannerTableViewCell *)oneObject;
            
            cell.backgroundColor = [UIColor clearColor];
            cell.navController = self.navigationController;
        }
        return cell;
    }
    else
    {
        static NSString *IdentifierContent = @"IdentifierContent"; //Set Identifier for cell
        HomeTabTableViewCell *cell = (HomeTabTableViewCell *)[tableView dequeueReusableCellWithIdentifier: IdentifierContent];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeTabTableViewCell"
                                                    owner:self options:nil];
            for (id oneObject in nib) if ([oneObject isKindOfClass:[HomeTabTableViewCell class]])
                cell = (HomeTabTableViewCell *)oneObject;
            
            cell.backgroundColor = [UIColor clearColor];
            cell.navController = self.navigationController;
        }
        return cell;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
        return ( SCREEN_WIDTH / 2) + 44;
    else
        return 580;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager = [LanguageManager sharedManager];
    [TabTitleHandler handleTabTitle:self.tabBarController selectedIndex:0];

    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor = APP_THEME_LIGHT_GREEN;
    [refreshController addTarget:self action:@selector(handlePullToRefresh:) forControlEvents:UIControlEventValueChanged];
    
    tableView.refreshControl = refreshController;
    
    [self.navigationController.navigationBar setHidden:YES];
    (self.navigationItem).backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController initShoppingListStyle];
    UIGraphicsBeginImageContext(self.view.frame.size);
    
    [[UIImage imageNamed:@"background"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [myAppDelegate setStatusBarBackgroundColor:[UIColor colorWithPatternImage:image]];
   // self.viewTop.backgroundColor = [UIColor colorWithPatternImage:image];
}

#pragma mark - Handle pull to refresh

-(void)handlePullToRefresh : (id)sender
{
    [self.tableView reloadData];
    [refreshController endRefreshing];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

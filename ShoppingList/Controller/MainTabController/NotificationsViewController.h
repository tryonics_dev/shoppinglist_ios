//
//  NotificationsViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationModel.h"
#import "NotificationTableViewCell.h"
#import "ProductDetailViewController.h"
#import "EmptyCartViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface NotificationsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    LanguageManager *languageManager;
    NotificationListModel *notificationListModel;
    UIRefreshControl *refreshController;
    EmptyCartViewController *emptyCartView;
}

@property (weak , nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END

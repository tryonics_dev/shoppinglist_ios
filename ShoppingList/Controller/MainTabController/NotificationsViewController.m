//
//  NotificationsViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "NotificationsViewController.h"

@interface NotificationsViewController ()

@end

@implementation NotificationsViewController
@synthesize tableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initStyles];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self getNotification];
}
#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return notificationListModel.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *IdentifierContent = @"IdentifierContent"; //Set Identifier for cell
    NotificationTableViewCell *cell = (NotificationTableViewCell *)[tableView dequeueReusableCellWithIdentifier: IdentifierContent];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationTableViewCell"
                                                     owner:self options:nil];
        for (id oneObject in nib) if ([oneObject isKindOfClass:[NotificationTableViewCell class]])
            cell = (NotificationTableViewCell *)oneObject;
        
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    }
    NotificationModel *notification = notificationListModel.data[indexPath.row];
    cell.lblTitle.text = notification.message;
    cell.txtDescription.text = notification.message;
    NSString *strOfferDates = [NSString stringWithFormat:@"%@ %@ %@ %@",[languageManager languageStringForKey:@"Offer valid from"],[Utility formatDate:notification.start_date],[languageManager languageStringForKey:@"to"],[Utility formatDate:notification.end_date] ];
    cell.lblDate.text = strOfferDates;
    
    cell.lblProductTitle.text = notification.product_name;
    
    [cell.imgProduct setShowActivityIndicatorView:YES];
    [cell.imgProduct sd_setImageWithURL:[NSURL URLWithString:notification.image]
                     placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                              options:SDWebImageRefreshCached];
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NotificationModel *notification = notificationListModel.data[indexPath.row];
    ProductDetailViewController *ctrlProduct = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
    ctrlProduct.productId = notification.product_id;
    [self.navigationController pushViewController:ctrlProduct animated:YES];
}

#pragma mark - Userdefine methods

-(void)initStyles
{
    [TabTitleHandler handleTabTitle:self.tabBarController selectedIndex:3];
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"Notifications"] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor = APP_THEME_LIGHT_GREEN;
    [refreshController addTarget:self action:@selector(getNotification) forControlEvents:UIControlEventValueChanged];
    
    tableView.refreshControl = refreshController;
    
    emptyCartView = [[EmptyCartViewController alloc] initWithNibName:@"EmptyCartViewController" bundle:nil];
    emptyCartView.view.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
    emptyCartView.view.tag = 100;
    [emptyCartView.btnRetry addTarget:self action:@selector(continueShopping) forControlEvents:UIControlEventTouchUpInside];
    emptyCartView.lblMessage.text = [languageManager languageStringForKey:@"No Promotions available"];
}
-(void)handleEmptyView:(BOOL)isShow
{
    if(isShow)
    {
        [self.tableView setHidden:YES];
        BOOL viewFound = NO;
        for (id view in self.view.subviews) {
            if([view isKindOfClass:[UIView class]])
            {
                UIView *lblView = (UIView *)view;
                if(lblView.tag == 100)
                {
                    viewFound = YES;
                    break;
                }
            }
        }
        if(!viewFound)
        {
            [self.view addSubview:emptyCartView.view];
        }
    }
    else
    {
        [self.tableView setHidden:NO];
        for (id view in self.view.subviews) {
            if([view isKindOfClass:[UIView class]])
            {
                UIView *lblView = (UIView *)view;
                if(lblView.tag == 100)
                {
                    [lblView removeFromSuperview];
                    
                }
            }
        }
    }
}
-(void)continueShopping
{
    [myAppDelegate tabBarController].selectedIndex = 0;
}

#pragma mark - Network calls

-(void)getNotification
{
    [[NetworkManager sharedManager] getNotifications:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success)
        {
            NSError *error;
            self->notificationListModel = [[NotificationListModel alloc] initWithDictionary:responseData error:&error];
            [self->tableView reloadData ];
            [self handleEmptyView:NO];
            if(self->notificationListModel.data.count == 0)
                [self handleEmptyView:YES];
        }
        else
        {
            [self handleEmptyView:YES];
        }
        [self->refreshController endRefreshing];
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
        [self->refreshController endRefreshing];
         [self handleEmptyView:YES];
    }];
}
@end

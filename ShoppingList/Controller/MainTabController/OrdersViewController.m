//
//  OrdersViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "OrdersViewController.h"

@interface OrdersViewController ()

@end

@implementation OrdersViewController
@synthesize tableView,arrOrders,refreshController,bottomRefreshControl;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:ORDER_STATUS_UPDATED_NOTIFICATION
                                               object:nil];
    
    self.tableView.estimatedRowHeight = 0;
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor = APP_THEME_LIGHT_GREEN;
    [refreshController addTarget:self action:@selector(handlePullToRefresh:) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.refreshControl = refreshController;
    
    bottomRefreshControl = [[UIRefreshControl alloc] init];
    bottomRefreshControl.tintColor = APP_THEME_LIGHT_GREEN;
    [bottomRefreshControl addTarget:self action:@selector(loadMoreOrders) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.bottomRefreshControl = bottomRefreshControl;
    arrOrders = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initUI];
    currentPage = 1;
    if(arrOrders == nil)
         arrOrders = [[NSMutableArray alloc] init];
    else{
        [arrOrders removeAllObjects];
        [self.tableView reloadData];
    }
    
    [self getOrders];
}
- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - Local notification receiver

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:ORDER_STATUS_UPDATED_NOTIFICATION])
    {
        [self handlePullToRefresh:nil];
    }
}
#pragma mark - Userdefine methods

-(void)initUI
{
    [TabTitleHandler handleTabTitle:self.tabBarController selectedIndex:1];
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"Orders"] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    
    emptyCartView = [[EmptyCartViewController alloc] initWithNibName:@"EmptyCartViewController" bundle:nil];
    emptyCartView.view.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
    emptyCartView.view.tag = 100;
    [emptyCartView.btnRetry addTarget:self action:@selector(continueShopping) forControlEvents:UIControlEventTouchUpInside];
    emptyCartView.lblMessage.text = [languageManager languageStringForKey:@"Sorry! No orders Available"];
    
}
-(void)handleEmptyView:(BOOL)isShow
{
    if(isShow)
    {
        [self.tableView setHidden:YES];
        BOOL viewFound = NO;
        for (id view in self.view.subviews) {
            if([view isKindOfClass:[UIView class]])
            {
                UIView *lblView = (UIView *)view;
                if(lblView.tag == 100)
                {
                    viewFound = YES;
                    break;
                }
            }
        }
        if(!viewFound)
        {
            [self.view addSubview:emptyCartView.view];
        }
    }
    else
    {
        [self.tableView setHidden:NO];
        for (id view in self.view.subviews) {
            if([view isKindOfClass:[UIView class]])
            {
                UIView *lblView = (UIView *)view;
                if(lblView.tag == 100)
                {
                    [lblView removeFromSuperview];
                    
                }
            }
        }
    }
}
-(void)continueShopping
{
    [myAppDelegate tabBarController].selectedIndex = 0;
}
-(void)orderCancelConfirmation:(id)sender
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:[languageManager languageStringForKey:@"Are you sure, Do you want to cancel this order?"]
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[languageManager languageStringForKey:@"Yes"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [self cancelOrder:((UIButton *)sender).tag];
                               }];
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:[languageManager languageStringForKey:@"No"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)viewOrderReceipt:(id)sender
{
    
}

#pragma mark - Handle pull to refresh

-(void)handlePullToRefresh : (id)sender
{
    [self.arrOrders removeAllObjects];
    [self.tableView reloadData];
    currentPage = 1;
    [self getOrders];
}
-(void)loadMoreOrders
{
    currentPage++;
    [self getOrders];
}
#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrOrders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    OrderListTableViewCell *cell = (OrderListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"OrderListTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    OrderItemModel *orderItem = self.arrOrders[indexPath.row];
    cell.lblDate.text =[Utility formatDateToOrderList:orderItem.date_added];
    cell.lblStatus.text = orderItem.status;
    cell.lblOrderId.text =[NSString stringWithFormat:@"%@ : %@", [languageManager languageStringForKey:@"Order ID"],orderItem.order_id];
    cell.lblDeliveredTo.text = orderItem.name;
    cell.lblTotal.text = orderItem.total;
    cell.lblProductCount.text = [NSString stringWithFormat:@"%@ : %li",[languageManager languageStringForKey:@"Product count"],(long)orderItem.products];
    
    cell.lblStatus.textColor = [UIColor darkGrayColor];
    
    cell.btnCancel.tag = [orderItem.order_id integerValue];
    cell.btnReOrder.tag = [orderItem.order_id integerValue];
    
    if(orderItem.order_status_id == ORDER_PENDING_STATUS ||
       orderItem.order_status_id == ORDER_PREPARE_STATUS ||
       orderItem.order_status_id == ORDER_DISPATCH_STATUS ||
       orderItem.order_status_id == ORDER_ON_ROUTE_STATUS ||
       orderItem.order_status_id == ORDER_READY_DELIVER_STATUS)
    {
        cell.viewDate.backgroundColor = [myAppDelegate colourBackground];
        
//        [cell.btnCancel shoppingClearRoundedCorner:[languageManager languageStringForKey:@"Cancel"]];
//        [cell.btnReOrder shoppingDisableRoundedCorner:[languageManager languageStringForKey:@"TRACK"]];
//
//        if(orderItem.order_status_id == ORDER_DISPATCH_STATUS ||
//           orderItem.order_status_id == ORDER_ON_ROUTE_STATUS)
//        {
//             [cell.btnReOrder shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"TRACK"]];
//        }
        
         [cell.btnReOrder shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"TRACK"]];
         [cell.btnCancel shoppingClearRoundedCorner:[languageManager languageStringForKey:@"View Receipt"]];
    }
    else
    {
        cell.viewDate.backgroundColor = [UIColor lightGrayColor];
        
//        [cell.btnCancel shoppingTransparentDisableRoundedCorner:[languageManager languageStringForKey:@"Cancel"]];
//        [cell.btnReOrder shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"REORDER"]];
        
        [cell.btnReOrder shoppingDisableRoundedCorner:[languageManager languageStringForKey:@"TRACK"]];
        [cell.btnCancel shoppingClearRoundedCorner:[languageManager languageStringForKey:@"View Receipt"]];
        
        if(orderItem.order_status_id == ORDER_RETURN_STATUS)
        {
            cell.lblStatus.textColor = [UIColor redColor];
        }
        else if (orderItem.order_status_id == ORDER_CANCEL_STATUS)
        {
            cell.lblStatus.textColor = [UIColor orangeColor];
        }
    }
    
    return cell;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 280;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

#pragma mark - Network calls

-(void)getOrders
{
    [[NetworkManager sharedManager] getOderList:currentPage  success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            NSError *error;
            OrderItemListModel *orderItemList = [[OrderItemListModel alloc] initWithDictionary:responseData error:&error];
            [self.arrOrders addObjectsFromArray:orderItemList.data];
            [self->tableView reloadData ];
            if(self.arrOrders.count>0)
                [self handleEmptyView:NO];
            else
                [self handleEmptyView:YES];
            
        }
        else
        {
             [self handleEmptyView:YES];
        }
         [self->refreshController endRefreshing];
         [self->bottomRefreshControl endRefreshing];
        
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [self->refreshController endRefreshing];
        [self->bottomRefreshControl endRefreshing];
        if(self.arrOrders.count==0)
            [self handleEmptyView:YES];
    }];
}

-(void)cancelOrder:(NSInteger)orderId
{
    
}

#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
   
    if([segue.identifier isEqualToString:@"viewReceipt"])
    {
        ReceiptViewController *ctrlReceipt = [segue destinationViewController];
        ctrlReceipt.orderId = ((UIButton *)sender).tag;
    }
    else if ([segue.identifier isEqualToString:@"tracker"])
    {
        OrderTrackingViewController *ctrlTracker = [segue destinationViewController];
        ctrlTracker.orderId =  ((UIButton *)sender).tag;
    }
}
@end

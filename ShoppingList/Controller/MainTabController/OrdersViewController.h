//
//  OrdersViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListTableViewCell.h"
#import "OrderItemListModel.h"
#import "ReceiptViewController.h"
#import "OrderTrackingViewController.h"
#import "EmptyCartViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrdersViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    LanguageManager *languageManager;
    EmptyCartViewController *emptyCartView;
    NSInteger currentPage;
}
@property (weak,nonatomic)IBOutlet UITableView *tableView;


@property (strong, nonatomic)NSMutableArray *arrOrders;
@property (strong, nonatomic) UIRefreshControl *refreshController,*bottomRefreshControl;
@end

NS_ASSUME_NONNULL_END

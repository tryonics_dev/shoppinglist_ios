//
//  HomeViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTabTableViewCell.h"
#import "HomeBannerTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    LanguageManager *languageManager;
    UIRefreshControl *refreshController;
   
}
@property (nonatomic ,weak)IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END

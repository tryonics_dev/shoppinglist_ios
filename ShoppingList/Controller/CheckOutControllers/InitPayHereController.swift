//
//  InitPayHereController.swift
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/1/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

import Foundation

public class InitPayHereController : NSObject {
    
    var merchantId : String?
    var merchantSecret : String?
    var isSandBoxEnabled : Bool = false
    
    @objc var orderId : String?
    @objc var items : [Item]?
    @objc var customer : Customer?
    @objc var totalAmount : Double = 0
    
    public override init() {
        
        let path = Bundle.main.path(forResource: "APIConfig", ofType: "plist")
        let dictAPIConfig = NSDictionary(contentsOfFile: path ?? "")
        merchantId = dictAPIConfig?["PAYHERE_MERCHANT_ID"] as? String
        merchantSecret  = dictAPIConfig?["PAYHERE_MERCHANT_SECRET"] as? String
        isSandBoxEnabled = dictAPIConfig?["PAYHERE_SANDBOX_ENABLED"] as! Bool
        
    }
    @objc public func phViewController() -> PHViewController
    {
        let req : InitRequest = InitRequest()
        req.merchantId = merchantId
        req.merchantSecret = merchantSecret
        req.amount = totalAmount
        req.currency = "LKR"
        req.orderId = orderId;
        req.itemsDescription = "ShoppingList Item(s)"
//        req.custom1 = "This is the custom 1 message"
//        req.custom2 = "This is the custom 2 message"
        
        req.items = items
        req.customer = customer
        
        let phVC = PHViewController()
        phVC.initRequest = req
        phVC.isSandBoxEnabled = isSandBoxEnabled
        phVC.modalPresentationStyle = .overFullScreen
        
        return phVC
    }
}

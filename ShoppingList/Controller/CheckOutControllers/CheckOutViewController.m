//
//  CheckOutViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/31/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "CheckOutViewController.h"
#import "payHereSDK.h"
#import "ShoppingList-Swift.h"
#import "DeliveryCostModel.h"

@interface CheckOutViewController ()<PHViewControllerDelegate>

@end

@implementation CheckOutViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    checkoutHandler = [[CheckoutHandler alloc] initWithDelegate:self];
    networkManager = [NetworkManager sharedManager];
    
    [self getAddressList];
    [self getShippingCostRequest];
    self.isPayHereSelected = NO;
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initUI];
    
   
}
#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager = [LanguageManager sharedManager];

    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"Checkout"] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    
    [self.btnCancel shoppingClearRoundedCorner:[languageManager languageStringForKey:@"Cancel"]];
    [self.btnConfirm shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Confirm"]];
    [self.btnEditBillingAddress setTitle:[languageManager languageStringForKey:@"Edit"] forState:UIControlStateNormal];
    [self.btnEditDeliveryAddress setTitle:[languageManager languageStringForKey:@"Edit"] forState:UIControlStateNormal];
    
    self.lblTitleOne.text = [NSString stringWithFormat:@"%@ 1 : %@",[languageManager languageStringForKey:@"Step"],[languageManager languageStringForKey:@"Billing Address"]];
    self.lblTitleTwo.text = [NSString stringWithFormat:@"%@ 2 : %@",[languageManager languageStringForKey:@"Step"],[languageManager languageStringForKey:@"Delivery Address"]];
    self.lblTitleThree.text = [NSString stringWithFormat:@"%@ 3 : %@",[languageManager languageStringForKey:@"Step"],[languageManager languageStringForKey:@"Payment Method"]];
    
    [self.radioCashOnDelivery setTitle:[languageManager languageStringForKey:@"Cash on Delivery"] forState:UIControlStateNormal];
    [self.radioPayHere setTitle:[languageManager languageStringForKey:@"Online payment"] forState:UIControlStateNormal];
    
    if([[CartManager sharedManager]deliveryAddressModel] != nil && [self isValidlandMark:[[CartManager sharedManager]deliveryAddressModel].landmark]){
        [self setAddress:[[CartManager sharedManager]deliveryAddressModel] textView:self.txtDeliveryAddress];
    }
    if([[CartManager sharedManager]billingAddressModel] != nil && [self isValidlandMark:[[CartManager sharedManager]billingAddressModel] .landmark]){
        [self setAddress:[[CartManager sharedManager]billingAddressModel] textView:self.txtBillingAddress];
    }
    
    if(IS_IPHONE_5){
        self.constrainTop.constant = 10;
        self.constrainButtonTop.constant = 7;
    }

    self.viewProgress.layer.cornerRadius = VIEW_CORNER_RADIUS ;
    self.viewProgress.layer.borderWidth = 1.0f;
    self.viewProgress.layer.borderColor = APP_THEME_LIGHT_GREEN.CGColor;
    self.viewProgress.layer.masksToBounds = YES;
    
    [self.viewProgressBackground setFrame:self.view.frame];
    self.viewProgressBackground.center = self.view.center;
}
-(void)setAddress:(AddressModel *)addressModel textView:(UITextView *)textView
{
    if(addressModel!= nil){
        NSMutableString *address = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@ %@, ",addressModel.firstname,addressModel.lastname]];
        if(![addressModel.address_1 isEqualToString:@""])
             [address appendFormat:@"%@",addressModel.address_1];
        if(![addressModel.address_2 isEqualToString:@""])
            [address appendFormat:@", %@",addressModel.address_2];
        if(![addressModel.city isEqualToString:@""])
            [address appendFormat:@", %@",addressModel.city];
        if(![addressModel.postcode isEqualToString:@""])
            [address appendFormat:@", %@",addressModel.postcode];
        if(![addressModel.country isEqualToString:@""])
            [address appendFormat:@", %@",addressModel.country];

        textView.text = address;
        
        if (textView.contentSize.height > textView.frame.size.height) {
            int fontIncrement = 1;
            while (textView.contentSize.height > textView.frame.size.height) {
                textView.font =[UIFont fontWithName:@"SFUIDisplay-Regular" size:14-fontIncrement]; 
                fontIncrement++;
            }
        }
        
    }
    else
    {
       textView.text = [languageManager languageStringForKey:@"Please select your address"];
    }
}
-(void)showOrderProgress
{
    [self.imgProgress setImage:[UIImage imageNamed:@"loaderImage"]];
    self.lblProgressTitle.text = [languageManager languageStringForKey:@"Please wait..."];
    self.lblProgressMessage.text = [languageManager languageStringForKey:@"We are processing your order."];
    [self.navigationController.view addSubview:self.viewProgressBackground];
    [self startAnimating];
}
-(void)showOrderError:(NSString *)message
{
    [self stopAnimating];
    [self.imgProgress setImage:[UIImage imageNamed:@"orderFailedIcon"]];
    self.lblProgressTitle.text = [languageManager languageStringForKey:@"Order Failed"];
    if([message isEqualToString:@""])
        self.lblProgressMessage.text = [languageManager languageStringForKey:@"Sorry! Your request couldn't be processed. Please try again later."];
    else
        self.lblProgressMessage.text = message;
    
    [self performSelector:@selector(hideOrderProgress) withObject:nil afterDelay:3];
}
-(void)hideOrderProgress
{
    [self.viewProgressBackground removeFromSuperview];
}
-(void)dismissCheckoutController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)loadPayHereController
{
    InitPayHereController *initController = [[InitPayHereController alloc] init];
    initController.orderId = self.strOrderId;
    initController.totalAmount = self.cartModel.total_raw;
    NSMutableArray *arrItems = [[NSMutableArray alloc] init];
    for (CartItemModel *cartItem in self.cartModel.products) {
        Item *item = [[Item alloc] initWithId:[NSString stringWithFormat:@"%ld",(long)cartItem.product_id] name:cartItem.name quantity:[cartItem.quantity intValue]];
        [arrItems addObject:item];
    }
    initController.items = arrItems;
    
    CustomerModel *customerModel = [CustomerModel getSavedCustomerObject];
    Customer *customer = [[Customer alloc] init];
    customer.firstName = customerModel.firstname;
    customer.lastName = customerModel.lastname;
    customer.email = customerModel.email;
    customer.phone = customerModel.telephone;
    
    AddressModel *billingAddress=[[CartManager sharedManager]billingAddressModel];
    Address *bAddress = [[Address alloc] init];
    bAddress.address = [NSString stringWithFormat:@"%@ %@",billingAddress.address_1,billingAddress.address_2];
    bAddress.city = billingAddress.city;
    bAddress.country = billingAddress.country;
    
    AddressModel *deliveryAddress= [[CartManager sharedManager]deliveryAddressModel];
    Address *dAddress = [[Address alloc] init];
    dAddress.address = [NSString stringWithFormat:@"%@ %@",deliveryAddress.address_1,deliveryAddress.address_2];
    dAddress.city = deliveryAddress.city;
    dAddress.country = deliveryAddress.country;
    
    customer.address = bAddress;
    customer.deliveryAddress = dAddress;
    
    initController.customer = customer;
    PHViewController *phVC = [initController phViewController];
    phVC.delegate =self;
    [self presentViewController:phVC animated:YES completion:nil];
}
-(void)checkoutOrder
{
    AddressModel *billingAddress=[[CartManager sharedManager]billingAddressModel];
    checkoutHandler.paymentAddressId = (int)billingAddress.address_id;
    checkoutHandler.billingAddressLandMark = billingAddress.landmark;
    checkoutHandler.billingdAddressLatitude = [NSString stringWithFormat:@"%f",billingAddress.landmark_lat];
    checkoutHandler.billingAddressLongitude =  [NSString stringWithFormat:@"%f",billingAddress.landmark_lon];
    
    AddressModel *deliveryAddress= [[CartManager sharedManager]deliveryAddressModel];
    checkoutHandler.shippingAddressId = (int)deliveryAddress.address_id;
    checkoutHandler.shippingAddressLandMark = deliveryAddress.landmark;
    checkoutHandler.shippingdAddressLatitude = [NSString stringWithFormat:@"%f",deliveryAddress.landmark_lat];
    checkoutHandler.shippingAddressLongitude = [NSString stringWithFormat:@"%f",deliveryAddress.landmark_lon];
    
    checkoutHandler.shippingMethod = @"flat.flat";
    checkoutHandler.paymentMethod = self.isPayHereSelected ? @"payhere" : @"cod";
    
    [self showOrderProgress];
    [checkoutHandler startOrderOverview];
}
-(BOOL)isValidlandMark:(NSString *)landmark
{
    if([landmark isEqualToString:@""] || landmark == NULL)
        return NO;
    else
        return YES;
}
#pragma mark - PHViewControllerDelegate

-(void)onResponseReceivedWithResponse:(PHResponse *)response
{
    NSLog(@"%@ , %i " , response.getMessage ,response.isSuccess);
    if(response.isSuccess)
    {
        [self showOrderProgress];
        [checkoutHandler confirmOrder];
    }
    else
    {
        [self showOrderError:response.getMessage];
    }
}

#pragma mark  - Progress animations

- (void)startAnimating {

    CGFloat toValue = YES ? M_PI * 2 : -M_PI * 2;
    
    [self rotateView:self.imgProgress from:0.0 to:toValue duration:2.0 repeatCount:HUGE_VALF forKey:@"rotation" removeAnimationOnCompletion:NO];
}
- (void)stopAnimating {
 
    [self.imgProgress.layer removeAnimationForKey:@"rotation"];
}

- (void)rotateView:(UIView *)view from:(CGFloat)fromValue to:(CGFloat)toValue duration:(CGFloat)duration repeatCount:(CGFloat)repeatCount forKey:(NSString *)key removeAnimationOnCompletion:(BOOL)removeOnCompletion {
    CABasicAnimation *rotationAnimation = [CABasicAnimation animation];
    rotationAnimation.keyPath = @"transform.rotation.z";
    rotationAnimation.fromValue = @(fromValue);
    rotationAnimation.toValue = @(toValue);
    rotationAnimation.duration = duration;
    rotationAnimation.repeatCount = repeatCount;
    rotationAnimation.removedOnCompletion = removeOnCompletion;
    rotationAnimation.fillMode = kCAFillModeForwards;
    [view.layer addAnimation:rotationAnimation forKey:key];
}
#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender== self.btnConfirm)
    {
       if([[CartManager sharedManager]deliveryAddressModel] != nil && [[CartManager sharedManager]billingAddressModel] != nil)
       {
           [self checkoutOrder];
       }
       else
       {
           [Utility showMessageAlertView:self.navigationController message:[languageManager languageStringForKey:@"Please check your Billing Address and Delivery Address"]];
       }
    }
    else if(sender == self.btnCancel)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (sender ==self.btnEditBillingAddress)
    {
        AddressListViewController *ctrlAddress = [self.sBoard instantiateViewControllerWithIdentifier:@"AddressListViewController"];
        ctrlAddress.isDeliveryAddressEdit = NO;
        ctrlAddress.isBillingAddressEdit = YES;
        [self.navigationController pushViewController:ctrlAddress animated:YES];
    }
    else if (sender == self.btnEditDeliveryAddress)
    {
        AddressListViewController *ctrlAddress = [self.sBoard instantiateViewControllerWithIdentifier:@"AddressListViewController"];
        ctrlAddress.isDeliveryAddressEdit = YES;
        ctrlAddress.isBillingAddressEdit = NO;
        [self.navigationController pushViewController:ctrlAddress animated:YES];
    }
    else if (sender == self.radioPayHere)
    {
        self.isPayHereSelected = YES;
    }
    else if (sender == self.radioCashOnDelivery)
    {
        self.isPayHereSelected = NO;
    }
}

#pragma mark - Checkout handler delegates

-(void)isOrderOverviewSuccess:(BOOL)isOverviewSuccess message:(NSString *)message
{
    if(isOverviewSuccess){
        if(self.isPayHereSelected)
        {
            [self hideOrderProgress];
            [self loadPayHereController];
        }
        else
        {
            [checkoutHandler confirmOrder];
        }
    }
    else
    {
        [self showOrderError:message];
    }
}
-(void)isOrderConfirmSuccess:(BOOL)isConfirmSuccess message:(NSString *)message
{
    [self stopAnimating];
    if(isConfirmSuccess)
    {
        [self.imgProgress setImage:[UIImage imageNamed:@"orderSuccessIcon"]];
        self.lblProgressTitle.text = [languageManager languageStringForKey:@"Order Success"];
        self.lblProgressMessage.text = [languageManager languageStringForKey:@"Your order has been placed."];
       [self performSelector:@selector(dismissCheckoutController) withObject:nil afterDelay:3];
    }else
    {
        [self showOrderError:@""];
    }
}
-(void)didOrderOverViewConfirm:(NSString *)orderId
{
    self.strOrderId = orderId;
}
-(void)orderOverviewStatus:(NSString *)statusMessage
{
    self.lblProgressMessage.text = statusMessage;
}
#pragma mark - Network calls

-(void)getAddressList
{
    [networkManager getCustomerAddressList:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            NSError *error;
            AddressListModel *addressList = [[AddressListModel alloc] initWithDictionary:responseData error:&error];
                for (AddressModel *addressModel in addressList.data) {
                    if(addressModel.is_default){
                        if([self isValidlandMark:addressModel.landmark]){
                            [[CartManager sharedManager] setDeliveryAddress:addressModel];
                            [[CartManager sharedManager] setBillingAddress:addressModel];
                            [self setAddress:addressModel textView:self.txtBillingAddress];
                            [self setAddress:addressModel textView:self.txtDeliveryAddress];
                            break;
                        }
                    }
                }
        }
       
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
    
    }];
}


#pragma mark - Network calls

-(void)getShippingCostRequest
{
    
    [networkManager getDeliveryCost:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            
            NSString *deliveryCost = [responseData valueForKeyPath:@"data.cost"];
            self.lblDeliveryCost.text = [NSString stringWithFormat:@"Rs. %@", deliveryCost];
    
             }

    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
           // NSLog(@"ERRRRRRRRRRRR %@", failureReason);
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

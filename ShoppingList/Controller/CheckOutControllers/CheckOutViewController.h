//
//  CheckOutViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/31/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressListViewController.h"
#import "DLRadioButton.h"
#import "AddressModel.h"
#import "UIImage+GIF.h"
#import "CheckoutHandler.h"

NS_ASSUME_NONNULL_BEGIN

@interface CheckOutViewController : UIViewController<CheckoutHandlerDelegate>
{
    LanguageManager *languageManager;
    NetworkManager *networkManager;
    CheckoutHandler *checkoutHandler;
}
@property(nonatomic,weak)IBOutlet UIButton
*btnConfirm,
*btnCancel,
*btnEditBillingAddress,
*btnEditDeliveryAddress;
@property(nonatomic,weak)IBOutlet UILabel
*lblTitleOne,
*lblTitleTwo,
*lblTitleThree,
*lblDeliveryCost;
@property(nonatomic,weak)IBOutlet UITextView
*txtBillingAddress,
*txtDeliveryAddress;
@property(nonatomic,weak)IBOutlet NSLayoutConstraint
*constrainTop,
*constrainButtonTop;
@property(nonatomic,weak)IBOutlet DLRadioButton
*radioPayHere,
*radioCashOnDelivery;

@property(nonatomic,strong)IBOutlet UIView *viewProgressBackground;
@property(nonatomic,weak)IBOutlet UIView *viewProgress;
@property(nonatomic,weak)IBOutlet UILabel *lblProgressTitle;
@property(nonatomic,weak)IBOutlet UILabel *lblProgressMessage;
@property(nonatomic,weak)IBOutlet UIImageView *imgProgress;

@property(nonatomic,strong)UIStoryboard *sBoard;
@property(nonatomic)BOOL isPayHereSelected;
@property(nonatomic,strong)NSString *strOrderId;
@property(nonatomic,strong)CartModel *cartModel;


@end

NS_ASSUME_NONNULL_END

//
//  AddNewAddressViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/28/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddNewAddressTableViewCell.h"
#import "GooglePlacePicker/GooglePlacePicker.h"
#import "AddressModel.h"
#import "CustomerModel.h"

NS_ASSUME_NONNULL_BEGIN

@import GooglePlaces;

@interface AddNewAddressViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,GMSAutocompleteViewControllerDelegate>
{
    LanguageManager *languageManager;
    GMSAutocompleteFilter *_filter;
    double closestPlaceLat;
    double closestPlaceLon;
}
@property(nonatomic,weak)IBOutlet UITableView *tableview;
@property (nonatomic ,strong)UITapGestureRecognizer *tapRec;

@property (weak , nonatomic) UILabel
*lblTitleFirstName,
*lblErrorFirstName,
*lblTitleLastName,
*lblErrorLastName,
*lblErrorCompany,
*lblErrorAddress1,
*lblErrorAddress2,
*lblErrorCity,
*lblErrorPostCode,
*lblErrorClosestLocation,
*lblTitleCompany,
*lblTitleAddress1,
*lblTitleAddress2,
*lblTitleCity,
*lblTitlePostCode,
*lblTitleClosestLocation;

@property (weak,nonatomic) UITextField *txtFirstName,
*txtLastName,*txtCompany,*txtAddress1,*txtAddress2,*txtCity,*txtPostCode,*txtClosestLocation;

@property(nonatomic,weak) UIButton *btnDefault;

@property(nonatomic)BOOL isDefaultAddress,isEditAddress;
@property(nonatomic)AddressModel *addressModel;
@end

NS_ASSUME_NONNULL_END

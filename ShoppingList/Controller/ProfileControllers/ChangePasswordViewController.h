//
//  ChangePasswordViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/26/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerModel.h"
#import "SignInViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate>
{
     LanguageManager *languageManager;
}
@property(nonatomic,weak)IBOutlet UILabel
*lblName,
*lblPhone,*lblTitlePassword,
*lblErrorPassword,
*lblTitleConfirmPassword,
*lblErrorConfirmPassword;
@property (weak,nonatomic)IBOutlet UITextField
*txtPassword,
*txtConfirmPassword;
@property(nonatomic,weak)IBOutlet UIButton *btnSave,*btnViewPassword,*btnViewConfirmPassword;
@property(nonatomic,weak)IBOutlet UIImageView *imgTick,*imgLogo;
@property(nonatomic)BOOL isFromForgotPassword;
@property(nonatomic,strong)NSString *strTelephoneNumber;
@end

NS_ASSUME_NONNULL_END

//
//  AddressListViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/28/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "AddressListViewController.h"

@interface AddressListViewController ()

@end

@implementation AddressListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self loadProfileDetails];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getAddressList];
}
#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"Address List"] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    self.navigationItem.rightBarButtonItems =@[[[UIBarButtonItem alloc]initWithCustomView:self.btnAddNew]];
    
    self.lblNoAddress.text = [languageManager languageStringForKey:@"Your Address Book is empty"];
    
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor = APP_THEME_LIGHT_GREEN;
    [refreshController addTarget:self action:@selector(handlePullToRefresh:) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.refreshControl = refreshController;
}
-(void)loadProfileDetails
{
    CustomerModel *customerModel = [CustomerModel getSavedCustomerObject];
    [self.lblName shoppingListProfileNameStyle:customerModel.fullName];
    [self.lblPhone shoppingListProfilePhoneNumberStyle:customerModel.phoneWithCountryCode ];
}
-(void)editAddress:(NSInteger)index
{
    AddNewAddressViewController *ctrlAddress = [self.storyboard instantiateViewControllerWithIdentifier:@"AddNewAddressViewController"];
    ctrlAddress.isEditAddress = YES;
    ctrlAddress.addressModel = addressList.data[index];
    [self.navigationController pushViewController:ctrlAddress animated:YES];
}
-(void)deleteAddress:(int)index
{
    AddressModel *addressModel = [addressList.data objectAtIndex:index];
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:[languageManager languageStringForKey:@"Are you sure, You want to delete this address ?"]
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[languageManager languageStringForKey:@"Yes"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [self handleSelectedCheckOutAddress:addressModel.address_id];
                                   [self deleteCustomerAddress:(int)addressModel.address_id];
                               }];
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:[languageManager languageStringForKey:@"No"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)handleSelectedCheckOutAddress:(NSInteger) selectedAddressId
{
    CartManager *cartManager = [CartManager sharedManager];
    if(cartManager.billingAddressModel != nil && cartManager.billingAddressModel.address_id == selectedAddressId)
    {
        [cartManager setBillingAddress:nil];
    }
    if(cartManager.deliveryAddressModel != nil && cartManager.deliveryAddressModel.address_id == selectedAddressId)
    {
        [cartManager setDeliveryAddress:nil];
    }
}
-(BOOL)isValidlandMark:(NSString *)landmark
{
    if([landmark isEqualToString:@""] || landmark == NULL)
        return NO;
    else
        return YES;
}
#pragma mark - Handle pull to refresh

-(void)handlePullToRefresh : (id)sender
{
    [self getAddressList];
}
#pragma mark - Action method

-(IBAction)buttonClick:(id)sender
{
    if(sender==self.btnAddNew)
    {
    }
}
#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return addressList.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     AddressLisrTableViewCell *cell = (AddressLisrTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AddressLisrTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    AddressModel *addressModel = [addressList.data objectAtIndex:indexPath.row];
    cell.lblName.text = [NSString stringWithFormat:@"%@ %@",addressModel.firstname,addressModel.lastname];
  
    NSMutableString *address = [[NSMutableString alloc] initWithString:addressModel.address_1];
    if(![addressModel.address_2 isEqualToString:@""])
        [address appendFormat:@", %@",addressModel.address_2];
    if(![addressModel.city isEqualToString:@""])
        [address appendFormat:@", %@",addressModel.city];
    if(![addressModel.postcode isEqualToString:@""])
        [address appendFormat:@", %@",addressModel.postcode];
    if(![addressModel.country isEqualToString:@""])
        [address appendFormat:@", %@",addressModel.country];
    
    cell.txtAddress.text = address;
    [cell.imgDefault setHidden:!addressModel.is_default];
   
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.isBillingAddressEdit){
        if([self isValidlandMark:((AddressModel *)[addressList.data objectAtIndex:indexPath.row]).landmark]){
            [[CartManager sharedManager] setBillingAddress:[addressList.data objectAtIndex:indexPath.row]];
            [self.navigationController popViewControllerAnimated:YES];
        }else
        {
             [Utility showMessageAlertView:self.navigationController message:[languageManager languageStringForKey:@"Please add the closest landmark"]];
        }
    }
    else if (self.isDeliveryAddressEdit)
    {
        if([self isValidlandMark:((AddressModel *)[addressList.data objectAtIndex:indexPath.row]).landmark]){
            [[CartManager sharedManager] setDeliveryAddress:[addressList.data objectAtIndex:indexPath.row]];
            [self.navigationController popViewControllerAnimated:YES];
        }else
        {
            [Utility showMessageAlertView:self.navigationController message:[languageManager languageStringForKey:@"Please add the closest landmark"]];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *btnDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:[NSString stringWithFormat:@"  %@  ",[languageManager languageStringForKey:@"Delete"]] handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        [self deleteAddress:(int)indexPath.row];
                                    }];
    btnDelete.backgroundColor = [UIColor redColor]; //arbitrary color
    
    UITableViewRowAction *btnEdit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:[NSString stringWithFormat:@"  %@  ",[languageManager languageStringForKey:@"Edit"]] handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                     {
                                         [self editAddress:indexPath.row];
                                     }];
    btnEdit.backgroundColor = APP_VIEW_BACKGROUND_CLOUR;//arbitrary color
    
    return @[btnDelete, btnEdit]; //array with all the buttons you want. 1,2,3, etc...
}

#pragma mark - Network calls

-(void)getAddressList
{
    [[NetworkManager sharedManager] getCustomerAddressList:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            NSError *error;
            self->addressList = [[AddressListModel alloc] initWithDictionary:responseData error:&error];
            
            if(self->addressList.data.count > 0){
                [self.tableView reloadData];
                [self.viewNoAddress setHidden:YES];
            }
            else
            {
                [self.viewNoAddress setHidden:NO];
            }
            
        }
        else
        {
            [self.viewNoAddress setHidden:NO];
            [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
        [self->refreshController endRefreshing];
        
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
         [self->refreshController endRefreshing];
        [Utility showErrorAlertView:self.navigationController];
    }];
}
-(void)deleteCustomerAddress:(int)addressId
{
    [[NetworkManager sharedManager] deleteAddress:addressId success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            dispatch_async(dispatch_get_main_queue(), ^{
              [Utility showMessageAlertView:self.navigationController message:[self->languageManager languageStringForKey:@"Address successfully deleted."]];
               [self getAddressList];
            });
        }
        else
        {
            [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
    }];
}
@end

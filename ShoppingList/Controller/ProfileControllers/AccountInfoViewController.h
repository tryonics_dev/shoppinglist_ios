//
//  AccountInfoViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/26/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NumberVerificationViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface AccountInfoViewController : UIViewController<UITextFieldDelegate>
{
    LanguageManager *languageManager;
    BOOL isFirstNameChange,isLastNameChange,isEmailChange;
    NSString *strTelephone,*strFirstName;
}
@property(nonatomic,weak)IBOutlet UILabel
*lblName,
*lblPhone,
*lblFirstName,
*lblLastName,
*lblTelephone,
*lblTitleEmail,
*lblErrorFirstName,
*lblErrorLastName,
*lblErrorPhoneNumber,
*lblErrorEmail;
@property(nonatomic,weak)IBOutlet UIButton
*btnSave,
*btnFirstName,
*btnLastName,
*btnTelephone,
*btnEmail;
@property(nonatomic,weak)IBOutlet UITextField
*txtFirstName,
*txtLastName,
*txtTelephone,
*txtEmail;
@end

NS_ASSUME_NONNULL_END

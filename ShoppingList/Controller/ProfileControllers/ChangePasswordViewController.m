//
//  ChangePasswordViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/26/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self loadProfileDetails];
    // Do any additional setup after loading the view.
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [textField showErrorTextField:NO];
    if(textField == self.txtPassword)
    {
        self.lblErrorPassword.text =@"";
    }
    else if (textField == self.txtConfirmPassword)
    {
        self.lblErrorConfirmPassword.text = @"";
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Userdefine methods

-(void)initUI
{
    [TabTitleHandler handleTabTitle:self.tabBarController selectedIndex:4];
    languageManager = [LanguageManager sharedManager];
    if(self.isFromForgotPassword)
    {
        [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@""] navigationItem:self.navigationItem];
        [self.navigationController initShoppingListStyle];
        [self.btnSave shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Change Password"]];
        self.lblName.text = [languageManager languageStringForKey:@"Reset Password"];
        [self.lblName setFont:[UIFont fontWithName:@"SFCompactText-Bold" size:30]];
    }
    else
    {
        [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"Change Password"] navigationItem:self.navigationItem];
        [self.navigationController initShoppingListStyle];
        [self.btnSave shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Update"]];
        [self.imgLogo setHidden:YES];
    }
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    
    
    [self.lblTitlePassword shoppingTextLableStyle:[languageManager languageStringForKey:@"Password"]];
    [self.txtPassword shoppingListGreenFullRoundedCorner];
    [self.lblErrorPassword shoppingTextErrorStyle:@""];
    
    [self.lblTitleConfirmPassword shoppingTextLableStyle:[languageManager languageStringForKey:@"Confirm Password"]];
    [self.txtConfirmPassword shoppingListGreenFullRoundedCorner];
    [self.lblErrorConfirmPassword shoppingTextErrorStyle:@""];
}
-(void)loadProfileDetails
{
    if(!self.isFromForgotPassword)
    {
        CustomerModel *customerModel = [CustomerModel getSavedCustomerObject];
        [self.lblName shoppingListProfileNameStyle:customerModel.fullName];
        [self.lblPhone shoppingListProfilePhoneNumberStyle:customerModel.phoneWithCountryCode ];
        if(customerModel == nil)
        {
            [self.imgTick setHidden:YES];
        }
    }
    else
    {
        [self.lblPhone setHidden:YES];
        [self.imgTick setHidden:YES];
    }
}
-(BOOL)isPageValid
{
    BOOL isValid = YES;
   
    if(![Utility validatePassword:self.txtPassword.text])
    {
        [self.txtPassword showErrorTextField:YES];
        self.lblErrorPassword.text = [languageManager languageStringForKey:@"Password must be at least 6 characters and contain at least 1 special character (#?!@$%&)"];
        isValid = NO;
    }
    if(![self.txtPassword.text isEqualToString:self.txtConfirmPassword.text])
    {
        [self.txtConfirmPassword showErrorTextField:YES];
        self.lblErrorConfirmPassword.text = [languageManager languageStringForKey:@"Password mismatch"];
        isValid = NO;
    }
    return isValid;
}
#pragma mark - Action method

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnSave)
    {
        if([self isPageValid])
        {
            if(self.isFromForgotPassword)
                [self forgotPassword];
            else
                [self updatePassword];
        }
    }
    else if(sender == self.btnViewPassword)
    {
        self.txtPassword.secureTextEntry = !self.txtPassword.secureTextEntry;
        if(self.txtPassword.secureTextEntry)
            [self.btnViewPassword setTitle:[languageManager languageStringForKey:@"View"] forState:UIControlStateNormal];
        else
            [self.btnViewPassword setTitle:[languageManager languageStringForKey:@"Hide"] forState:UIControlStateNormal];
    }
    else if ( sender == self.btnViewConfirmPassword)
    {
        self.txtConfirmPassword.secureTextEntry =  !self.txtConfirmPassword.secureTextEntry;
        if(self.txtConfirmPassword.secureTextEntry)
            [self.btnViewConfirmPassword setTitle:[languageManager languageStringForKey:@"View"] forState:UIControlStateNormal];
        else
            [self.btnViewConfirmPassword setTitle:[languageManager languageStringForKey:@"Hide"] forState:UIControlStateNormal];
    }
}
#pragma mark - Network calls

-(void)forgotPassword
{
    [[NetworkManager sharedManager] forgotPassword:self.txtPassword.text confirmPassword:self.txtConfirmPassword.text telephone:self.strTelephoneNumber success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
           
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 SignInViewController *ctrl_signIn= [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
                 ctrl_signIn.shoudlShowBack = NO;
                 [self.navigationController pushViewController:ctrl_signIn animated:YES];
                 
                  [Utility showMessageAlertView:self.navigationController message:[self->languageManager languageStringForKey:@"Password successfully changed"]];
                 
            });
        }
        else
        {
            [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
         [Utility showErrorAlertView:self.navigationController];
    }];
}

-(void)updatePassword
{
    [[NetworkManager sharedManager] updatePassword:self.txtPassword.text confirmPassword:self.txtConfirmPassword.text success:^(id  _Nonnull responseData) {
            BOOL success = [[responseData objectForKey:@"success"] boolValue];
            if(success){
                [self logOutCustomer];
            }
            else
            {
                [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
            }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
         [Utility showErrorAlertView:self.navigationController];
    }];
}
-(void)logOutCustomer
{
    [[NetworkManager sharedManager] customerLogout:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            [Utility showMessageAlertView:self.navigationController message:[self->languageManager languageStringForKey:@"Password successfully changed"]];
            [myAppDelegate customerLogout];
        }else
        {
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [Utility showErrorAlertView:self.navigationController];
    }];
}

@end

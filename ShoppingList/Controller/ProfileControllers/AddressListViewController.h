//
//  AddressListViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/28/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerModel.h"
#import "AddressLisrTableViewCell.h"
#import "AddNewAddressViewController.h"
#import "AddressListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddressListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    LanguageManager *languageManager;
    UIRefreshControl *refreshController;
    AddressListModel *addressList;
}
@property(nonatomic,weak)IBOutlet UILabel
*lblName,*lblPhone;
@property(nonatomic,weak)IBOutlet UIButton *btnAddNew;
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,weak)IBOutlet UILabel *lblNoAddress;
@property(nonatomic,weak)IBOutlet UIView *viewNoAddress;
@property(nonatomic)BOOL isBillingAddressEdit,isDeliveryAddressEdit;
@end

NS_ASSUME_NONNULL_END

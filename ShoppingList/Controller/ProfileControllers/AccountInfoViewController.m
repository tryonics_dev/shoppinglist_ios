//
//  AccountInfoViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/26/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "AccountInfoViewController.h"
#import "CustomerModel.h"
@interface AccountInfoViewController ()

@end

@implementation AccountInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self loadProfileDetails];
    // Do any additional setup after loading the view.
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.txtEmail)
        self.lblErrorEmail.text = @"";
    else if (textField == self.txtLastName)
        self.lblErrorLastName.text = @"";
    else if (textField == self.txtFirstName)
        self.lblErrorFirstName.text =@"";
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Userdefine methods

-(void)initUI
{
    [TabTitleHandler handleTabTitle:self.tabBarController selectedIndex:4];
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"Account Info"] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    [self.btnSave shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Save"]];

    self.lblFirstName.text = [languageManager languageStringForKey:@"First Name"];
    self.lblLastName.text= [languageManager languageStringForKey:@"Last Name"];
    self.lblTelephone.text = [languageManager languageStringForKey:@"Mobile"];
    self.lblTitleEmail.text = [languageManager languageStringForKey:@"Email"];
    
    self.lblErrorEmail.text = @"";
    self.lblErrorLastName.text =@"";
    self.lblErrorFirstName.text = @"";
    self.lblErrorPhoneNumber.text = @"";
}
-(void)loadProfileDetails
{
    CustomerModel *customerModel = [CustomerModel getSavedCustomerObject];
    [self.lblName shoppingListProfileNameStyle:customerModel.fullName];
    [self.lblPhone shoppingListProfilePhoneNumberStyle:customerModel.phoneWithCountryCode ];

    self.txtFirstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:customerModel.firstname attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
    self.txtLastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:customerModel.lastname attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
    self.txtTelephone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:customerModel.phoneWithCountryCode attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
     self.txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:customerModel.email attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
    
    strTelephone = customerModel.telephone;
    strFirstName = customerModel.firstname;
}
-(BOOL)isPageValid
{
    BOOL isValid = YES;
    if((isFirstNameChange && self.txtFirstName.text.length < 2))
    {
        [self.txtFirstName showErrorTextField:YES];
        self.lblErrorFirstName.text = [languageManager languageStringForKey:@"First name must be between 1 and 32 characters"];
        isValid =NO;
    }
    else if  (isFirstNameChange && ![Utility validateName:self.txtFirstName.text])
    {
        [self.txtFirstName showErrorTextField:YES];
        self.lblErrorFirstName.text = [languageManager languageStringForKey:@"Invalid name format"];
        isValid =NO;
    }
    if(isLastNameChange && self.txtLastName.text.length < 2)
    {
        [self.txtLastName showErrorTextField:YES];
        self.lblErrorLastName.text = [languageManager languageStringForKey:@"Last name must be between 1 and 32 characters"];
        isValid =NO;
    }
    else if  (isLastNameChange && ![Utility validateName:self.txtLastName.text])
    {
        [self.txtLastName showErrorTextField:YES];
        self.lblErrorLastName.text = [languageManager languageStringForKey:@"Invalid name format"];
        isValid =NO;
    }
    if((isEmailChange && self.txtEmail.text.length>0) && ![Utility validateEmail:self.txtEmail.text])
    {
        [self.txtEmail showErrorTextField:YES];
        self.lblErrorEmail.text =  [languageManager languageStringForKey:@"Please enter a valid Email"];
        isValid = NO;
    }
    return isValid;
}
-(void)updateCustomerModel
{
    CustomerModel *customerModel = [CustomerModel getSavedCustomerObject];
    if(isFirstNameChange)
        customerModel.firstname = self.txtFirstName.text;
    if(isLastNameChange)
        customerModel.lastname = self.txtLastName.text;
    if(isEmailChange)
        customerModel.email = self.txtEmail.text;
    
    [CustomerModel updateLocalCustomerData:customerModel];
    [self.navigationController popViewControllerAnimated:YES];
   
    self.txtLastName.text = @"";
    self.txtFirstName.text =@"";
    self.txtEmail.text = @"";
    self.txtLastName.userInteractionEnabled = NO;
    self.txtFirstName.userInteractionEnabled = NO;
    self.txtEmail.userInteractionEnabled = NO;

    self.txtFirstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:customerModel.firstname attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
    self.txtLastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:customerModel.lastname attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
    self.txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:customerModel.email attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];

    isFirstNameChange = NO;
    isLastNameChange = NO;
    isEmailChange = NO;
    
    [self.lblName shoppingListProfileNameStyle:[NSString stringWithFormat:@"%@ %@ ",customerModel.firstname,customerModel.lastname]];
    [self.lblPhone shoppingListProfilePhoneNumberStyle:customerModel.phoneWithCountryCode ];
}
#pragma mark - Action method

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnSave)
    {
        [self.view endEditing:YES];
        if([self isPageValid]){
            [self updateCustomer];
        }
        
    }
    else if (sender == self.btnFirstName)
    {
        self.lblErrorFirstName.text =@"";
        if( self.txtFirstName.userInteractionEnabled)
        {
            self.txtFirstName.userInteractionEnabled =NO;
            [self.txtFirstName resignFirstResponder];
            self.txtFirstName.text =@"";
            isFirstNameChange = NO;
        }else{
            self.txtFirstName.userInteractionEnabled =YES;
            self.txtFirstName.text = self.txtFirstName.placeholder;
            [self.txtFirstName becomeFirstResponder];
            isFirstNameChange = YES;
        }
    }
    else if (sender == self.btnLastName)
    {
        self.lblErrorLastName.text =@"";
        if( self.txtLastName.userInteractionEnabled)
        {
            self.txtLastName.userInteractionEnabled =NO;
            [self.txtLastName resignFirstResponder];
            self.txtLastName.text =@"";
            isLastNameChange = NO;
        }else{
            self.txtLastName.userInteractionEnabled =YES;
            self.txtLastName.text =  self.txtLastName.placeholder;
            [self.txtLastName becomeFirstResponder];
            isLastNameChange = YES;
        }
    }
    else if (sender == self.btnEmail)
    {
        self.lblErrorEmail.text = @"";
        if( self.txtEmail.userInteractionEnabled)
        {
            self.txtEmail.userInteractionEnabled =NO;
            [self.txtEmail resignFirstResponder];
            self.txtEmail.text =@"";
            isEmailChange = NO;
        }else{
            self.txtEmail.userInteractionEnabled =YES;
            self.txtEmail.text = self.txtEmail.placeholder;
            [self.txtEmail becomeFirstResponder];
            isEmailChange = YES;
        }
    }
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if([segue.identifier isEqualToString:@"infoMobileNumberUpdate"])
     {
         NumberVerificationViewController *ctrVerification =segue.destinationViewController;
         ctrVerification.isFromProfileUpdate = YES;
         ctrVerification.isFromForgotPassword =NO;
     }
 }


#pragma mark - Network calls

-(void)updateCustomer
{
    BOOL isUpdateRequired = NO;
    CustomerModel *customerModel = [CustomerModel getSavedCustomerObject];
   
    if(isFirstNameChange){
        customerModel.firstname = self.txtFirstName.text;
        isUpdateRequired = YES;
    }
    if(isLastNameChange){
        customerModel.lastname = self.txtLastName.text;
        isUpdateRequired = YES;
    }
    if(isEmailChange){
        customerModel.email = self.txtEmail.text;
        isUpdateRequired = YES;
    }
    if(isUpdateRequired){
        [[NetworkManager sharedManager] updateCustomer:[customerModel toDictionary] success:^(id  _Nonnull responseData) {
            BOOL success = [[responseData objectForKey:@"success"] boolValue];
            if(success){
                 [Utility showMessageAlertView:self.navigationController message:[self->languageManager languageStringForKey:@"Account details successfully updated."]];
                [self updateCustomerModel];
            }
            else
            {
                 [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
            }
        } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
             [Utility showErrorAlertView:self.navigationController];
        }];
    }
}

@end

//
//  AddNewAddressViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/28/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "AddNewAddressViewController.h"


@interface AddNewAddressViewController ()

@end

@implementation AddNewAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    // Do any additional setup after loading the view.
}

#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:(self.isEditAddress ? @"Update" : @"Add New Address")] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    self.tapRec = [[UITapGestureRecognizer alloc]
                   initWithTarget:self action:@selector(viewTap:)];
    [self.tapRec setCancelsTouchesInView:NO];
    self.tapRec.numberOfTapsRequired =1;
    
}
-(void)viewTap:(UIGestureRecognizer *)gestureRecognizer
{
    [self.view endEditing:YES];
}
-(NSDictionary *)getPostObject
{
    NSDictionary *dicPostObject  = [NSDictionary dictionaryWithObjectsAndKeys:
                                    self.txtFirstName.text,@"firstname",
                                    self.txtLastName.text,@"lastname",
                                    self.txtCity.text,@"city",
                                    self.txtAddress1.text,@"address_1",
                                    self.txtAddress2.text,@"address_2",
                                    @"196",@"country_id",
                                    self.txtPostCode.text,@"postcode",
                                    @"3030",@"zone_id",
                                    self.txtCompany.text,@"company",
                                    [NSNumber numberWithBool:self.isDefaultAddress],@"default",
                                    [NSNumber numberWithDouble:closestPlaceLon],@"landmark_lon",
                                    [NSNumber numberWithDouble:closestPlaceLat],@"landmark_lat",
                                    self.txtClosestLocation.text,@"landmark",nil];
    
//    AddressModel *address = [[AddressModel alloc] init];
//    address.firstname = self.txtFirstName.text;
//    address.lastname = self.txtLastName.text;
//    address.city = self.txtCity.text;
//    address.address_1 = self.txtAddress1.text;
//    address.address_2 = self.txtAddress2.text;
//    address.country_id = 196;
//    address.zone_id = 3030;
//    address.company = self.txtCompany.text;
//    address.is_default = self.isDefaultAddress;
//    address.landmark_lon = [[NSString stringWithFormat:@"%.04f", closestPlaceLon] doubleValue]; ;
//    address.landmark_lat = [[NSString stringWithFormat:@"%.04f", closestPlaceLat] doubleValue];
//    address.landmark =  self.txtClosestLocation.text;
    
    return dicPostObject;
}
#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [textField showErrorTextField:NO];
    if(textField == self.txtFirstName)
        self.lblErrorFirstName.text = @"";
    else if(textField == self.txtLastName)
        self.lblErrorLastName.text = @"";
    else if (textField == self.txtAddress1)
        self.lblErrorAddress1.text = @"";
    else if (textField == self.txtAddress2)
        self.lblErrorAddress2.text = @"";
    else if (textField == self.txtCity)
        self.lblErrorCity.text = @"";
    else if (textField == self.txtClosestLocation)
        self.lblErrorClosestLocation.text =@"";
    else if(textField == self.txtPostCode)
    {
        if(![string isEqualToString:@""] && self.txtPostCode.text.length>4)
        {
            return NO;
        }
        return YES;
    }
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)isPageValid
{
    BOOL isValid = YES;
    if(self.txtFirstName.text.length < 2)
    {
        [self.txtFirstName showErrorTextField:YES];
        self.lblErrorFirstName.text = [languageManager languageStringForKey:@"First name must be between 1 and 32 characters"];
        isValid =NO;
    }
    else if (![Utility validateName:self.txtFirstName.text])
    {
        
        [self.txtFirstName showErrorTextField:YES];
        self.lblErrorFirstName.text = [languageManager languageStringForKey:@"Invalid name format"];
        isValid =NO;
    }
    if(self.txtLastName.text.length < 2)
    {
        [self.txtLastName showErrorTextField:YES];
        self.lblErrorLastName.text = [languageManager languageStringForKey:@"Last name must be between 1 and 32 characters"];
        isValid =NO;
    }
    else if (![Utility validateName:self.txtLastName.text])
    {
        [self.txtLastName showErrorTextField:YES];
        self.lblErrorLastName.text = [languageManager languageStringForKey:@"Invalid name format"];
        isValid =NO;
    }
    if([self.txtAddress1.text isEqualToString:@""])
    {
        [self.txtAddress1 showErrorTextField:YES];
        self.lblErrorAddress1.text = [languageManager languageStringForKey:@"This field is required"];
        isValid =NO;
    }
    else if (![Utility validateAddress:self.txtAddress1.text])
    {
        [self.txtAddress1 showErrorTextField:YES];
        self.lblErrorAddress1.text = [languageManager languageStringForKey:@"Invalid address format"];
        isValid =NO;
    }
    if (![self.txtAddress2.text isEqualToString:@""] && ![Utility validateAddress:self.txtAddress2.text])
    {
        [self.txtAddress2 showErrorTextField:YES];
        self.lblErrorAddress2.text = [languageManager languageStringForKey:@"Invalid address format"];
        isValid =NO;
    }
    if([self.txtCity.text isEqualToString:@""])
    {
        [self.txtCity showErrorTextField:YES];
        self.lblErrorCity.text = [languageManager languageStringForKey:@"This field is required"];
        isValid =NO;
    }
    else if(self.txtCity.text.length < 2 || self.txtCity.text.length > 38)
    {
        [self.txtCity showErrorTextField:YES];
        self.lblErrorCity.text = [languageManager languageStringForKey:@"City must be between 2 and 38 characters"];
        isValid =NO;
    }
    if([self.txtClosestLocation.text isEqualToString:@""])
    {
        [self.txtClosestLocation showErrorTextField:YES];
        self.lblErrorClosestLocation.text = [languageManager languageStringForKey:@"This field is required"];
        isValid =NO;
    }
    return isValid;
}
#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddNewAddressTableViewCell *cell = (AddNewAddressTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AddNewAddressTableViewCell" forIndexPath:indexPath];
    [cell.viewBackground addGestureRecognizer:self.tapRec];
   
    self.btnDefault = cell.btnDefault;
    
    self.txtLastName = cell.txtLastName;
    self.txtFirstName = cell.txtFirstName;
    self.txtCompany = cell.txtCompany;
    self.txtAddress1 = cell.txtAddress1;
    self.txtAddress2 = cell.txtAddress2;
    self.txtCity = cell.txtCity;
    self.txtPostCode = cell.txtPostCode;
    self.txtClosestLocation  = cell.txtClosestLocation;
    
    self.txtLastName.delegate = self;
    self.txtFirstName.delegate = self;
    self.txtCompany.delegate = self;
    self.txtAddress1.delegate = self;
    self.txtAddress2.delegate = self;
    self.txtCity.delegate = self;
    self.txtPostCode.delegate = self;
    self.txtClosestLocation.delegate  = self;
    
    self.lblErrorFirstName = cell.lblErrorFirstName;
    self.lblErrorLastName = cell.lblErrorLastName;
    self.lblErrorCompany = cell.lblErrorCompany;
    self.lblErrorAddress1 = cell.lblErrorAddress1;
    self.lblErrorAddress2 = cell.lblErrorAddress2;
    self.lblErrorPostCode = cell.lblErrorPostCode;
    self.lblErrorCity = cell.lblErrorCity;
    self.lblErrorClosestLocation = cell.lblErrorClosestLocation;
    
    CustomerModel *customerModel = [CustomerModel getSavedCustomerObject];
    self.txtFirstName.text = customerModel.firstname;
    self.txtLastName.text = customerModel.lastname;
    
    if(self.isEditAddress)
    {
        self.txtFirstName.text =self.addressModel.firstname;
        self.txtLastName.text =  self.addressModel.lastname;
        self.txtCompany.text = self.addressModel.company;
        self.txtAddress1.text=self.addressModel.address_1;
        self.txtAddress2.text = self.addressModel.address_2;
        self.txtCity.text= self.addressModel.city;
        self.txtPostCode.text = self.addressModel.postcode;
        self.txtClosestLocation.text =self.addressModel.landmark;
        closestPlaceLat = self.addressModel.landmark_lat;
        closestPlaceLon = self.addressModel.landmark_lon;
        self.isDefaultAddress = self.addressModel.is_default;

        [self.btnDefault setSelected:self.isDefaultAddress];
        
         [cell.btnSave shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Update"]];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 1000;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Action method

-(IBAction)defaultAddressToggle:(id)sender
{
    if(self.isDefaultAddress)
    {
        [self.btnDefault setSelected:NO];
        self.isDefaultAddress = NO;
    }
    else
    {
        [self.btnDefault setSelected:YES];
        self.isDefaultAddress = YES;
    }
}
-(IBAction)showGooglePlacePicker:(id)sender
{
    
    self.lblErrorClosestLocation.text = @"";
    [self.txtClosestLocation showErrorTextField:NO];
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    
    // Specify the place data types to return.
    GMSPlaceField fields = (GMSPlaceFieldName | GMSPlaceFieldPlaceID);
    acController.placeFields = fields;
    
    // Specify a filter.
    _filter = [[GMSAutocompleteFilter alloc] init];
    _filter.type = kGMSPlacesAutocompleteTypeFilterEstablishment;
    _filter.country = @"LK";
    acController.autocompleteFilter = _filter;
    
    // Display the autocomplete view controller.
    [self presentViewController:acController animated:YES completion:nil];
}
 -(IBAction)addAddressClick:(id)sender
{
    [self.view endEditing:YES];
    if([self isPageValid])
    {
        if(self.isEditAddress)
            [self updateAddress];
        else
            [self addAddress];
    }
}

#pragma mark - google place picker delegates

- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];

    GMSPlacesClient *_placesClient =[GMSPlacesClient sharedClient];
    [_placesClient lookUpPlaceID:place.placeID callback:^(GMSPlace *loc, NSError *error) {
       if (error == nil && place != nil) {
          self->closestPlaceLat = loc.coordinate.latitude;
          self-> closestPlaceLon = loc.coordinate.longitude;
          self.txtClosestLocation.text = loc.name;
        }
        
    }];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - Network calls

-(void)addAddress
{
    [[NetworkManager sharedManager] addAddress:[self getPostObject] success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            [self.navigationController popViewControllerAnimated:YES];
            [Utility showMessageAlertView:self.navigationController message:[self->languageManager languageStringForKey:@"Address successfully saved."]];
        }
        else
        {
            [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
        
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
    }];
}
-(void)updateAddress
{
    [[NetworkManager sharedManager] updateAddress:[self getPostObject] id:(int)self.addressModel.address_id success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            
             [self.navigationController popViewControllerAnimated:YES];
             [Utility showMessageAlertView:self.navigationController message:[self->languageManager languageStringForKey:@"Address successfully updated."]];
        }
        else
        {
           [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

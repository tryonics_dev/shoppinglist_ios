//
//  NumberVerificationViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/22/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "NumberVerificationViewController.h"

@interface NumberVerificationViewController ()

@end

@implementation NumberVerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    
    #ifdef DEBUG
    self.txtMobileNumber.text = @"717329507";
    #endif
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{
    [self.txtMobileNumber becomeFirstResponder];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
     [self.view endEditing:YES];
}
#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@""] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [self.navigationController.navigationBar setHidden:NO];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    [self.lblTitle shoppingTitleStyle: [languageManager languageStringForKey:@"Mobile Number Verification"]];
    [self.lblSubTitle shoppingSubTitleStyle:[languageManager languageStringForKey:@"We need to text you the OTP to authenticate your account"]];
    [self.lblMobileNumber shoppingTextLableStyle:[languageManager languageStringForKey:@"Mobile number"]];
    [self.lblMobileNumberRequired shoppingTextItalicGerayStyle:[languageManager languageStringForKey:@"Mobile number verification will be required"]];
    
    self.viewTextField.layer.borderColor = APP_THEME_LIGHT_GREEN.CGColor;
    self.viewTextField.layer.borderWidth = 1;
    self.viewTextField.layer.cornerRadius = BUTTON_CORNER_RADIUS;
    self.viewTextField.layer.masksToBounds = YES;
    self.viewTextField.backgroundColor = [UIColor clearColor];
    
    [self.btnSendOTP shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Send OTP"]];
    
    if(IS_IPHONE_5)
    {
        self.constrainTitleTop.constant = 0;
        self.constrainSubTitleTop.constant =15;
        self.constrainButtonTop.constant =15;
        self.constrainSubSubTitleTop.constant = 0;
    }
}
-(void)viewIsErrorStyle:(BOOL)isError
{
    if(isError)
    {
         self.viewTextField.layer.borderColor = APP_DARK_RED_CLOUR.CGColor;
    }
    else
    {
        self.viewTextField.layer.borderColor = APP_THEME_LIGHT_GREEN.CGColor;
    }
}
#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==self.txtMobileNumber ){
        [self viewIsErrorStyle:NO];
        if([string isEqualToString:@""])
            return YES;
        if(textField.text.length<9)
        {
            return YES;
        }
        else
            return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender== self.btnSendOTP)
    {
        if([Utility validateSLMobile:self.txtMobileNumber.text])
        {
            [self viewIsErrorStyle:NO];
            [self requestOTP];
//            ChangePasswordViewController *ctrlChangePassword = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
//            ctrlChangePassword.isFromForgotPassword = self.isFromForgotPassword;
//            ctrlChangePassword.strTelephoneNumber = @"0717329507";///self.strPhoneNumber;
//            [self.navigationController pushViewController:ctrlChangePassword animated:YES];
        }
        else
        {
             [self viewIsErrorStyle:YES];
            [Utility showMessageAlertView:self.navigationController message:[languageManager languageStringForKey:@"Please enter valid mobile number"]];
        }
    }
}
#pragma mark - Network calls

-(void)requestOTP
{
    int otpType = 1;
    if(self.isFromProfileUpdate)
        otpType = 6;
    else if (self.isFromForgotPassword)
        otpType = 4;
    
    [[NetworkManager sharedManager] getOTP:self.txtMobileNumber.text type:otpType success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            
            OTPViewController *ctrlOTP = [self.storyboard instantiateViewControllerWithIdentifier:@"OTPViewController"];
            ctrlOTP.strPhoneNumber = self.txtMobileNumber.text;
            ctrlOTP.isFromProfileUpdate = self.isFromProfileUpdate;
            ctrlOTP.isFromForgotPassword = self.isFromForgotPassword;
            [self.navigationController pushViewController:ctrlOTP animated:YES];
        }
        else
        {
           [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [Utility showErrorAlertView:self.navigationController];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

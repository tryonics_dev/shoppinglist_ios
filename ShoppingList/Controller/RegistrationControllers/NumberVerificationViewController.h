//
//  NumberVerificationViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/22/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OTPViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface NumberVerificationViewController : UIViewController<UITextFieldDelegate>
{
     LanguageManager *languageManager;
}
@property (weak , nonatomic)IBOutlet UILabel *lblTitle,*lblSubTitle,*lblMobileNumber,*lblMobileNumberRequired;
@property (weak, nonatomic)IBOutlet UIView *viewTextField;
@property (weak,nonatomic)IBOutlet UITextField *txtMobileNumber;
@property (weak,nonatomic)IBOutlet UIButton *btnSendOTP;
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *constrainTitleTop,*constrainSubTitleTop,*constrainButtonTop,*constrainSubSubTitleTop;
@property (nonatomic)BOOL isFromProfileUpdate,isFromForgotPassword;
@end

NS_ASSUME_NONNULL_END

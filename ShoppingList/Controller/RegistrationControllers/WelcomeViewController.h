//
//  WelcomeViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/21/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignInViewController.h"
#import "NumberVerificationViewController.h"
#import "FTPopOverMenu.h"
#import "LanguageSelectionViewController.h"
#import "TTTAttributedLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WelcomeViewController : UIViewController<TTTAttributedLabelDelegate>
{
    LanguageManager *languageManager;
    
}
@property (weak,nonatomic)IBOutlet UIView *viewTop;
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *topViewHeightConstrain,*lblNewUserTopConstrain;
@property (weak,nonatomic)IBOutlet UILabel *lblNewUser,*lblAccount;
@property (weak,nonatomic)IBOutlet TTTAttributedLabel *lblTerms;
@property (weak,nonatomic)IBOutlet UIButton *btnCreateAccount,*btnSignIn,*btnForgotPassword,*btnSettings;
@property (weak,nonatomic)IBOutlet UIImageView *imglog;
@end

NS_ASSUME_NONNULL_END

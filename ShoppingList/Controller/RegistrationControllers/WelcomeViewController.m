//
//  WelcomeViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/21/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self initUI];
}
#pragma mark - Userdefine methods

-(void)initUI
{
    [TabTitleHandler handleTabTitle:self.tabBarController selectedIndex:4];
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@""] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [self.navigationController.navigationBar setHidden:YES];
    [myAppDelegate setStatusBarBackgroundColor:[myAppDelegate colourBackground]];
    
    if(IS_IPHONE_5)
    {
        self.topViewHeightConstrain.constant = 160;
        self.lblNewUserTopConstrain.constant = 5;
        [self.imglog setHidden:YES];
    }
    
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, SCREEN_WIDTH, self.topViewHeightConstrain.constant) byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){SCREEN_WIDTH/2, SCREEN_WIDTH/2}].CGPath;
    self.viewTop.layer.mask = maskLayer;
    
    self.viewTop.backgroundColor = [myAppDelegate colourBackground];

    [self.lblNewUser shoppingTextLableStyle:[languageManager languageStringForKey:@"I am a new user"]];
    [self.lblAccount shoppingTextLableStyle:[languageManager languageStringForKey:@"Already have an account"]];
    
    [self.btnCreateAccount shoppingClearRoundedCorner:[languageManager languageStringForKey:@"Register now"]];
    [self.btnSignIn shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Log In now"]];
    [self.btnForgotPassword setTitle:[languageManager languageStringForKey:@"forgot my password"] forState:UIControlStateNormal];
    
    //self.view.backgroundColor = APP_VIEW_BACKGROUND_OFF_WHITE_CLOUR;
    
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.menuWidth=220;
    
    [self.btnSettings addTarget:self action:@selector(onNavMenuButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *termsOfService = [NSString stringWithFormat:@"%@",@"By continue with Shopping List for create an account, you agree to Tryonics(Pvt) Ltd’s Terms of service and Privacy Policy"];
   
    self.lblTerms.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    self.lblTerms.delegate = self;
    self.lblTerms.text = termsOfService;
    NSRange range = [self.lblTerms.text rangeOfString:@"Terms of service"];
    [self.lblTerms addLinkToURL:[NSURL URLWithString:@"https://shoppinglist.lk/index.php?route=information/information&information_id=5"] withRange:range];
     NSRange rangePrivacy = [self.lblTerms.text rangeOfString:@"Privacy Policy"];
    [self.lblTerms addLinkToURL:[NSURL URLWithString:@"https://shoppinglist.lk/index.php?route=information/information&information_id=3"] withRange:rangePrivacy];
    
    
    //https://shoppinglist.lk/index.php?route=information/information&information_id=5
    //https://shoppinglist.lk/index.php?route=information/information&information_id=3
    
}
#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    [[[UIActionSheet alloc] initWithTitle:[url absoluteString] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open Link in Safari", nil), nil] showInView:self.view];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:actionSheet.title]];
}
#pragma mark - Action methods

-(void)onNavMenuButtonTapped:(id *)sender event:(UIEvent *)event
{
    [FTPopOverMenu showFromEvent:event withMenuArray:@[[languageManager languageStringForKey:@"Language"]]  doneBlock:^(NSInteger selectedIndex) {
        if(selectedIndex==0)
        {
            LanguageSelectionViewController *ctrlLanguage = [self.storyboard instantiateViewControllerWithIdentifier:@"LanguageSelectionViewController"];
            ctrlLanguage.IsFromProfile = YES;
            [self.navigationController pushViewController:ctrlLanguage animated:YES];
        }
        
        
    } dismissBlock:^{
        NSLog(@"cancel");
    }];

}

 #pragma mark - Navigation

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if([segue.identifier isEqualToString:@"directLogin"])
     {
         SignInViewController *ctrlSignIn = [segue destinationViewController];
         ctrlSignIn.shoudlShowBack = YES;
     }
     else if ([segue.identifier isEqualToString:@"isDirectCall"])
     {
         NumberVerificationViewController *ctrlNumberVerification =[segue destinationViewController];
         ctrlNumberVerification.isFromProfileUpdate = NO;
         ctrlNumberVerification.isFromForgotPassword = NO;
     }
     else if ([segue.identifier isEqualToString:@"forgotPassword"])
     {
         NumberVerificationViewController *ctrlNumberVerification =[segue destinationViewController];
         ctrlNumberVerification.isFromProfileUpdate = NO;
         ctrlNumberVerification.isFromForgotPassword = YES;
         
     }
 }


@end

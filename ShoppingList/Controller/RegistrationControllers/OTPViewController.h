//
//  OTPViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/22/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUpViewController.h"
#import "SignInViewController.h"
#import "ChangePasswordViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OTPViewController : UIViewController<UITextFieldDelegate>
{
    LanguageManager *languageManager;
   
}
@property (nonatomic)BOOL isFromProfileUpdate,isFromForgotPassword;

@property (weak , nonatomic)IBOutlet UILabel *lblTitle,*lblSubTitle,*lblVerificationCode,*lblExpiryTime;
@property (weak,nonatomic)IBOutlet UIButton *btnVerify,*btnResendOtp;
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *constrainTitleTop,*constrainSubTitleTop,*constrainButtonTop,*constrainSubSubTitleTop;
@property(weak,nonatomic)IBOutlet UITextField *txtOne,*txtTwo,*txtThree,*txtFour;

@property (nonatomic ,strong)NSString *strPhoneNumber;
@property (nonatomic)int expiryTime;

@end

NS_ASSUME_NONNULL_END

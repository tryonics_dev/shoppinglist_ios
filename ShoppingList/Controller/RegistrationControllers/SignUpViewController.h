//
//  SignUpViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/23/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUpTableViewCell.h"
#import "SignInViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SignUpViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
     LanguageManager *languageManager;
}
@property (weak, nonatomic)IBOutlet UITableView *tableView;
@property (nonatomic ,strong)UITapGestureRecognizer *tapRec;
@property (weak,nonatomic)IBOutlet UITextField *txtFirstName,
*txtLastName,
*txtEmail,
*txtTelephone,
*txtPassword,
*txtConfirmPassword;
@property (weak , nonatomic)IBOutlet UILabel
*lblErrorFirstName,
*lblErrorLastName,
*lblErrorEmail,
*lblErrorPassword,
*lblErrorConfirmPassword;

@property (nonatomic ,strong)NSString *strPhoneNumber;
@end

NS_ASSUME_NONNULL_END

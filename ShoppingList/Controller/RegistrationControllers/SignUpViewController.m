//
//  SignUpViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/23/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController
@synthesize strPhoneNumber;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    
#ifdef DEBUG
   
    
#endif
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton=YES;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [self.txtFirstName becomeFirstResponder];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@""] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [self.navigationController.navigationBar setHidden:NO];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    self.tapRec = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(viewTap:)];
    [self.tapRec setCancelsTouchesInView:NO];
    self.tapRec.numberOfTapsRequired =1;
  
   
}
-(void)viewTap:(UIGestureRecognizer *)gestureRecognizer
{
    [self.view endEditing:YES];
}
-(BOOL)isPageValid
{
    BOOL isValid = YES;
    if(self.txtFirstName.text.length < 2)
    {
        [self.txtFirstName showErrorTextField:YES];
        self.lblErrorFirstName.text = [languageManager languageStringForKey:@"First name must be between 1 and 32 characters"];
        isValid =NO;
    }
    else if (![Utility validateName:self.txtFirstName.text])
    {
       
        [self.txtFirstName showErrorTextField:YES];
        self.lblErrorFirstName.text = [languageManager languageStringForKey:@"Invalid name format"];
        isValid =NO;
    }
    if(self.txtLastName.text.length < 2)
    {
        [self.txtLastName showErrorTextField:YES];
        self.lblErrorLastName.text = [languageManager languageStringForKey:@"Last name must be between 1 and 32 characters"];
        isValid =NO;
    }
    else if (![Utility validateName:self.txtLastName.text])
    {
        [self.txtLastName showErrorTextField:YES];
        self.lblErrorLastName.text = [languageManager languageStringForKey:@"Invalid name format"];
        isValid =NO;
    }
    if(![self.txtEmail.text isEqualToString:@""] && ![Utility validateEmail:self.txtEmail.text])
    {
        [self.txtEmail showErrorTextField:YES];
        self.lblErrorEmail.text = [languageManager languageStringForKey:@"Please enter a valid Email"];
        isValid = NO;
    }
    if(![Utility validatePassword:self.txtPassword.text])
    {
        [self.txtPassword showErrorTextField:YES];
        self.lblErrorPassword.text = [languageManager languageStringForKey:@"Password must be at least 6 characters and contain at least 1 special character (#?!@$%&)"];
        isValid = NO;
    }
    if(![self.txtPassword.text isEqualToString:self.txtConfirmPassword.text])
    {
        [self.txtConfirmPassword showErrorTextField:YES];
        self.lblErrorConfirmPassword.text = [languageManager languageStringForKey:@"Password mismatch"];
        isValid = NO;
    }
    return isValid;
}
#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [textField showErrorTextField:NO];
    if(textField == self.txtFirstName)
        self.lblErrorFirstName.text = @"";
    else if(textField == self.txtLastName)
        self.lblErrorLastName.text = @"";
    else if (textField == self.txtEmail)
        self.lblErrorEmail.text = @"";
    else if (textField == self.txtPassword || textField == self.txtConfirmPassword)
    {
        self.lblErrorPassword.text = @"";
        self.lblErrorConfirmPassword.text =@"";
        [self.txtPassword showErrorTextField:NO];
         [self.txtConfirmPassword showErrorTextField:NO];
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SignUpTableViewCell *cell = (SignUpTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SignUpCell" forIndexPath:indexPath];
    [cell.viewBackground addGestureRecognizer:self.tapRec];
    self.txtFirstName = cell.txtFirstName;
    self.txtLastName = cell.txtLastName;
    self.txtEmail = cell.txtEmail;
    self.txtTelephone = cell.txtTelephone;
    self.txtPassword = cell.txtPassword;
    self.txtConfirmPassword = cell.txtConfirmPassword;
    
    self.txtFirstName.delegate = self;
    self.txtLastName.delegate = self;
    self.txtEmail.delegate = self;
    self.txtTelephone.delegate = self;
    self.txtPassword.delegate = self;
    self.txtConfirmPassword.delegate = self;
    
    self.lblErrorFirstName = cell.lblErrorFirstName;
    self.lblErrorLastName = cell.lblErrorLastName;
    self.lblErrorEmail = cell.lblErrorEmail;
    self.lblErrorPassword = cell.lblErrorPassword;
    self.lblErrorConfirmPassword = cell.lblErrorConfirmPassword;
    
    self.txtTelephone.text =[NSString stringWithFormat:@"+94 %@",self.strPhoneNumber] ;
    
#ifdef DEBUG
    self.txtFirstName.text = @"FirstName";
    self.txtLastName.text = @"LastName";
    self.txtEmail.text = @"test@yopmail.com";
    self.txtPassword.text = @"qwerty123#";
    self.txtConfirmPassword.text = @"qwerty123#";
#endif
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 800;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - Action methods

-(IBAction)signUpClick:(id)sender
{
    [self.view endEditing:YES];
   if([self isPageValid])
   {
       [self registerrCustomer];
   }
}

#pragma mark - Network calls

-(void)registerrCustomer
{
    NSDictionary *postObject = [NSDictionary dictionaryWithObjectsAndKeys:self.txtFirstName.text,@"firstname",
                                self.txtLastName.text,@"lastname",
                                self.txtEmail.text,@"email",
                                self.txtPassword.text ,@"password",
                                self.txtConfirmPassword.text,@"confirm",
                                [NSString stringWithFormat:@"0%@",self.strPhoneNumber],@"telephone",
                                @"1",@"agree",
                                nil];
    [[NetworkManager sharedManager] registerCustomer:postObject success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            
            SignInViewController *ctrlSignIn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [self.navigationController pushViewController:ctrlSignIn animated:YES];
            
            [Utility showMessageAlertView:self.navigationController message:[self->languageManager languageStringForKey:@"Congratulations! Your Account has been successfully created"]];
            
        }else
        {
           [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
          [Utility showErrorAlertView:self.navigationController];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

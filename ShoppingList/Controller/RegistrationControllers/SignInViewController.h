//
//  SignInViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/23/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerModel.h"
#import "NumberVerificationViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface SignInViewController : UIViewController
{
    LanguageManager *languageManager;
}
@property (weak , nonatomic)IBOutlet UILabel *lblTitle,*lblSubTitle,*lblMobileNumber,*lblPassword;
@property (weak, nonatomic)IBOutlet UIView *viewTextField;
@property (weak,nonatomic)IBOutlet UITextField *txtMobileNumber,*txtPassword;
@property (weak,nonatomic)IBOutlet UIButton *btnSendOTP,*btnViewPassword,*btnForgotPassword;
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *constrainTitleTop,*constrainSubTitleTop,*constrainButtonTop,*constrainSubSubTitleTop;
@property (nonatomic)BOOL shoudlShowBack;
@end

NS_ASSUME_NONNULL_END

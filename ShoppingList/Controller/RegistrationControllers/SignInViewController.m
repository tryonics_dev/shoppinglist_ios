//
//  SignInViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/23/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "SignInViewController.h"

@interface SignInViewController ()

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    
#ifdef DEBUG
    self.txtMobileNumber.text = @"717329507";
#endif
}
-(void)viewWillAppear:(BOOL)animated
{
    if(!self.shoudlShowBack){
        self.navigationItem.hidesBackButton=YES;
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }
    }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@""] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [self.navigationController.navigationBar setHidden:NO];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    [self.lblTitle shoppingTitleStyle: [languageManager languageStringForKey:@"Log In"]];
    [self.lblSubTitle shoppingSubTitleStyle:[languageManager languageStringForKey:@""]];
    [self.lblMobileNumber shoppingTextLableStyle:[languageManager languageStringForKey:@"Mobile number"]];
    [self.lblPassword shoppingTextLableStyle:[languageManager languageStringForKey:@"Password"]];
    
    self.viewTextField.layer.borderColor = APP_THEME_LIGHT_GREEN.CGColor;
    self.viewTextField.layer.borderWidth = 1;
    self.viewTextField.layer.cornerRadius = BUTTON_CORNER_RADIUS;
    self.viewTextField.layer.masksToBounds = YES;
    self.viewTextField.backgroundColor = [UIColor clearColor];
    
    [self.btnSendOTP shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Log In"]];
    
    if(IS_IPHONE_5)
    {
        self.constrainTitleTop.constant = 0;
        self.constrainSubTitleTop.constant =0;
        self.constrainButtonTop.constant = 15;
        self.constrainSubSubTitleTop.constant = 0;
    }
    else if (IS_IPHONE_6)
    {
        self.constrainTitleTop.constant = 20;
         self.constrainSubTitleTop.constant =10;
    }
    [self.txtPassword shoppingListGreenFullRoundedCorner];
}
-(void)viewIsErrorStyle:(BOOL)isError
{
    if(isError)
    {
        self.viewTextField.layer.borderColor = APP_DARK_RED_CLOUR.CGColor;
    }
    else
    {
        self.viewTextField.layer.borderColor = APP_THEME_LIGHT_GREEN.CGColor;
    }
}
-(BOOL)isPageValid
{
    [self clearPageError];
    BOOL isValid = YES;
    
    if(![Utility validateSLMobile:self.txtMobileNumber.text])
    {
        [self viewIsErrorStyle:YES];
        isValid= NO;
    }
    if(self.txtPassword.text.length == 0 )
    {
        isValid = NO;
        [self.txtPassword showErrorTextField:YES];
    }
   
    return isValid;
}
-(void)clearPageError
{
    [self.lblSubTitle shoppingSubTitleStyle:@""];
    [self viewIsErrorStyle:NO];
    [self.txtPassword showErrorTextField:NO];
}
#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==self.txtMobileNumber ){
        [self viewIsErrorStyle:NO];
        if([string isEqualToString:@""])
            return YES;
        if(textField.text.length<9)
        {
            return YES;
        }
        else
            return NO;
    }
    else if (textField == self.txtPassword)
    {
        [textField showErrorTextField:NO];
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender== self.btnSendOTP)
    {
        if([self isPageValid])
        {
           [self clearPageError];
            [self customerLogin];
        }
    }
    else if (sender==self.btnViewPassword)
    {
        self.txtPassword.secureTextEntry = !self.txtPassword.secureTextEntry;
        if(self.txtPassword.secureTextEntry)
            [self.btnViewPassword setTitle:[languageManager languageStringForKey:@"View"] forState:UIControlStateNormal];
        else
            [self.btnViewPassword setTitle:[languageManager languageStringForKey:@"Hide"] forState:UIControlStateNormal];
    }
}

#pragma mark - Network calls

-(void)customerLogin
{
    [[NetworkManager sharedManager] customerLogin:self.txtMobileNumber.text password:self.txtPassword.text success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            
            NSError *error;
            CustomerModel *customer = [[CustomerModel alloc] initWithDictionary:[responseData objectForKey:@"data"] error:&error];
          
            [CustomerModel updateLocalCustomerData:customer];
            if([[CartManager sharedManager]isCheckOutClicked]){
                self.tabBarController.selectedIndex = 2;
            }
            [myAppDelegate loadProfileTab];
            
    
        }
        else
        {
            [self.lblSubTitle shoppingSubTitleStyle:[responseData objectForKey:@"error"][0]];
            [self viewIsErrorStyle:YES];
            [self.txtPassword showErrorTextField:YES];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
         [Utility showErrorAlertView:self.navigationController];
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"forgotPassword"])
    {
        NumberVerificationViewController *ctrlNumberVerification =[segue destinationViewController];
        ctrlNumberVerification.isFromProfileUpdate = NO;
        ctrlNumberVerification.isFromForgotPassword = YES;
        
    }
}

@end

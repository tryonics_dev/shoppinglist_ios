//
//  OTPViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/22/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "OTPViewController.h"

@interface OTPViewController ()

@end

@implementation OTPViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    // Do any additional setup after loading the view.
    
#ifdef DEBUG
//    self.txtOne.text = @"0";
//    self.txtTwo.text = @"0";
//    self.txtThree.text = @"0";
//    self.txtFour.text = @"0";
    
#endif
}
-(void)viewWillAppear:(BOOL)animated
{
    
}
-(void)viewDidAppear:(BOOL)animated
{
    
    [self.txtOne becomeFirstResponder];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [textField showErrorTextField:NO];
    if([string isEqualToString:@""])
        return YES;
    if(textField.text.length<1)
    {
        
        return YES;
    }
    else{
        if(textField == self.txtOne)
            [self.txtTwo becomeFirstResponder];
        else if (textField == self.txtTwo)
            [self.txtThree becomeFirstResponder];
        else if (textField == self.txtThree)
            [self.txtFour becomeFirstResponder];
        
        return NO;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@""] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [self.navigationController.navigationBar setHidden:NO];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    [self.lblTitle shoppingTitleStyle: [languageManager languageStringForKey:@"Enter OTP Code"]];
    [self.lblSubTitle shoppingSubTitleStyle:[languageManager languageStringForKey:@"A Verification SMS will be sent your phone"]];
    [self.lblVerificationCode shoppingTextLableStyle:[languageManager languageStringForKey:@"Enter verification code"]];
   
   [self.btnVerify shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Verify"]];
    [self.btnResendOtp setTitle:[languageManager languageStringForKey:@"Resend OTP"] forState:UIControlStateNormal];
    
    if(IS_IPHONE_5)
    {
        self.constrainTitleTop.constant = 0;
        self.constrainSubTitleTop.constant =15;
        self.constrainButtonTop.constant =15;
        self.constrainSubSubTitleTop.constant = 0;
    }
    
    [self.txtOne shoppingListGreenRoundedCorner];
    [self.txtTwo shoppingListGreenRoundedCorner];
    [self.txtThree shoppingListGreenRoundedCorner];
    [self.txtFour shoppingListGreenRoundedCorner];
    
    [self.btnResendOtp setHidden:YES];
    [self startOTPTImeoutCounter];
    
   
}
-(void)startOTPTImeoutCounter
{
    self.expiryTime = 120;
    self.lblExpiryTime.text = [NSString stringWithFormat:@"%@ %i %@",[languageManager languageStringForKey:@"OTP Expires in"],self.expiryTime,[languageManager languageStringForKey:@"seconds"]];
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(expiryTimeCounter:)
                                   userInfo:nil
                                    repeats:YES];
}
-(void)expiryTimeCounter:(NSTimer *)timer
{
    self.expiryTime -= 1;
    if(self.expiryTime < 1 )
    {
        [timer invalidate];
        timer= nil;
        self.lblExpiryTime.text = [languageManager languageStringForKey:@"OTP expired"];
        [self.btnResendOtp setHidden:NO];
    }else{
        self.lblExpiryTime.text = [NSString stringWithFormat:@"%@ %i %@",[languageManager languageStringForKey:@"OTP Expires in"],self.expiryTime,[languageManager languageStringForKey:@"seconds"]];
    }
}
-(void)showErrorTextFieldStyle
{
    [self.txtOne showErrorTextField:YES];
    [self.txtTwo showErrorTextField:YES];
    [self.txtThree showErrorTextField:YES];
    [self.txtFour showErrorTextField:YES];
}

-(void)showMobleNumberUpdateMessage
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:[languageManager languageStringForKey:@"Phone number successfully updated."]
                                preferredStyle:UIAlertControllerStyleAlert];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    [self performSelector:@selector(dismissAutoHideAlert:) withObject:alert afterDelay:3];
    
}

-(void)dismissAutoHideAlert:(UIAlertController *)alertView{
    [alertView dismissViewControllerAnimated:NO completion:nil];
    
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnVerify)
    {
        BOOL isValid = YES;
        if([self.txtOne.text isEqualToString:@""])
        {
            [self.txtOne showErrorTextField:YES];
            isValid = NO;
        }
        if([self.txtTwo.text isEqualToString:@""])
        {
            [self.txtTwo showErrorTextField:YES];
            isValid = NO;
        }
        if([self.txtThree.text isEqualToString:@""])
        {
            [self.txtThree showErrorTextField:YES];
            isValid = NO;
        }
        if([self.txtFour.text isEqualToString:@""])
        {
            [self.txtFour showErrorTextField:YES];
            isValid = NO;
        }
        if(isValid)
        {
            [self verifyOTP];
        }
        else
        {
            [Utility showMessageAlertView:self.navigationController message:[languageManager languageStringForKey:@"OTP cannot be empty"]];
        }
    }
    else if (sender == self.btnResendOtp)
    {
        [self reRequestOTP];
    }
}

#pragma mark - Network calls

-(void)reRequestOTP
{
    [[NetworkManager sharedManager] getOTP:self.strPhoneNumber type:3 success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            
           [Utility showMessageAlertView:self.navigationController message:[self->languageManager languageStringForKey:@"OTP successfully resent"]];
           [self.btnResendOtp setHidden:YES];
           [self startOTPTImeoutCounter];
        }
        else
        {
            [Utility showErrorAlertView:self.navigationController];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [Utility showErrorAlertView:self.navigationController];
    }];
}
-(void)verifyOTP
{
    NSString *strOTP = [NSString stringWithFormat:@"%@%@%@%@",self.txtOne.text,self.txtTwo.text,self.txtThree.text,self.txtFour.text];
   
    int otpType = 2;
    if(self.isFromProfileUpdate)
        otpType = 7;
    else if (self.isFromForgotPassword)
        otpType = 5;
    
    [[NetworkManager sharedManager] confirmOTP:self.strPhoneNumber type:otpType otp:strOTP success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            
            NSDictionary *dicData = [responseData objectForKey:@"data"];
            if(self.isFromForgotPassword)
            {
                ChangePasswordViewController *ctrlChangePassword = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
                ctrlChangePassword.isFromForgotPassword = self.isFromForgotPassword;
                ctrlChangePassword.strTelephoneNumber = self.strPhoneNumber;
                [self.navigationController pushViewController:ctrlChangePassword animated:YES];
            }
            else
            {
                BOOL customerExist = [[dicData objectForKey:@"customer_exist"] boolValue];
                if(customerExist)
                {
                    if(!self.isFromProfileUpdate){
                        SignInViewController *ctrlSignIn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
                        [self.navigationController pushViewController:ctrlSignIn animated:YES];
                    }
                    [Utility showMessageAlertView:self.navigationController message:[self->languageManager languageStringForKey:@"This number already registered. Please login using phone number and passowrd."]];
                }else
                {
                    if(self.isFromProfileUpdate){
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [self updateCustomerMobileNumber];
                        });
                    }
                    else{
                        SignUpViewController *ctrlSignUp = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
                        ctrlSignUp.strPhoneNumber= self.strPhoneNumber;
                        [self.navigationController pushViewController:ctrlSignUp animated:YES];
                    }
                }
            }
        }
        else
        {
           [self.txtOne showErrorTextField:YES];
           [self.txtTwo showErrorTextField:YES];
           [self.txtThree showErrorTextField:YES];
           [self.txtFour showErrorTextField:YES];
           [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
            self.lblSubTitle.text = [responseData objectForKey:@"error"][0];
        }
        
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
         [Utility showErrorAlertView:self.navigationController];
    }];
}

-(void)updateCustomerMobileNumber
{
    CustomerModel *customerModel = [CustomerModel getSavedCustomerObject];
    customerModel.telephone = [NSString stringWithFormat:@"0%@",self.strPhoneNumber] ;
   
    [[NetworkManager sharedManager] updateCustomer:[customerModel toDictionary] success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            
            customerModel.telephone = [NSString stringWithFormat:@"0%@",self.strPhoneNumber];
            [CustomerModel updateLocalCustomerData:customerModel];
            [self showMobleNumberUpdateMessage];
        }
        else
        {
            [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [Utility showErrorAlertView:self.navigationController];
    }];
}

@end

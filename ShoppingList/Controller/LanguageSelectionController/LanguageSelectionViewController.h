//
//  LanguageSelectionViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/5/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LanguageSelectionViewController : UIViewController
{
    LanguageManager *languageManager;
}
@property (weak,nonatomic)IBOutlet UILabel *lblTitle,*lblSubTitle;
@property (weak,nonatomic)IBOutlet UIButton *btnSinhala,*btnTamil,*btnEnglish;
@property (weak,nonatomic)IBOutlet UIImageView *imgLogo;
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *constrainTop;

@property (nonatomic) BOOL IsFromProfile;
@end

NS_ASSUME_NONNULL_END

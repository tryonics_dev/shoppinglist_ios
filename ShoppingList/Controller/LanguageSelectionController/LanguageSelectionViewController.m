//
//  LanguageSelectionViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/5/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "LanguageSelectionViewController.h"

@interface LanguageSelectionViewController ()

@end

@implementation LanguageSelectionViewController

@synthesize btnTamil,btnEnglish,btnSinhala;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    // Do any additional setup after loading the view.
}

#pragma  mark - Userdefine methods

-(void)initUI
{
    if(self.IsFromProfile)
    {
       
        languageManager = [LanguageManager sharedManager];
        [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"Language"] navigationItem:self.navigationItem];
        [self.navigationController initShoppingListStyle];
        [self.navigationController.navigationBar setHidden:NO];
        [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
         self.view.backgroundColor =[UIColor whiteColor];
        
        [self.btnSinhala shoppingClearRoundedCorner:@"සිංහල"];
        [self.btnTamil shoppingClearRoundedCorner:@"தமிழ்"];
        [self.btnEnglish shoppingClearRoundedCorner:@"English"];
        
        [self.lblTitle setTextColor:[UIColor darkGrayColor]];
        [self.lblSubTitle setTextColor:[UIColor darkGrayColor]];
        
        [self.imgLogo setImage: [UIImage imageNamed:@"languageLogoGreen"]];
        
        if(IS_IPHONE_5)
        {
            self.constrainTop.constant = 20;
        }
    }
    else
    {
        self.view.backgroundColor =[myAppDelegate colourBackground];
        [myAppDelegate setStatusBarBackgroundColor:[myAppDelegate colourBackground]];
        [self.btnSinhala shoppingTransparentRoundedCorner:@"සිංහල"];
        [self.btnTamil shoppingTransparentRoundedCorner:@"தமிழ்"];
        [self.btnEnglish shoppingTransparentRoundedCorner:@"English"];
    }
    
}

#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if(sender == self.btnSinhala)
    {
        [defaults setObject:LANGUAGE_CODE_SINHALA forKey:SELECTED_LANGUAGE_KEY];
    }
    else if (sender == self.btnTamil)
    {
        [defaults setObject:LANGUAGE_CODE_TAMIL forKey:SELECTED_LANGUAGE_KEY];
    }
    else if (sender == self.btnEnglish)
    {
        [defaults setObject:LANGUAGE_CODE_ENGLISH forKey:SELECTED_LANGUAGE_KEY];
    }
    [defaults synchronize];
    
    [[LanguageManager sharedManager] updateLanguage];
    [[NetworkManager sharedManager] updateLanguage];
    
    [myAppDelegate loadMainTab];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

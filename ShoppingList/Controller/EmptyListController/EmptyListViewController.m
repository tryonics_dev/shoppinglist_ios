//
//  EmptyListViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/7/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "EmptyListViewController.h"

@interface EmptyListViewController ()

@end

@implementation EmptyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.btnRetry shoppingGreenRoundedCorner:[[LanguageManager sharedManager] languageStringForKey:@"RETRY"]];
    self.lblMessage.text =[[LanguageManager sharedManager] languageStringForKey:@"No item found at this time."]; 
    // Do any additional setup after loading the view from its nib.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

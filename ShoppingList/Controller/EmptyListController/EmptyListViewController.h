//
//  EmptyListViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/7/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmptyListViewController : UIViewController
{
    
}
@property (weak,nonatomic)IBOutlet UILabel *lblMessage;
@property (weak,nonatomic)IBOutlet UIButton *btnRetry;
@end

NS_ASSUME_NONNULL_END

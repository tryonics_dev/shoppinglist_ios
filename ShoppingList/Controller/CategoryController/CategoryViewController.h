//
//  CategoryViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/6/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoriesModel.h"
#import "CategoryCollectionViewCell.h"
#import "SubCategoriesViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface CategoryViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    LanguageManager *languageManager;
    NetworkManager *networkManager;
    EmptyListViewController *emptyView;
}
@property (weak,nonatomic)IBOutlet UICollectionView *collectionview;
@property (strong, nonatomic) CategoriesModel *categories;
@property (strong , nonatomic)UIRefreshControl* refreshController;

@end

NS_ASSUME_NONNULL_END

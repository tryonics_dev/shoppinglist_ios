//
//  SubCategoriesViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/8/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "SubCategoriesViewController.h"

@interface SubCategoriesViewController ()

@end

@implementation SubCategoriesViewController
@synthesize subCategories,collectionview,refreshController,parentCategoryId,parentCategoryName;

- (void)viewDidLoad {
    [super viewDidLoad];
    networkManager = [NetworkManager sharedManager];
    [self initUI];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self loadSubCategories];
}
#pragma mark - CollectionView Delegate/DataSource


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(self.subCategories !=nil)
        return self.subCategories.sub_categories.count;
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CategoryCollectionViewCell *cell;
    if(cell == nil){
        cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCollectionViewCell" forIndexPath:indexPath];
    }
    
    SubCategoryModel *subCategory = [self.subCategories.sub_categories objectAtIndex:indexPath.row];
    cell.lblTitle.text = subCategory.name;
    
    [cell.imgCategory setShowActivityIndicatorView:YES];
    [cell.imgCategory sd_setImageWithURL:[NSURL URLWithString:subCategory.image]
                        placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                                 options:SDWebImageRefreshCached];
    
    return cell;
    
}
-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    SubCategoryModel *subCategory = [self.subCategories.sub_categories objectAtIndex:indexPath.row];
    SubSubCategoryViewController *ctrlSubCategories = [[SubSubCategoryViewController alloc] initWithNibName:@"SubSubCategoryViewController" bundle:nil];
    ctrlSubCategories.parentCategoryName = subCategory.name;
    ctrlSubCategories.parentCategoryId = subCategory.category_id;
    [self.navigationController pushViewController:ctrlSubCategories animated:YES];
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE_5)
        return COLLECTION_VIEW_CELL_SIZE_IPHONE5;
    
    return COLLECTION_VIEW_CELL_SIZE;
    
}
#pragma mark - Userdefine methods

-(void)initUI
{
     [self.collectionview registerNib:[UINib nibWithNibName:@"CategoryCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CategoryCollectionViewCell"];
    
    [self.navigationController.navigationBar setHidden:NO];
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:self.parentCategoryName navigationItem:self.navigationItem];
    
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor = APP_THEME_LIGHT_GREEN;
    [refreshController addTarget:self action:@selector(loadSubCategories) forControlEvents:UIControlEventValueChanged];
    
    collectionview.refreshControl = refreshController;
    
    emptyView = [[EmptyListViewController alloc] initWithNibName:@"EmptyListViewController" bundle:nil];
    emptyView.view.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
    emptyView.lblMessage.text = [languageManager languageStringForKey:@"We cannot find any categrory list."];
    [emptyView.btnRetry addTarget:self action:@selector(loadSubCategories) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)handleEmptyView:(BOOL)isShow
{
    if(isShow)
    {
        [self.collectionview setHidden:YES];
        [self.view addSubview:emptyView.view];
    }
    else
    {
        [self.collectionview setHidden:NO];
        [emptyView.view removeFromSuperview];
    }
}
#pragma mark - Network calls

-(void)loadSubCategories
{
    [networkManager getSubCategoriesByParentID:parentCategoryId success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            NSError *error;
            self.subCategories =[[SubCategoriesModel alloc] initWithDictionary:[responseData objectForKey:@"data"] error:&error];
            [self.collectionview reloadData];
            [self handleEmptyView:NO];
            if(self.subCategories.sub_categories.count ==0)
            {
                [self handleEmptyView:YES];
            }
        }
        else
        {
            [self handleEmptyView:YES];
        }
        [self->refreshController endRefreshing];
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [self handleEmptyView:YES];
        [self->refreshController endRefreshing];
    }];
    
}

@end

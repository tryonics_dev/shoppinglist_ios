//
//  CategoryViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/6/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "CategoryViewController.h"

@interface CategoryViewController ()

@end

@implementation CategoryViewController

@synthesize categories,collectionview,refreshController;
- (void)viewDidLoad {
    [super viewDidLoad];
    networkManager = [NetworkManager sharedManager];
    [self initUI];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self loadCategories];
}
#pragma mark - CollectionView Delegate/DataSource


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    if(self.categories !=nil)
        return self.categories.data.count;
  
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
   

    CategoryCollectionViewCell *cell;
    if(cell == nil){
        cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCollectionViewCell" forIndexPath:indexPath];
    }
    
    CategoryModel *category = [self.categories.data objectAtIndex:indexPath.row];
    cell.lblTitle.text = category.name;
    
    [cell.imgCategory setShowActivityIndicatorView:YES];
    [cell.imgCategory sd_setImageWithURL:[NSURL URLWithString:category.image]
                        placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                                 options:SDWebImageRefreshCached];

    return cell;
    
}
-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryModel *category = [self.categories.data objectAtIndex:indexPath.row];
    SubCategoriesViewController *ctrlSubCategories = [[SubCategoriesViewController alloc] initWithNibName:@"SubCategoriesViewController" bundle:nil];
    ctrlSubCategories.parentCategoryName = category.name;
    ctrlSubCategories.parentCategoryId = category.category_id;
    [self.navigationController pushViewController:ctrlSubCategories animated:YES];
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE_5)
        return COLLECTION_VIEW_CELL_SIZE_IPHONE5;
    else
        return COLLECTION_VIEW_CELL_SIZE;
    
}
#pragma mark - Userdefine methods

-(void)initUI
{
    
    [self.collectionview registerNib:[UINib nibWithNibName:@"CategoryCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CategoryCollectionViewCell"];
    
    [self.navigationController.navigationBar setHidden:NO];
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"All Categories"] navigationItem:self.navigationItem];
   
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor = APP_THEME_LIGHT_GREEN;
    [refreshController addTarget:self action:@selector(loadCategories) forControlEvents:UIControlEventValueChanged];
    
    collectionview.refreshControl = refreshController;
    
    emptyView = [[EmptyListViewController alloc] initWithNibName:@"EmptyListViewController" bundle:nil];
    emptyView.view.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
    emptyView.lblMessage.text = [languageManager languageStringForKey:@"We cannot find any categrory list."];
    [emptyView.btnRetry addTarget:self action:@selector(loadCategories) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)handleEmptyView:(BOOL)isShow
{
    if(isShow)
    {
        [self.collectionview setHidden:YES];
        [self.view addSubview:emptyView.view];
    }
    else
    {
        [self.collectionview setHidden:NO];
        [emptyView.view removeFromSuperview];
    }
}
#pragma mark - Network calls

-(void)loadCategories
{
    [networkManager getAllCategories:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            
            NSError *error;
            self.categories =[[CategoriesModel alloc] initWithDictionary:responseData error:&error];
            [self.collectionview reloadData];
            [self handleEmptyView:NO];
            if(self.categories.data.count ==0)
            {
                 [self handleEmptyView:YES];
            }
            
        }
        else
        {
            [self handleEmptyView:YES];
        }
        [self->refreshController endRefreshing];
        
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
        [self handleEmptyView:YES];
        [self->refreshController endRefreshing];
    }];
   
}

@end

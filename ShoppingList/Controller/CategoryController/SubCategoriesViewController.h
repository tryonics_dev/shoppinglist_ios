//
//  SubCategoriesViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/8/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubCategoriesModel.h"
#import "CategoryCollectionViewCell.h"
#import "SubSubCategoryViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SubCategoriesViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    LanguageManager *languageManager;
    NetworkManager *networkManager;
    EmptyListViewController *emptyView;
}
@property(nonatomic)NSInteger parentCategoryId;
@property(strong,nonatomic)NSString *parentCategoryName;
@property (weak,nonatomic)IBOutlet UICollectionView *collectionview;
@property (strong, nonatomic) SubCategoriesModel *subCategories;
@property (strong , nonatomic)UIRefreshControl* refreshController;
@end

NS_ASSUME_NONNULL_END

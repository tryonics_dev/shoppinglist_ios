//
//  ProductListViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/8/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListModel.h"
#import "ProductCollectionViewCell.h"
#import "ProductFiltersViewController.h"
#import "ProductDetailViewController.h"
#import "SpecialProductListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductListViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,ProductFiltersDelegate>
{
    LanguageManager *languageManager;
    NetworkManager *networkManager;
    EmptyListViewController *emptyView;
    NSInteger currentPage;
    NSArray *arrSelectedFilters;
}
@property (weak, nonatomic) IBOutlet UIButton *btnFilters;
@property (weak,nonatomic)IBOutlet UILabel *lblResultCount;
@property (weak,nonatomic)IBOutlet UICollectionView *collectionview;

@property (strong, nonatomic) ProductListModel *productListModel;
@property (strong , nonatomic) SpecialProductListModel *specialProductListModel;
@property (strong , nonatomic)UIRefreshControl* refreshController,*bottomRefreshControl;
@property (strong,nonatomic)ProductFiltersViewController *filterViewController;

@property(nonatomic)NSInteger selectedCategoryId;
@property(strong,nonatomic)NSString *selectedCategoryName;
@property (strong, nonatomic)NSMutableArray *arrProducts;
@property (strong,nonatomic )NSArray *arrFilterGroups;
@property (nonatomic)BOOL isSpecials,isLatest;

@end

NS_ASSUME_NONNULL_END

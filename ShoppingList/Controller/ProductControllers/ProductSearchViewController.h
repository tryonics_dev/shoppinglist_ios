//
//  ProductSearchViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/13/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListModel.h"
#import "ProductCollectionViewCell.h"
#import "ProductDetailViewController.h"
#import "ProductFiltersViewController.h"
#import "FilterGroupListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductSearchViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UISearchBarDelegate,ProductFiltersDelegate>
{
    LanguageManager *languageManager;
    NetworkManager *networkManager;
    EmptyListViewController *emptyView;
    NSArray *arrSelectedFilters;
    NSString *strCategoryId;
}
@property (weak,nonatomic)IBOutlet UICollectionView *collectionview;
@property (nonatomic ,weak)IBOutlet UISearchBar *searchBar;
@property (nonatomic,weak)IBOutlet UIView *viewSearch;
@property (weak,nonatomic)IBOutlet UILabel *lblResultCount;
@property (weak,nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak,nonatomic) IBOutlet UIButton *btnFilter;

@property (strong,nonatomic)ProductFiltersViewController *filterViewController;
@property (strong , nonatomic) ProductListModel *productListModel;


@end

NS_ASSUME_NONNULL_END

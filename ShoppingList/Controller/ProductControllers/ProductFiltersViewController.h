//
//  ProductFiltersViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/9/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductFilterTableViewCell.h"
#import "FilterGroupListModel.h"
#import "FilterGroupModel.h"
#import "ProductFilterModel.h"
#import "ProductFilterCollectionViewCell.h"
#import "HeaderCollectionReusableView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ProductFiltersDelegate <NSObject>
@required
- (void)didApplyFilters:(NSArray *)selectedFilters;

@end
@interface ProductFiltersViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSInteger view_height,view_width;
    NSArray *arrFilters;
    NSMutableArray *arrSelectedFilters;
}
@property(strong,nonatomic)id<ProductFiltersDelegate> delegate;

@property (weak ,nonatomic) IBOutlet UIButton *btnApply,*btnClose;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewFilter;

@property (nonatomic) BOOL isShowing;
@property (strong,nonatomic )NSArray *arrFilterGroups;
@property (nonatomic,weak)UINavigationController *navController;


-(void)setBaseViwe:(UIView *)baseView;
-(void)show;
-(void)showWithSelectedFilter:(NSArray *)filters;
-(void)hide;
@end

NS_ASSUME_NONNULL_END

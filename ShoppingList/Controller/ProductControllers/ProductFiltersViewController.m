//
//  ProductFiltersViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/9/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ProductFiltersViewController.h"

@interface ProductFiltersViewController ()

@end

@implementation ProductFiltersViewController
@synthesize isShowing,
btnApply,
arrFilterGroups;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    
    // Do any additional setup after loading the view from its nib.
}
/*
#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrFilterGroups.count;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    FilterGroupModel *filterModel = self.arrFilterGroups[section];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width, 30)];
    [label setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:14.0]];
    [label setTextColor:[UIColor darkGrayColor]];
    NSString *string =filterModel.name;
  
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    
    return view;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    FilterGroupModel *filterModel = self.arrFilterGroups[section];
    return filterModel.filter.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *IdentifierContent = @"IdentifierContent"; //Set Identifier for cell
    ProductFilterTableViewCell *cell = (ProductFilterTableViewCell *)[tableView dequeueReusableCellWithIdentifier: IdentifierContent];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductFilterTableViewCell"
                                                     owner:self options:nil];
        for (id oneObject in nib) if ([oneObject isKindOfClass:[ProductFilterTableViewCell class]])
            cell = (ProductFilterTableViewCell *)oneObject;
        
        cell.backgroundColor = [UIColor clearColor];
       
    }
    ProductFilterModel *productFilter = ((FilterGroupModel *)self.arrFilterGroups[indexPath.section]).filter[indexPath.row];
    cell.lblFilterName.text = productFilter.name;
    cell.btnCheckBox.tag = productFilter.filter_id;
    
    int selectedFilterId = (int)productFilter.filter_id;
    if([arrSelectedFilters containsObject:[NSNumber numberWithInt:selectedFilterId]])
    {
        [cell.imgCheckBox setImage:[UIImage imageNamed:@"checked"]];
    }
    else
    {
        [cell.imgCheckBox setImage:[UIImage imageNamed:@"unchecked"]];
    }
    return cell;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
*/

#pragma mark - CollectionView Delegate/DataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return arrFilterGroups.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    FilterGroupModel *filterModel = self.arrFilterGroups[section];
    return filterModel.filter.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ProductFilterCollectionViewCell *cell;
    if(cell == nil){
        cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductFilterCollectionViewCell" forIndexPath:indexPath];
    }
    
    ProductFilterModel *productFilter = ((FilterGroupModel *)self.arrFilterGroups[indexPath.section]).filter[indexPath.row];
    cell.lblFilterTitle.text = productFilter.name;
    cell.btnFilter.tag = productFilter.filter_id;
    int selectedFilterId = (int)productFilter.filter_id;
    if([arrSelectedFilters containsObject:[NSNumber numberWithInt:selectedFilterId]])
    {
        [cell.imgFilter setImage:[UIImage imageNamed:@"checked"]];
    }
    else
    {
        [cell.imgFilter setImage:[UIImage imageNamed:@"unchecked"]];
    }
    return cell;
    
}
-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        HeaderCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        FilterGroupModel *filterModel = self.arrFilterGroups[indexPath.section];
        headerView.title.text = filterModel.name;
        reusableview = headerView;
        
    }
    
    
    return reusableview;
}
#pragma mark - UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductFilterModel *productFilter = ((FilterGroupModel *)self.arrFilterGroups[indexPath.section]).filter[indexPath.row];
        
    return [self getSizeOfText:productFilter.name];
}


#pragma mark - User define methods

-(CGSize)getSizeOfText:(NSString *)text{
    
    NSInteger width = [text sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"SFUIDisplay-Medium" size:14]}].width+50 ;
    return CGSizeMake(width, 35);
}
-(void)initUI
{
//    self.view.layer.borderColor = APP_THEME_LIGHT_GREEN.CGColor;
//    self.view.layer.borderWidth = 1.0f;
//    self.view.layer.cornerRadius = VIEW_CORNER_RADIUS;
//    self.view.clipsToBounds = NO;
//    self.view.layer.masksToBounds = NO;
    
     [self.collectionViewFilter registerNib:[UINib nibWithNibName:@"ProductFilterCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ProductFilterCollectionViewCell"];
    
    [self.collectionViewFilter registerNib:[UINib nibWithNibName:@"HeaderCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
     UIWindow *window = UIApplication.sharedApplication.keyWindow;
    
    if(IS_IPHONE_5)
        view_height = window.frame.size.height - 48;
    else
        view_height = window.frame.size.height - 80;
   
    
    [self.view setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, view_height)];
    [self.btnApply shoppingGreenRoundedCorner:[[LanguageManager sharedManager] languageStringForKey:@"Apply"]];
    [self.btnClose setTitle:[[LanguageManager sharedManager] languageStringForKey:@"Close"] forState:UIControlStateNormal];
    
    arrSelectedFilters = [[NSMutableArray alloc] init];
}
-(void)setBaseViwe:(UIView *)baseView
{
    isShowing = false;
    [baseView addSubview:self.view];
}
-(void)show
{
    isShowing = YES;
    [UIView animateWithDuration: 0.4 animations:^{
        [self.view setFrame:CGRectMake(0,0, SCREEN_WIDTH, self->view_height)];
    }];
   
}
-(void)showWithSelectedFilter:(NSArray *)filters
{
   arrSelectedFilters = [NSMutableArray arrayWithArray:filters];
   [self.collectionViewFilter reloadData];
   [self show];
}
-(void)hide
{
    isShowing = NO;
    [UIView animateWithDuration: 0.4 animations:^{
        [self.view setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, self->view_height)];
    }];
}

#pragma mark - Action methods

-(IBAction)filterClickHandler:(id)sender
{
    int selectedFilterId = (int)((UIButton *)sender).tag;
    if([arrSelectedFilters containsObject:[NSNumber numberWithInt:selectedFilterId]])
    {
        [arrSelectedFilters removeObject:[NSNumber numberWithInt:selectedFilterId]];
    }
    else
    {
        [arrSelectedFilters addObject:[NSNumber numberWithInt:selectedFilterId]];
    }
    [self.collectionViewFilter reloadData];
}
-(IBAction)buttonClick:(id)sender
{
    if(sender == btnApply)
    {
        [_delegate didApplyFilters:arrSelectedFilters];
    }
    else if (sender == self.btnClose)
    {
        [self hide];
    }
}


@end

//
//  ProductSearchViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/13/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ProductSearchViewController.h"

@interface ProductSearchViewController ()

@end

@implementation ProductSearchViewController
@synthesize collectionview,searchBar,viewSearch,lblResultCount,productListModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    strCategoryId = @"0";
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - Userdefine methods

-(void)initUI
{
    networkManager = [NetworkManager sharedManager];
    [self.navigationController.navigationBar setHidden:NO];
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"Search"] navigationItem:self.navigationItem];
    
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    self.searchBar.placeholder = [languageManager languageStringForKey:@"Search in ShoppingList"];
    
    self.viewSearch.backgroundColor = [UIColor clearColor];
    self.viewSearch.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.viewSearch.layer.borderWidth = 1;
    self.viewSearch.layer.cornerRadius = VIEW_CORNER_RADIUS;
    self.viewSearch.layer.masksToBounds = YES;
    
    [self.searchBar becomeFirstResponder];
    [self.activityIndicator stopAnimating];
    
    if (@available(iOS 13.0, *)) {
        searchBar.searchTextField.backgroundColor = [UIColor clearColor];
        searchBar.searchTextField.clearButtonMode = UITextFieldViewModeNever;
    }else{
        UITextField *textField = [searchBar valueForKey:@"_searchField"];
        textField.clearButtonMode = UITextFieldViewModeNever;
    }
     [self.btnFilter setTitle:[languageManager languageStringForKey:@"Filters"] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc]initWithCustomView:self.btnFilter]];
    
   
    self.filterViewController = [[ProductFiltersViewController alloc] initWithNibName:@"ProductFiltersViewController" bundle:nil];
//    self.filterViewController.arrFilterGroups = self.arrFilterGroups;
    self.filterViewController.delegate =self;
    [self.filterViewController setBaseViwe:self.navigationController.view];
    
    [self.btnFilter setHidden:YES];
   
}
-(void)handleEmptyView:(BOOL)isShow
{
    if(isShow)
    {
        [self.collectionview setHidden:YES];
        [self.view addSubview:emptyView.view];
    }
    else
    {
        [self.collectionview setHidden:NO];
        [emptyView.view removeFromSuperview];
    }
}
-(void)clearCollectionview
{
    productListModel = nil;
    [self.collectionview reloadData];
    [self.lblResultCount setHidden:YES];
}
-(void)setLblResultCount
{
    [self->lblResultCount setHidden:NO];
    self->lblResultCount.text =[NSString stringWithFormat:@"%lu %@", (unsigned long)self->productListModel.data.count,[self->languageManager languageStringForKey:@"Results"]];
     [self.activityIndicator stopAnimating];
}
#pragma mark - CollectionView Delegate/DataSource


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.productListModel.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ProductCollectionViewCell *cell;
    if(IS_IPHONE_5)
    {
        [collectionView registerNib:[UINib nibWithNibName:@"ProductCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ProductCollectionViewCell"];
        if(cell == nil){
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductCollectionViewCell" forIndexPath:indexPath];
        }
    }else{
        [collectionView registerNib:[UINib nibWithNibName:@"ProductLargeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ProductLargeCollectionViewCell"];
        if(cell == nil){
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductLargeCollectionViewCell" forIndexPath:indexPath];
        }
    }
    [cell.imgThumb setShowActivityIndicatorView:YES];
    
    
    ProductModel *product =  [self.productListModel.data objectAtIndex:indexPath.row];
    [cell.imgThumb sd_setImageWithURL:[NSURL URLWithString:product.image]
                     placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                              options:SDWebImageRefreshCached];
    cell.lblTitle.text = product.name;
    [cell.imgGiftIcon setHidden:!product.gift_flag];
    if(product.special > 0)
    {
        [cell.lblOldPrice setHidden:NO];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:product.price_formated];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                value:@1
                                range:NSMakeRange(0, [attributeString length])];
        cell.lblOldPrice.attributedText =attributeString;
        cell.lblPrice.text = product.special_formated;
    }
    else
    {
        [cell.lblOldPrice setHidden:YES];
        cell.lblPrice.text = product.price_formated;
    }
    
   
    return cell;
    
    
}
-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   ProductModel *product =  [self.productListModel.data objectAtIndex:indexPath.row];
    ProductDetailViewController *ctrlProductDetails = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
    ctrlProductDetails.productId = product.product_id;
    [self.navigationController pushViewController:ctrlProductDetails animated:YES];
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
}
#pragma mark - UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE_5)
        return COLLECTION_VIEW_CELL_PRODUCTS_SIZE_IPHONE5;
    
    return COLLECTION_VIEW_CELL_PRODUCTS_SIZE;
    
}
#pragma mark - Searchbar delegates

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length > 3){
        
        [self getSearchFilters:[searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [self searchProducts:[searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    else if (searchText.length == 0)
    {
        [self clearCollectionview];
        [self.btnFilter setHidden:YES];
    }
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    // Do the search...
}

#pragma mark - Product filters delegate

-(void)didApplyFilters:(NSArray *)selectedFilters
{
    [self.filterViewController hide];
    arrSelectedFilters = nil;
    arrSelectedFilters = [[NSArray alloc] initWithArray:selectedFilters];
    [self.btnFilter setBadge:[NSString stringWithFormat:@"%lu",(unsigned long)arrSelectedFilters.count]];
    [self searchProducts:[self.searchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
}

#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnFilter)
    {
        [self.view endEditing:YES];
        if([self.filterViewController isShowing])
            [self.filterViewController hide];
        else{
            
            [self.filterViewController showWithSelectedFilter:arrSelectedFilters];
        }
    }
}
#pragma mark - Network calls

-(void)getSearchFilters:(NSString *)searchText
{
    arrSelectedFilters  = nil;
    [self.btnFilter setBadge:@"0"];
    [networkManager getSearchProductFilters:searchText success:^(id  _Nonnull responseData) {
         BOOL success = [[responseData objectForKey:@"success"] boolValue];
         if(success){
             [self.btnFilter setHidden:NO];
             self->strCategoryId = responseData[@"data"][@"category_id"];
              NSError *error;
             FilterGroupListModel *filterGroupsList =[[FilterGroupListModel alloc] initWithDictionary:[responseData objectForKey:@"data"] error:&error];
             self->_filterViewController.arrFilterGroups = filterGroupsList.filter_groups;
             if(filterGroupsList != nil && filterGroupsList.filter_groups.count>0)
                 [self.btnFilter setHidden:NO];
             else
                 [self.btnFilter setHidden:YES];
         }
         else
         {
            [self.btnFilter setHidden:YES];
         }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [self.btnFilter setHidden:YES];
    }];
}

- (void)searchProducts:(NSString *)searchText
{
    if(self.activityIndicator.isHidden){
        [self.activityIndicator startAnimating];
    }
    NSMutableString *strFilters = [[NSMutableString alloc] initWithString:@""];
    for (id filterId in arrSelectedFilters) {
        [strFilters appendFormat:@"%@,",filterId];
    }
    if(strCategoryId == nil)
        strCategoryId = @"0";
    
    [networkManager searchProduct:searchText filters:strFilters categoryId:strCategoryId success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            NSError *error;
            self.productListModel =[[ProductListModel alloc] initWithDictionary:responseData error:&error];
           
            [self.collectionview reloadData];
            [self handleEmptyView:NO];
            if(self.productListModel.data.count ==0)
            {
               // [self handleEmptyView:YES];
            }
            [self setLblResultCount];
            
        }
        else
        {
           // [self handleEmptyView:YES];
           [self setLblResultCount];
        }
        if(self.searchBar.text.length == 0)
        {
             [self clearCollectionview];
        }
       
        
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
       // [self handleEmptyView:YES];
        [self setLblResultCount];
        if(self.searchBar.text.length == 0)
        {
            [self clearCollectionview];
        }
        
    }];
}

@end

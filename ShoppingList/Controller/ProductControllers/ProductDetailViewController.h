//
//  ProductDetailViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/10/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"
#import "ProductDetailTitleTableViewCell.h"
#import "ProductDetailDescriptionTableViewCell.h"
#import "ProductDetailFreeGiftTableViewCell.h"
#import "ProductDetailDiscountTableViewCell.h"
#import "NSString_stripHtml.h"
#import "KXHtmlLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProductDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    LanguageManager *languageManager;
    NetworkManager *networkManager;
    NSArray *arrDiscounts,*arrFreeGifts;
    int numberOfSectionInTableView;
    ProductModel *productModel;
    int itemCount ;
   
}
@property(weak,nonatomic)IBOutlet UIButton *btnCheckOut,*btnAddToCart,*btnClose;
@property(weak,nonatomic)IBOutlet UITableView *tableView;
@property(weak,nonatomic)IBOutlet UIView *viewPopUpBase,*viewPopUp,*viewButtonBase;
@property(weak,nonatomic)IBOutlet UITextView *textViewOffer;
@property(weak,nonatomic)IBOutlet UIImageView *imgOffer;
@property(weak,nonatomic)UITextField *lblItemCount;

@property (nonatomic)NSInteger productId;
@end

NS_ASSUME_NONNULL_END

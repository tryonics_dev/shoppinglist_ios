//
//  ProductDetailViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/10/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ProductDetailViewController.h"

@interface ProductDetailViewController ()

@end

@implementation ProductDetailViewController

@synthesize
btnCheckOut,
btnAddToCart,
tableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self loadProductDetail];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark - UITextField Delegates

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(self.lblItemCount.text.length == 0 && [string isEqualToString:@"0"])
    {
        return false;
    }
    else if (![string isEqualToString:@""] && self.lblItemCount.text.length > 3)
    {
        return false;
    }
    else{
        return true;
    }
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(self.lblItemCount.text.length == 0 || [self.lblItemCount.text intValue] == 0)
    {
        self.lblItemCount.text =@"1";
    }
    return YES;
}
#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return numberOfSectionInTableView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
        return 1;
    else if (section == 1)
    {
        if(arrDiscounts.count > 0)
            return arrDiscounts.count;
        else if(arrFreeGifts.count > 0)
            return arrFreeGifts.count;
        else
            return 1;
    }
    else if (section == 2)
    {
        if(arrDiscounts.count > 0 && arrFreeGifts.count > 0)
            return arrFreeGifts.count;
        else
            return 1;
    }
    else
        return 1;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
       return  [self sectionTitleView:@""];
    else if (section == 1)
    {
        if(arrDiscounts.count > 0)
            return  [self sectionTitleView:[languageManager languageStringForKey:@"Discount(s)"]];
        else if(arrFreeGifts.count > 0)
            return  [self sectionTitleView:[languageManager languageStringForKey:@""]];
        else
            return  [self sectionTitleView:[languageManager languageStringForKey:@"Description"]];
    }
    else if (section == 2)
    {
        if(arrDiscounts.count > 0 && arrFreeGifts.count > 0)
            return  [self sectionTitleView:[languageManager languageStringForKey:@""]];
        else
            return  [self sectionTitleView:[languageManager languageStringForKey:@"Description"]];
    }
    else
        return  [self sectionTitleView:[languageManager languageStringForKey:@"Description"]];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        static NSString *IdentifierContent = @"IdentifierContent"; //Set Identifier for cell
        ProductDetailTitleTableViewCell *cell = (ProductDetailTitleTableViewCell *)[tableView dequeueReusableCellWithIdentifier: IdentifierContent];
        
        if (cell == nil) {
            NSArray *nib;
            if(IS_IPHONE_5)
                nib= [[NSBundle mainBundle] loadNibNamed:@"ProductDetailTitleTableViewCell"
                                                         owner:self options:nil];
            else
                nib= [[NSBundle mainBundle] loadNibNamed:@"ProductDetailTitleLargeTableViewCell"
                                                   owner:self options:nil];
            
            for (id oneObject in nib) if ([oneObject isKindOfClass:[ProductDetailTitleTableViewCell class]])
                cell = (ProductDetailTitleTableViewCell *)oneObject;
            
            cell.backgroundColor = [UIColor clearColor];
            [cell.btnUp addTarget:self action:@selector(increaseItemCount:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnDown addTarget:self action:@selector(decreaseItemCount:) forControlEvents:UIControlEventTouchUpInside];
            self.lblItemCount = cell.lblItemCount;
            self.lblItemCount.delegate = self;
            
        }
        cell.lblItemCount.text = [NSString stringWithFormat:@"%i",itemCount];
        
        if(productModel!=nil){
            [cell loadProductImages:productModel.images image:productModel.image];
            cell.lblTitle.text = productModel.name;
            [cell.lblTitle sizeToFit];
            if(productModel.special > 0)
            {
                [cell.lblOldPrice setHidden:NO];
                NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:productModel.price_formated];
                [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                        value:@1
                                        range:NSMakeRange(0, [attributeString length])];
                cell.lblOldPrice.attributedText =attributeString;
                
                cell.lblPrice.text = productModel.special_formated;
            }
            else
            {
                [cell.lblOldPrice setHidden:YES];
                cell.lblPrice.text = productModel.price_formated;
            }
        }
        return cell;
    }
    else if (indexPath.section == 1)
    {
        if(arrDiscounts.count > 0)
        {
            static NSString *IdentifierContent = @"IdentifierContent"; //Set Identifier for cell
            ProductDetailDiscountTableViewCell *cell = (ProductDetailDiscountTableViewCell *)[tableView dequeueReusableCellWithIdentifier: IdentifierContent];
            
            if (cell == nil) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductDetailDiscountTableViewCell"
                                                             owner:self options:nil];
                for (id oneObject in nib) if ([oneObject isKindOfClass:[ProductDetailDiscountTableViewCell class]])
                    cell = (ProductDetailDiscountTableViewCell *)oneObject;
                
                cell.backgroundColor = [UIColor clearColor];
                
            }
            ProductDiscountModel *discount =  arrDiscounts[indexPath.row];
            NSString *strDiscount = [NSString stringWithFormat:@"%li %@ %@", (long)discount.quantity,[languageManager languageStringForKey:@"or more"],discount.price_formated];
            cell.lblDiscount.text = strDiscount;
            return cell;
        }
        else
        {
            if(arrFreeGifts.count > 0)
                return [self productDetailFreeGiftCell:tableView IndexPath:indexPath];
            else
               return [self productDetailDescriptionCell:tableView];
        }
    }
    else if (indexPath.section == 2)
    {
        if(arrDiscounts.count > 0 && arrFreeGifts.count > 0)
        {
             return [self productDetailFreeGiftCell:tableView IndexPath:indexPath];
        }
        else
        {
           return [self productDetailDescriptionCell:tableView];
        }
    }
    else
    {
        return [self productDetailDescriptionCell:tableView];
    }
}
-(ProductDetailFreeGiftTableViewCell *)productDetailFreeGiftCell:(UITableView *)tableView
IndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *IdentifierContent = @"IdentifierContent"; //Set Identifier for cell
    ProductDetailFreeGiftTableViewCell *cell = (ProductDetailFreeGiftTableViewCell *)[tableView dequeueReusableCellWithIdentifier: IdentifierContent];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductDetailFreeGiftNewTableViewCell"
                                                     owner:self options:nil];
        for (id oneObject in nib) if ([oneObject isKindOfClass:[ProductDetailFreeGiftTableViewCell class]])
            cell = (ProductDetailFreeGiftTableViewCell *)oneObject;
        
        cell.backgroundColor = [UIColor clearColor];
        [cell.btnPopUp addTarget:self action:@selector(toggleOfferPopUp) forControlEvents:UIControlEventTouchUpInside];
        
       
    }
    ProductFreeGiftModel *gift = arrFreeGifts[indexPath.row];
    cell.lblCondition.text =gift.condition;
    [cell.imgGift sd_setImageWithURL:[NSURL URLWithString:gift.image ]
                     placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                              options:SDWebImageRefreshCached];
    
    self.textViewOffer.text = gift.condition;
    [self.imgOffer sd_setImageWithURL:[NSURL URLWithString:gift.image]
                     placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                              options:SDWebImageRefreshCached];
    
    
    
    return cell;
}
-(ProductDetailDescriptionTableViewCell *)productDetailDescriptionCell:(UITableView *)tableView
{
    
    static NSString *IdentifierContent = @"IdentifierContent"; //Set Identifier for cell
    ProductDetailDescriptionTableViewCell *cell = (ProductDetailDescriptionTableViewCell *)[tableView dequeueReusableCellWithIdentifier: IdentifierContent];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductDetailDescriptionTableViewCell"
                                                     owner:self options:nil];
        for (id oneObject in nib) if ([oneObject isKindOfClass:[ProductDetailDescriptionTableViewCell class]])
            cell = (ProductDetailDescriptionTableViewCell *)oneObject;
        
        cell.backgroundColor = [UIColor clearColor];
        
    }
    [cell.lblDescription setHtml:productModel.description] ;//= [productModel.description stripHtml];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if(IS_IPHONE_5)
            return 480;
        else
            return 550;
    }
    else if (indexPath.section == 1)
    {
        if(arrDiscounts.count > 0){
            ProductDiscountModel *discount =  arrDiscounts[indexPath.row];
            NSString *strDiscount = [NSString stringWithFormat:@"%li %@ %@", (long)discount.quantity,[languageManager languageStringForKey:@"or more"],discount.price_formated];
            return [self heightForLabelText:strDiscount];
        }
        else if(arrFreeGifts.count > 0)
            return 55;
        else
            return [self heightForLabelText:productModel.description];
    }
    else if (indexPath.section == 2)
    {
        if(arrDiscounts.count > 0 && arrFreeGifts.count > 0)
            return 55;
        else
            return [self heightForLabelText:productModel.description];;
    }
    else
        return [self heightForLabelText:productModel.description];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Userdefine methods

-(void)initUI
{
    itemCount = 1;
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationController setLeftTitleWithString:@"" navigationItem:self.navigationItem];
    
    languageManager = [LanguageManager sharedManager];
    networkManager = [NetworkManager sharedManager];
    
    [self.btnCheckOut shoppingOrangeSquare:[languageManager languageStringForKey:@"Checkout"]];
    [self.btnAddToCart shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Add To Cart"]];
    
    [self.viewPopUpBase setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:self.viewPopUpBase];
    
    self.viewPopUp.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewPopUp.layer.borderWidth = 1 ;
    self.viewPopUp.layer.cornerRadius = VIEW_CORNER_RADIUS;
    self.viewPopUp.layer.masksToBounds = YES;
    
    self.imgOffer.layer.cornerRadius = VIEW_CORNER_RADIUS;
    self.imgOffer.layer.masksToBounds = YES;
    
    [self.btnAddToCart setHidden:YES];
    
    self.viewButtonBase.layer.masksToBounds = NO;
    self.viewButtonBase.layer.shadowOffset = CGSizeMake(-0.5, 0.5);
    self.viewButtonBase.layer.shadowOpacity = 0.2;
    self.viewButtonBase.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.viewButtonBase.bounds].CGPath;
    
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 100)];
    self.tableView.tableFooterView = footerView;
}
-(UIView *)sectionTitleView:(NSString *)title
{
    if([title isEqualToString:@""]){
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
        return view;
    }
    else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, tableView.frame.size.width, 30)];
        [label setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0]];
        [label setTextColor:[UIColor darkGrayColor]];
        NSString *string =title;
        
        [label setText:string];
        [view addSubview:label];
        [view setBackgroundColor:[UIColor clearColor]];
        
        return view;
    }
}
-(CGFloat)heightForLabelText:(NSString *)text{
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:[text stripHtml] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"SFUIDisplay-Medium" size:18]}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.tableView.layer.frame.size.width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    
    return ceil(rect.size.height + 30);
}
-(void)configTable
{
    arrDiscounts = productModel.discounts;
    arrFreeGifts = productModel.free_gifts_list;
    
    numberOfSectionInTableView = 1;
    if(arrFreeGifts.count >0)
        numberOfSectionInTableView += 1;
    if(arrDiscounts.count >0)
        numberOfSectionInTableView += 1;
    if(productModel.description.length > 0)
        numberOfSectionInTableView += 1;
}
-(void)toggleOfferPopUp
{
    [UIView animateWithDuration: 0.4 animations:^{
        if(self.viewPopUpBase.frame.origin.y == -100)
        {
             [self.viewPopUpBase setFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT)];
        }
        else
        {
             [self.viewPopUpBase setFrame:CGRectMake(0, -100, SCREEN_WIDTH, SCREEN_HEIGHT)];
        }
    }];
}
#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnAddToCart)
    {
        itemCount = [self.lblItemCount.text intValue];
        [[CartManager sharedManager] addItemToCart:[NSString stringWithFormat:@"%li" ,productModel.product_id] quantity:[NSString stringWithFormat:@"%i",itemCount] ];
    }
    else if(sender == self.btnClose)
    {
        [self toggleOfferPopUp];
    }
}
-(void)increaseItemCount:(id)sender
{
    itemCount = [self.lblItemCount.text intValue];
    itemCount += 1;
    [self.tableView beginUpdates];
    self.lblItemCount.text =[NSString stringWithFormat:@"%i",itemCount];
}
-(void)decreaseItemCount:(id)sender
{
    itemCount = [self.lblItemCount.text intValue];
    if(itemCount>1){
        itemCount -= 1;
        self.lblItemCount.text =[NSString stringWithFormat:@"%i",itemCount];
    }
}
#pragma mark - Network calls

-(void)loadProductDetail
{
    
    [networkManager getProductDetailsById:self.productId success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            
            NSError *error;
            self->productModel =[[ProductModel alloc] initWithDictionary:[responseData objectForKey:@"data"] error:&error];
            [self configTable];
            [self.tableView reloadData];
            [self.btnAddToCart setHidden:NO];
        }
        
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [self.btnAddToCart setHidden:YES];
    }];
}

@end

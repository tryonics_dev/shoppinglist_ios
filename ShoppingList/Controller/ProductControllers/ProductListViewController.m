//
//  ProductListViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/8/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ProductListViewController.h"

@interface ProductListViewController ()

@end

@implementation ProductListViewController
@synthesize productListModel,
collectionview,
refreshController,
selectedCategoryId,
selectedCategoryName,
bottomRefreshControl,
arrProducts,
lblResultCount,
arrFilterGroups,
btnFilters,
filterViewController,
isLatest,isSpecials,
specialProductListModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    networkManager = [NetworkManager sharedManager];
    [self initUI];
    currentPage = 1;
    arrProducts = [[NSMutableArray alloc] init];
    [self loadProductsList];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated
{
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
}
#pragma mark - CollectionView Delegate/DataSource


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return arrProducts.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ProductCollectionViewCell *cell;
    if(IS_IPHONE_5)
    {
        [collectionView registerNib:[UINib nibWithNibName:@"ProductCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ProductCollectionViewCell"];
        if(cell == nil){
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductCollectionViewCell" forIndexPath:indexPath];
        }
    }else{
        [collectionView registerNib:[UINib nibWithNibName:@"ProductLargeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ProductLargeCollectionViewCell"];
        if(cell == nil){
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductLargeCollectionViewCell" forIndexPath:indexPath];
        }
    }
    [cell.imgThumb setShowActivityIndicatorView:YES];
    
    if(isSpecials || isLatest)
    {
        FeaturedProductModel *product =  [arrProducts objectAtIndex:indexPath.row];
        [cell.imgThumb sd_setImageWithURL:[NSURL URLWithString:product.thumb]
                         placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                                  options:SDWebImageRefreshCached];
        cell.lblTitle.text = product.name;
        [cell.imgGiftIcon setHidden:!product.gift_flag];
        if(product.special > 0)
        {
            [cell.lblOldPrice setHidden:NO];
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:product.price_formated];
            [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                    value:@1
                                    range:NSMakeRange(0, [attributeString length])];
            cell.lblOldPrice.attributedText =attributeString;
            cell.lblPrice.text = product.special_formated;
        }
        else
        {
            [cell.lblOldPrice setHidden:YES];
            cell.lblPrice.text = product.price_formated;
        }
        cell.product_id = product.product_id;
    }
    else
    {
        ProductModel *product =  [arrProducts objectAtIndex:indexPath.row];
        NSURL *productImageURL = [NSURL URLWithString:product.image ];
        [cell.imgThumb sd_setImageWithURL:productImageURL
                         placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                                  options:SDWebImageRefreshCached];
        cell.lblTitle.text = product.name;
        [cell.imgGiftIcon setHidden:!product.gift_flag];
        if(product.special > 0)
        {
            [cell.lblOldPrice setHidden:NO];
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:product.price_formated];
            [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                    value:@1
                                    range:NSMakeRange(0, [attributeString length])];
            cell.lblOldPrice.attributedText =attributeString;
            cell.lblPrice.text = product.special_formated;
        }
        else
        {
            [cell.lblOldPrice setHidden:YES];
            cell.lblPrice.text = product.price_formated;
        }
        cell.product_id = product.product_id;
    }
    
    
    return cell;

    
}
-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductModel *product = self.arrProducts[indexPath.row];
    ProductDetailViewController *ctrlProductDetails = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
    ctrlProductDetails.productId = product.product_id;
    [self.navigationController pushViewController:ctrlProductDetails animated:YES];
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE_5)
        return COLLECTION_VIEW_CELL_PRODUCTS_SIZE_IPHONE5;
    
    return COLLECTION_VIEW_CELL_PRODUCTS_SIZE;
    
}
#pragma mark - Product filters delegate

-(void)didApplyFilters:(NSArray *)selectedFilters
{
    [filterViewController hide];
    arrSelectedFilters = nil;
    arrSelectedFilters = [[NSArray alloc] initWithArray:selectedFilters];
    [self pullToRefresh];
    [self.btnFilters setBadge:[NSString stringWithFormat:@"%lu",(unsigned long)arrSelectedFilters.count]];

}

#pragma mark - Userdefine methods

-(void)initUI
{
    
    [self.navigationController.navigationBar setHidden:NO];
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:self.selectedCategoryName navigationItem:self.navigationItem];
    [self.btnFilters setTitle:[languageManager languageStringForKey:@"Filters"] forState:UIControlStateNormal];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor = APP_THEME_LIGHT_GREEN;
    [refreshController addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    
    collectionview.refreshControl = refreshController;
    
    bottomRefreshControl = [[UIRefreshControl alloc] init];
    bottomRefreshControl.tintColor = APP_THEME_LIGHT_GREEN;
    [bottomRefreshControl addTarget:self action:@selector(loadMoreProducts) forControlEvents:UIControlEventValueChanged];
    
    collectionview.bottomRefreshControl = bottomRefreshControl;
    
    emptyView = [[EmptyListViewController alloc] initWithNibName:@"EmptyListViewController" bundle:nil];
    emptyView.view.center = CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
    emptyView.lblMessage.text = [languageManager languageStringForKey:@"We cannot find any product list."];
    [emptyView.btnRetry addTarget:self action:@selector(loadProductsList) forControlEvents:UIControlEventTouchUpInside];
    if(self.arrFilterGroups.count>0){
        [self.btnFilters setHidden:NO];
        filterViewController = [[ProductFiltersViewController alloc] initWithNibName:@"ProductFiltersViewController" bundle:nil];
        filterViewController.arrFilterGroups = self.arrFilterGroups;
        filterViewController.delegate =self;
        [filterViewController setBaseViwe:self.navigationController.view];
      //  filterViewController.navController = self.navigationController;
    }
    else
    {
        [self.btnFilters setHidden:YES];
    }
}
-(void)handleEmptyView:(BOOL)isShow
{
    if(isShow)
    {
        [self.collectionview setHidden:YES];
        [self.view addSubview:emptyView.view];
    }
    else
    {
        [self.collectionview setHidden:NO];
        [emptyView.view removeFromSuperview];
    }
}
-(void)pullToRefresh
{
    currentPage = 1;
    [self.arrProducts removeAllObjects];
    [self.collectionview reloadData];
    [self loadProductsList];
}
-(void)loadMoreProducts
{
    if(isSpecials || isLatest){
        [bottomRefreshControl endRefreshing];
        return;
    }
    
    currentPage ++;
    [self loadProductsList];
}
#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnFilters)
    {
        if([filterViewController isShowing])
            [filterViewController hide];
        else{
            
             [filterViewController showWithSelectedFilter:arrSelectedFilters];
        }
    }
}

#pragma mark - Network calls

-(void)loadProductsList
{
    if(isSpecials)
    {
        [networkManager getSpecialsProductList:^(id  _Nonnull responseData) {
            BOOL success = [[responseData objectForKey:@"success"] boolValue];
            if(success){
                NSError *error;
                self.specialProductListModel =[[SpecialProductListModel alloc] initWithDictionary:responseData error:&error];
                [self.arrProducts addObjectsFromArray:self.specialProductListModel.data];
                
                [self.collectionview reloadData];
                [self handleEmptyView:NO];
                if(self.arrProducts.count ==0)
                {
                    [self handleEmptyView:YES];
                }
                self->lblResultCount.text =[NSString stringWithFormat:@"%lu %@", (unsigned long)self ->arrProducts.count,[self->languageManager languageStringForKey:@"Results"]];
            }
            else
            {
                [self handleEmptyView:YES];
            }
            [self->refreshController endRefreshing];
            [self->bottomRefreshControl endRefreshing];
        } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
            [self handleEmptyView:YES];
            [self->refreshController endRefreshing];
            [self->bottomRefreshControl endRefreshing];
        }];
    }
    else if(isLatest)
    {
        [networkManager getLatestProductList:^(id  _Nonnull responseData) {
            BOOL success = [[responseData objectForKey:@"success"] boolValue];
            if(success){
                NSError *error;
                self.specialProductListModel =[[SpecialProductListModel alloc] initWithDictionary:responseData error:&error];
                [self.arrProducts addObjectsFromArray:self.specialProductListModel.data];
                
                [self.collectionview reloadData];
                [self handleEmptyView:NO];
                if(self.arrProducts.count ==0)
                {
                    [self handleEmptyView:YES];
                }
                self->lblResultCount.text =[NSString stringWithFormat:@"%lu %@", (unsigned long)self ->arrProducts.count,[self->languageManager languageStringForKey:@"Results"]];
            }
            else
            {
                [self handleEmptyView:YES];
            }
            [self->refreshController endRefreshing];
            [self->bottomRefreshControl endRefreshing];
        } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
            [self handleEmptyView:YES];
            [self->refreshController endRefreshing];
            [self->bottomRefreshControl endRefreshing];
        }];
    }
    else
    {
        NSMutableString *strFilters = [[NSMutableString alloc] initWithString:@""];
        for (id filterId in arrSelectedFilters) {
            [strFilters appendFormat:@"%@,",filterId];
        }
        [networkManager getProductListByCategoryID:selectedCategoryId page:currentPage filters:strFilters  success:^(id  _Nonnull responseData)
        {
            BOOL success = [[responseData objectForKey:@"success"] boolValue];
            if(success){
                NSError *error;
                self.productListModel =[[ProductListModel alloc] initWithDictionary:responseData error:&error];
                [self.arrProducts addObjectsFromArray:self.productListModel.data];
                
                [self.collectionview reloadData];
                [self handleEmptyView:NO];
                if(self.arrProducts.count ==0)
                {
                    [self handleEmptyView:YES];
                }
                self->lblResultCount.text =[NSString stringWithFormat:@"%lu %@", (unsigned long)self ->arrProducts.count,[self->languageManager languageStringForKey:@"Results"]];
            }
            else
            {
                [self handleEmptyView:YES];
            }
            [self->refreshController endRefreshing];
            [self->bottomRefreshControl endRefreshing];
            
        } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
            [self handleEmptyView:YES];
            [self->refreshController endRefreshing];
            [self->bottomRefreshControl endRefreshing];
        }];
    }
}

@end

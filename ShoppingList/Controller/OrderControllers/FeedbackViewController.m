//
//  FeedbackViewController.m
//  Tonic
//
//  Created by Mohamed Roshan on 11/16/17.
//  Copyright © 2017 Mohamed Roshan. All rights reserved.
//

#import "FeedbackViewController.h"

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController
@synthesize viewContainer,isReturn;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initStyle];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationItem.hidesBackButton=YES;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
   
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self removeFromParentViewController];
    [self.view removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
-(void)initStyle
{
    languageManager = [LanguageManager sharedManager];
    self.viewContainer.layer.borderColor=[UIColor clearColor].CGColor;
    self.viewContainer.layer.shadowOffset = CGSizeMake(0.2, 0.2);
    self.viewContainer.layer.shadowColor = [UIColor blackColor].CGColor;
    self.viewContainer.layer.cornerRadius=4;
    self.viewContainer.layer.shadowRadius = 4;
    self.viewContainer.layer.shadowOpacity = 0.2;
    self.viewContainer.clipsToBounds = false;
    self.viewContainer.layer.masksToBounds = false;
    
    self.txtFeedback.layer.borderColor=[UIColor groupTableViewBackgroundColor].CGColor;
    self.txtFeedback.layer.borderWidth=1;
    self.txtFeedback.layer.cornerRadius=4;
   
    self.lblTitle.text=[languageManager languageStringForKey:@"Feedback"];
    self.lblTextTitle.text=[languageManager languageStringForKey:@"Enter your feedback below"];
    
    if(self.isReturn)
         [self.btnSubmit shoppingClearRoundedCorner:[languageManager languageStringForKey:@"Return"]];
    else
        [self.btnSubmit shoppingClearRoundedCorner:[languageManager languageStringForKey:@"Cancel Order"]];
    
    
}

-(void)dismissValidationAlertView:(UIAlertController *)alertView{
    [alertView dismissViewControllerAnimated:NO completion:nil];
}
#pragma mark - Button Click

-(IBAction)buttonClick:(id)sender
{
    if(sender==self.btnClose){
        [self removeFromParentViewController];
        [self.view removeFromSuperview];
    }
    else if(sender==self.btnSubmit)
    {
        if(_txtFeedback.text.length>0){
            [self cancelOrder];
        }
        else
        {
            [Utility showMessageAlertView:self.navigationController message:[languageManager languageStringForKey:@"Your feedback is highly appreciated and will help us to improve our ability to serve you"]];
        }
    }
}
-(void)dismissAutoHideAlert:(UIAlertController *)alertView{
    [alertView dismissViewControllerAnimated:NO completion:nil];
     [self.delegate didOrderFeedbackSuccess:YES];
}
#pragma mark - Network calls

-(void)cancelOrder
{
    int orderStatusId = ORDER_CANCEL_STATUS;
    if(isReturn)
        orderStatusId = ORDER_RETURN_STATUS;
    
    [[NetworkManager sharedManager] updateOrderStatusById:self.orderId comment:self.txtFeedback.text status:orderStatusId success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
           
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *strMessage = [NSString stringWithFormat:@"%@ %li %@",[self->languageManager languageStringForKey:@"Your Order"],(long)self.orderId,(self->isReturn ? [self->languageManager languageStringForKey:@"has been returned"] : [self->languageManager languageStringForKey:@"has been canceled"] )];
                
                UIAlertController *alert = [UIAlertController
                                            alertControllerWithTitle:@""
                                            message:strMessage
                                            preferredStyle:UIAlertControllerStyleAlert];
                [self.navigationController presentViewController:alert animated:YES completion:nil];
                [self performSelector:@selector(dismissAutoHideAlert:) withObject:alert afterDelay:4];
                [self.btnClose sendActionsForControlEvents:UIControlEventTouchUpInside ];
                
                
                
            });
           
        }
        else
        {
            [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
        
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [Utility showErrorAlertView:self.navigationController];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

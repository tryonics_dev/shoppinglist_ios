//
//  OrderTrackingViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/6/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "OrderTrackingViewController.h"
#import "ShoppingList-Swift.h"
@import RMQClient;

@interface OrderTrackingViewController () <ARCarMovementDelegate>
{
     RMQConnection *conn;
}
@property (strong, nonatomic) ARCarMovement *moveMent;

@end

@implementation OrderTrackingViewController
@synthesize orderId,riderOldCoordinate;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:ORDER_STATUS_UPDATED_NOTIFICATION
                                               object:nil];
    [self initUI];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getOrderById];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [conn close];
    conn = nil;
}
- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [conn close];
    conn = nil;
}

#pragma mark - Local notification receiver

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:ORDER_STATUS_UPDATED_NOTIFICATION])
    {
        [self getOrderById];
    }
}

#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:@"" navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    self.btnClose.layer.cornerRadius = 8;
    self.btnClose.layer.masksToBounds = YES;
    
    NSString *path = [[NSBundle mainBundle] pathForResource: @"APIConfig" ofType: @"plist"];
    NSDictionary *dictAPIConfig = [NSDictionary dictionaryWithContentsOfFile: path];
    SOCKET_URL = [dictAPIConfig objectForKey: @"SOCKET_BASE_URL"];
    
    
}
-(void)updateUI
{
  
    self.lblStatus.text = orderItemModel.status;
    
    
    self.lblOrderId.text =[NSString stringWithFormat:@"%@",orderItemModel.order_id] ;
    self.lblPayment.text = orderItemModel.payment_method;
    self.txtAddress.text = [ NSString stringWithFormat:@"%@\n%@",orderCustomerDetails.customer_name,orderCustomerDetails.customer_address];
    
    self.lblDeliveryTo.text = [languageManager languageStringForKey:@"Delivery Address"];
   
    if(riderModel != nil)
    {
        self.lblRiderName.text = riderModel.name;
        self.lblVehicleNumber.text = riderModel.vehicle_number;
        [self.viewRider setHidden:NO];
    }
    else
    {
        [self.viewRider setHidden:YES];
    }
    
}
-(void)statusProgressAnimation
{
    if(orderItemModel.status_id == ORDER_PENDING_STATUS)
    {
        self.viewPending.backgroundColor = [UIColor lightGrayColor];
        self.viewPrepare.backgroundColor = [UIColor lightGrayColor];
        self.viewReadyToDeliver.backgroundColor = [UIColor lightGrayColor];
        self.viewDispatch.backgroundColor = [UIColor lightGrayColor];
        self.viewRoute.backgroundColor = [UIColor lightGrayColor];
        
        [self addAnimationsToProgressView:self.viewPending];
    }
    else if(orderItemModel.status_id == ORDER_PREPARE_STATUS)
    {
        self.viewPending.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewPrepare.backgroundColor = [UIColor lightGrayColor];
        self.viewReadyToDeliver.backgroundColor = [UIColor lightGrayColor];
        self.viewDispatch.backgroundColor = [UIColor lightGrayColor];
        self.viewRoute.backgroundColor = [UIColor lightGrayColor];
        
        [self addAnimationsToProgressView:self.viewPrepare];
    }
    else if(orderItemModel.status_id == ORDER_READY_DELIVER_STATUS)
    {
        self.viewPending.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewPrepare.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewReadyToDeliver.backgroundColor = [UIColor lightGrayColor];
        self.viewDispatch.backgroundColor = [UIColor lightGrayColor];
        self.viewRoute.backgroundColor = [UIColor lightGrayColor];
        
        [self addAnimationsToProgressView:self.viewReadyToDeliver];
    }
    else if(orderItemModel.status_id == ORDER_READY_DELIVER_STATUS)
    {
        self.viewPending.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewPrepare.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewReadyToDeliver.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewDispatch.backgroundColor = [UIColor lightGrayColor];
        self.viewRoute.backgroundColor = [UIColor lightGrayColor];
        
        [self addAnimationsToProgressView:self.viewDispatch];
    }
    else if(orderItemModel.status_id == ORDER_DISPATCH_STATUS)
    {
        self.viewPending.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewPrepare.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewReadyToDeliver.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewDispatch.backgroundColor = [UIColor lightGrayColor];
        self.viewRoute.backgroundColor = [UIColor lightGrayColor];
        
        [self addAnimationsToProgressView:self.viewDispatch];
    }
    else if(orderItemModel.status_id == ORDER_ON_ROUTE_STATUS)
    {
        self.viewPending.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewPrepare.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewReadyToDeliver.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewDispatch.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewRoute.backgroundColor = [UIColor lightGrayColor];
        
        [self addAnimationsToProgressView:self.viewRoute];
    }
    else
    {
        self.viewPending.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewPrepare.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewReadyToDeliver.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewDispatch.backgroundColor = APP_THEME_LIGHT_GREEN;
        self.viewRoute.backgroundColor = APP_THEME_LIGHT_GREEN;
    }
}
-(void)addAnimationsToProgressView:(UIView *)parentView
{
    [self.viewAnimation removeFromSuperview];
    [parentView addSubview:self.viewAnimation];
    [self animateStatus:parentView];
}
-(void)animateStatus:(UIView *)parentView
{
    [self.viewAnimation setFrame:CGRectMake(0, 0, 0, 5)];
    [UIView animateWithDuration:0.8f animations:^{
        self.viewAnimation.frame = CGRectMake(parentView.frame.size.width/2, 0, parentView.frame.size.width/2, 5);
    }completion:^(BOOL finished) {
        if(finished)
        {
            [UIView animateWithDuration:0.5f animations:^{
                self.viewAnimation.frame = CGRectMake(parentView.frame.size.width, 0,0, 5);
            }completion:^(BOOL finished) {
                if(finished)
                {
                      [self animateStatus:parentView];
                }
            }];
        }
    }];
    
   
}
#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnClose)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (sender == self.btnCall)
    {
        NSString *phoneNumber = [@"tel://" stringByAppendingString:riderModel.driver_mobile];
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:phoneNumber];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
            }
        }];
    }
}
-(void)loadGoogleMap
{
    self.moveMent = [[ARCarMovement alloc]init];
    self.moveMent.delegate = self;
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"coordinates" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
    self.CoordinateArr = [[NSMutableArray alloc]initWithArray:[NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil]];
    
   
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[orderCustomerDetails.customer_lat doubleValue]
                                                            longitude:[orderCustomerDetails.customer_long doubleValue]
                                                                 zoom:16];
    [self.mapView setCamera:camera];
    self.mapView.myLocationEnabled = NO;
    self.mapView.delegate = self;
    
     self.homeCoordinate = CLLocationCoordinate2DMake([orderCustomerDetails.customer_lat doubleValue],[orderCustomerDetails.customer_long doubleValue]);
    homeMarker = [[GMSMarker alloc] init];
    homeMarker.position =  self.homeCoordinate;
    homeMarker.icon = [UIImage imageNamed:@"customerHomeIcon"];
    homeMarker.map = self.mapView;
    
   
    driverMarker = [[GMSMarker alloc] init];
    driverMarker.icon = [UIImage imageNamed:@"riderIcon"];
    driverMarker.map = self.mapView;
    if(orderItemModel.order_status_id  == ORDER_ON_ROUTE_STATUS){
        [conn close];
        conn = nil;
        [self connectRabitMQ];
    }

    
}

#pragma mark - scheduledTimerWithTimeInterval Action
-(void)timerTriggered {

    if (self.counter < self.CoordinateArr.count) {

        self->driverMarker.position = self.riderOldCoordinate;
        CLLocationCoordinate2D newCoordinate = CLLocationCoordinate2DMake([self.CoordinateArr[self.counter][@"lat"] floatValue],[self.CoordinateArr[self.counter][@"long"] floatValue]);


        [self.moveMent arCarMovementWithMarker:driverMarker oldCoordinate:self.riderOldCoordinate newCoordinate:newCoordinate mapView:self.mapView bearing:0];  //instead value 0, pass latest bearing value from backend

        self.riderOldCoordinate = newCoordinate;
        self.counter = self.counter + 1; //increase the value to get all index position from array

    }

}

#pragma mark - ARCarMovementDelegate
- (void)arCarMovementMoved:(GMSMarker * _Nonnull)Marker {
   
    driverMarker = Marker;
    driverMarker.map = self.mapView;
    //animation to make car icon in center of the mapview
    //
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:driverMarker.position zoom:16.0f];
    [self.mapView animateWithCameraUpdate:updatedCamera];
    
}

#pragma mark  - RabbitMQ

- (void)connectRabitMQ {
    if(conn == nil || conn.isClosed){
        conn = [[RMQConnection alloc] initWithUri:SOCKET_URL
                                         delegate:[RMQConnectionDelegateLogger new]];
        [conn start];
        
        id<RMQChannel> ch = [conn createChannel];
        NSString *strQue = [NSString stringWithFormat:@"q_o_%i",(int)self.orderId];
        RMQQueue *q = [ch queue:strQue];
       
        [q subscribe:^(RMQMessage * _Nonnull message) {
            NSString *strMessage =  [[NSString alloc] initWithData:message.body encoding:NSUTF8StringEncoding];
            NSArray *arrCoordinates = [strMessage componentsSeparatedByString:@"#"];
            if(arrCoordinates.count>2)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(!CLLocationCoordinate2DIsValid(self->riderOldCoordinate))
                    {
                        self.riderOldCoordinate = CLLocationCoordinate2DMake([arrCoordinates[1] floatValue],[arrCoordinates[2] floatValue]);
                        self->driverMarker.position = self.riderOldCoordinate;
                    }
                    CLLocationCoordinate2D newRiderCoordinate = CLLocationCoordinate2DMake([arrCoordinates[1] floatValue],[arrCoordinates[2] floatValue]);
                    [self.moveMent arCarMovementWithMarker:self->driverMarker oldCoordinate:self.riderOldCoordinate newCoordinate:newRiderCoordinate mapView:self.mapView bearing:0];
                    self.riderOldCoordinate = newRiderCoordinate;
                   
                });
                
            }
            
            
        }];
    }
}

#pragma mark - Network calls

-(void)getOrderById
{
    [[NetworkManager sharedManager] getCustomerOrderById:self.orderId  success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            NSError *error;
            self->orderItemModel = [[OrderItemModel alloc] initWithDictionary:[[responseData objectForKey:@"data"] objectForKey:@"order_data"] error:&error];
            self->orderCustomerDetails = [[OrderCustomerDetails alloc] initWithDictionary:[[responseData objectForKey:@"data"] objectForKey:@"customer_data"] error:&error];
            if([[[responseData objectForKey:@"data"] allKeys]containsObject:@"driver_data"])
            {
                self->riderModel = [[RiderModel alloc] initWithDictionary:[[responseData objectForKey:@"data"] objectForKey:@"driver_data"] error:&error];
            }
            [self updateUI];
            [self loadGoogleMap];
            [self statusProgressAnimation];
            
        }
        else
        {
            [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
      
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [Utility showErrorAlertView:self.navigationController];
    }];
}


@end

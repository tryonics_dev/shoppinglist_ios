//
//  OrderTrackingViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/6/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "OrderItemModel.h"
#import "OrderCustomerDetails.h"
#import "RiderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderTrackingViewController : UIViewController<GMSMapViewDelegate>
{
    LanguageManager *languageManager;
    GMSMarker *driverMarker;
    GMSMarker *homeMarker;
    
    NSString *SOCKET_URL;
   
    OrderItemModel *orderItemModel;
    OrderCustomerDetails *orderCustomerDetails;
    RiderModel *riderModel;
   
}
@property (nonatomic)NSInteger orderId;
@property (strong, nonatomic) NSMutableArray *CoordinateArr;
@property (strong, nonatomic)IBOutlet GMSMapView *mapView;
@property CLLocationCoordinate2D riderOldCoordinate;
@property CLLocationCoordinate2D homeCoordinate;
@property GMSCoordinateBounds *mapBounds;
@property (weak, nonatomic) NSTimer *timer;
@property NSInteger counter;

@property(nonatomic,weak)IBOutlet UIButton *btnClose,*btnCall;
@property(nonatomic,weak)IBOutlet UILabel *lblStatus,
*lblOrderId,
*lblPayment,
*lblDeliveryTo,
*lblRiderName,
*lblVehicleNumber;
@property(nonatomic,weak)IBOutlet UITextView *txtAddress;
@property(nonatomic ,weak)IBOutlet UIView *viewPending,
*viewPrepare,
*viewReadyToDeliver,
*viewDispatch,
*viewRoute,
*viewAnimation,
*viewRider;
//@property(nonatomic,weak)IBOutlet UIView *baseMapView;
@end

NS_ASSUME_NONNULL_END

//
//  FeedbackViewController.h
//  Tonic
//
//  Created by Mohamed Roshan on 11/16/17.
//  Copyright © 2017 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FeedbackViewDelegate <NSObject>
@required
-(void)didOrderFeedbackSuccess:(BOOL)isSuccess;
@optional

@end

@interface FeedbackViewController : UIViewController<UITextViewDelegate>
{
    LanguageManager *languageManager;
}
@property(nonatomic,strong)id<FeedbackViewDelegate> delegate;
@property(nonatomic)NSInteger orderId;

@property(nonatomic,weak)IBOutlet UIButton *btnClose,*btnSubmit;
@property(nonatomic,weak)IBOutlet UILabel *lblTitle,*lblTextTitle;
@property(nonatomic,weak)IBOutlet UITextView *txtFeedback;
@property(nonatomic,weak)IBOutlet UIView *viewContainer;
@property(nonatomic)BOOL isReturn;
@end

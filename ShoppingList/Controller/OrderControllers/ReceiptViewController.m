//
//  ReceiptViewController.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ReceiptViewController.h"

@interface ReceiptViewController ()

@end

@implementation ReceiptViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:ORDER_STATUS_UPDATED_NOTIFICATION
                                               object:nil];
    
    [self initUI];
   
    self.lblOrerId.text = [NSString stringWithFormat:@"%li",(long)self.orderId];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
     //[self getOrderById];
    [self getServerDate];
}
- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Local notification receiver

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:ORDER_STATUS_UPDATED_NOTIFICATION])
    {
        //[self getOrderById];
        [self getServerDate];
    }
}

#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager = [LanguageManager sharedManager];
    [self.navigationController setLeftTitleWithString:[languageManager languageStringForKey:@"Receipt"] navigationItem:self.navigationItem];
    [self.navigationController initShoppingListStyle];
    [myAppDelegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    
    [self.btnCancel  shoppingTransparentDisableRoundedCorner:[languageManager languageStringForKey:@"Cancel Order"]];
    [self.btnReorder shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Reorder"]];
    
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
    self.tableView.tableFooterView = footerView;
    
    self.viewButtonHolder.layer.masksToBounds = NO;
    self.viewButtonHolder.layer.shadowOffset = CGSizeMake(-0.5, 0.5);
    self.viewButtonHolder.layer.shadowOpacity = 0.2;
    self.viewButtonHolder.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.viewButtonHolder.bounds].CGPath;
    
    [self.navigationItem.titleView addSubview:self.lblTotal];
    [self.lblTotal setFrame: CGRectMake(SCREEN_WIDTH-250, 10, 200, 21)];
}
-(void)handleCancelButton
{
    if(orderDetailModel.order_status_id == ORDER_PENDING_STATUS ||
       orderDetailModel.order_status_id == ORDER_PREPARE_STATUS ||
       orderDetailModel.order_status_id == ORDER_DISPATCH_STATUS ||
       orderDetailModel.order_status_id == ORDER_ON_ROUTE_STATUS ||
       orderDetailModel.order_status_id == ORDER_READY_DELIVER_STATUS)
    {
        [self.btnCancel shoppingClearRoundedCorner:[languageManager languageStringForKey:@"Cancel"]];
    }
    else
    {
        if(orderDetailModel.order_status_id == ORDER_DELIVERED_STATUS)
        {
            isReturn = [self isReturnPeriod];
            if(isReturn)
            {
                [self.btnCancel shoppingClearRoundedCorner:[languageManager languageStringForKey:@"Return"]];
            }
            else
            {
                [self.btnCancel shoppingTransparentDisableRoundedCorner:[languageManager languageStringForKey:@"Cancel"]];
            }
        }else{
            [self.btnCancel shoppingTransparentDisableRoundedCorner:[languageManager languageStringForKey:@"Cancel"]];
        }
    }
}
-(void)reOrderConfirmation
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:[languageManager languageStringForKey:@"Do you want to reorder these item(s)?"]
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[languageManager languageStringForKey:@"Yes"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [self productReorder];
                               }];
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:[languageManager languageStringForKey:@"No"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}
-(BOOL)isReturnPeriod
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *serverDate = [dateFormatter dateFromString:serverDateString];
    NSDate *orderDate=[[dateFormatter dateFromString:orderDetailModel.date_added] dateByAddingTimeInterval:60*60*24];
    NSComparisonResult result = [serverDate compare:orderDate];
    if(result == NSOrderedAscending)
    {
        return true;
    }
    return false;
}
-(void)dismissAutoHideAlert:(UIAlertController *)alertView{
    [alertView dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    self.tabBarController.selectedIndex = 2;
}
#pragma mark - UITableView Delegate/Datasource

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        cell.separatorInset = UIEdgeInsetsZero;
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return orderDetailModel.products.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        ReceiptDetailTableViewCell *cell = (ReceiptDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ReceiptDetailTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        cell.lblOrderId.text = [NSString stringWithFormat:@"%@ : %ld ",[languageManager languageStringForKey:@"Order ID"],(long)orderDetailModel.order_id];
        cell.lblOrderDate.text = [NSString stringWithFormat:@"%@ : %@",[languageManager languageStringForKey:@"Date Added"],[Utility formatDateToOrderList:orderDetailModel.date_added]];
    
        cell.txtPaymentMethod.text = [NSString stringWithFormat:@"%@ : %@",[languageManager languageStringForKey:@"Payment Method"],orderDetailModel.payment_method];

        [cell.lblBillingAddress setText:[orderDetailModel.payment_address stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"]];
        [cell.lblShippingAddress setText:[orderDetailModel.shipping_address stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"]];
        
        
        OrderCostModel *subTotal = self->orderDetailModel.totals[0];
        cell.lblSubTotal.text = [NSString stringWithFormat:@"Rs. %@", subTotal.value];
        cell.lblSubTotalText.text = subTotal.title;
        
        OrderCostModel *shippingCost = self->orderDetailModel.totals[1];
        cell.lblShippingCost.text = [NSString stringWithFormat:@"Rs. %@", shippingCost.value];
        cell.lblShippingCostText.text = shippingCost.title;
        
        OrderCostModel *totalCost = self->orderDetailModel.totals[2];
        cell.lblTotal.text = [NSString stringWithFormat:@"Rs. %@", totalCost.value];
        cell.lblTotalText.text = totalCost.title;
        
//        cell.lblSubTotal.text = [NSString stringWithFormat:@"%@ : %ld ",[languageManager languageStringForKey:@"Order ID"],(long)orderDetailModel.order_id];
    
        return cell;
    }
    else
    {
        ReciptDetailProductsTableViewCell *cell = (ReciptDetailProductsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ReciptDetailProductsTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        OrderDetailProductModel *product = orderDetailModel.products[indexPath.row-1];
        cell.lblProduct.text = product.name;
        cell.lblUnitPrice.text = [NSString stringWithFormat:@"%ld x %@",(long)product.quantity, product.price];
        cell.lblTotal.text = product.total;
        return cell;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
        return 595;
    else
        return 65;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

#pragma mark - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnCancel)
    {
        HandleFeedback *feedback=[[HandleFeedback alloc] initWithView:self.navigationController delegate:self];
        [feedback showFeedBackController:self.orderId isReturn:isReturn];
    }
    else if (sender == self.btnReorder)
    {
        [self reOrderConfirmation];
    }
}
#pragma mark - FeedbackView delegate

-(void)didOrderFeedbackSuccess:(BOOL)isSuccess
{
    if(isSuccess)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
#pragma mark - Network calls

-(void)getServerDate
{
    [[NetworkManager sharedManager] getServerDate:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success)
        {
            self->serverDateString = [responseData objectForKey:@"data"];
            [self getOrderById];
        }
        else
        {
             [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
    } ];
}

-(void)getOrderById
{
    [[NetworkManager sharedManager] getOrderById:self.orderId  success:^(id  _Nonnull responseData) {
        BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            NSError *error;
            self->orderDetailModel = [[OrderDetailModel alloc] initWithDictionary:[responseData objectForKey:@"data"] error:&error];
            self.lblOrerId.text = self->orderDetailModel.store_name;
            self.lblTotal.text = [NSString stringWithFormat:@"Rs. %.2f",self->orderDetailModel.total];
        
            [self handleCancelButton];
            
        }
        else
        {
            [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
        [self.tableView reloadData];
       
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [Utility showErrorAlertView:self.navigationController];
    }];
}
-(void)productReorder
{
    NSMutableArray *arrPost = [[NSMutableArray alloc] init];
    for (OrderDetailProductModel *product in orderDetailModel.products) {
        NSDictionary *dicProduct = [NSDictionary dictionaryWithObjectsAndKeys:product.product_id,@"product_id",[NSString stringWithFormat:@"%li",product.quantity],@"quantity", nil];
        [arrPost addObject:dicProduct];
    }
    [[NetworkManager sharedManager] productReorder:arrPost success:^(id  _Nonnull responseData) {
         BOOL success = [[responseData objectForKey:@"success"] boolValue];
        if(success){
            //[Utility showMessageAlertView:self.navigationController message:[self->languageManager languageStringForKey:@"Your cart has been updated."]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController *alert = [UIAlertController
                                            alertControllerWithTitle:@""
                                            message:[self->languageManager languageStringForKey:@"Your cart has been updated."]
                                            preferredStyle:UIAlertControllerStyleAlert];
                [self.navigationController presentViewController:alert animated:YES completion:nil];
                [self performSelector:@selector(dismissAutoHideAlert:) withObject:alert afterDelay:3];
                
            });
        }
        else
        {
            [Utility showErrorMessageAlertView:self.navigationController errorResponce:responseData];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        [Utility showErrorAlertView:self.navigationController];
    }];
}
@end

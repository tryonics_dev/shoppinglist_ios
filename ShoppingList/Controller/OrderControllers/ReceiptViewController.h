//
//  ReceiptViewController.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailModel.h"
#import "ReceiptDetailTableViewCell.h"
#import "KXHtmlLabel.h"
#import "ReciptDetailProductsTableViewCell.h"
#import "HandleFeedback.h"
NS_ASSUME_NONNULL_BEGIN

@interface ReceiptViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,FeedbackViewDelegate>
{
    LanguageManager *languageManager;
    OrderDetailModel *orderDetailModel;
    BOOL isReturn;
    NSString *serverDateString;
}
@property (nonatomic)NSInteger orderId;

@property(nonatomic,weak)IBOutlet UILabel *lblSubTotal;
@property(nonatomic,weak)IBOutlet UILabel *lblOrerId,*lblTotal;

@property(nonatomic,weak)IBOutlet UIButton *btnCancel,*btnReorder;
@property(nonatomic,weak)IBOutlet UIView *viewButtonHolder;
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@end

NS_ASSUME_NONNULL_END

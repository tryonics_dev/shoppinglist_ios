//
//  Utility.h
//  Tonic
//
//  Created by Mohamed Roshan on 2/27/17.
//  Copyright © 2017 Mohamed Roshan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject
{
    
}

+(NSString *)formatDate:(NSString*)serverDate;
+(NSString *)formatDateToOrderList:(NSString*)serverDate;
+ (BOOL) validateSLMobile: (NSString *) candidate ;
+ (BOOL) validateEmail: (NSString *) candidate;
+ (BOOL) validatePassword: (NSString *) candidate;
+(BOOL)validateName:(NSString *) candidate;
+(BOOL)validateAddress:(NSString *) candidate;
+(NSString *)validateURLString:(NSString *)url;

+(void)showErrorAlertView:(UINavigationController *)naveController;
+(void)showMessageAlertView:(UINavigationController *)naveController message:(NSString *)message;
+(void)showErrorMessageAlertView:(UINavigationController *)naveController errorResponce:(NSDictionary *)responce;
@end

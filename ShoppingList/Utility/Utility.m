//
//  Utility.m
//  Tonic
//
//  Created by Mohamed Roshan on 2/27/17.
//  Copyright © 2017 Mohamed Roshan. All rights reserved.
//

#import "Utility.h"
#import <QuartzCore/QuartzCore.h>

@implementation Utility

-(instancetype)init
{
    self=[super init];
    if(self)
    {
        
    }
    return self;
}
+(NSString *)formatDate:(NSString*)serverDate
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *date=[dateFormatter dateFromString:serverDate];
    dateFormatter.dateFormat = @"MMM dd, yyyy";
    NSString *strDate=[dateFormatter stringFromDate:date];
    return strDate;
}
+(NSString *)formatDateToOrderList:(NSString*)serverDate
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *date=[dateFormatter dateFromString:serverDate];
    dateFormatter.dateFormat = @"dd MMMM yyyy, EEEE";
    NSString *strDate=[dateFormatter stringFromDate:date];
    return strDate;
}
+ (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}
+(BOOL)validateName:(NSString *) candidate
{
    NSString *nameRegex = @"[a-zA-Z]+";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    
    return [nameTest evaluateWithObject:candidate];
}
+(BOOL)validateAddress:(NSString *) candidate
{
    NSString *addressRegex = @"[A-Za-z0-9'\\/\\-\\s]+";
    NSPredicate *addressTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", addressRegex];
    
    return [addressTest evaluateWithObject:candidate];
}
+ (BOOL) validateSLMobile: (NSString *) candidate {
    NSString *mobile = @"^[7][1-9]{1}[0-9]{7}$";
    NSPredicate *mobileTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobile];
    
    return [mobileTest evaluateWithObject:candidate];
}
+ (BOOL) validatePassword: (NSString *) candidate {
  //  NSString *password = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$^+=!*()@%&]).{6,}$";
    NSString *password = @"^(?=.*[a-z | 0-9])(?=.*[#$^+=!*()@%&]).{6,}$";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", password];
    
    return [passwordTest evaluateWithObject:candidate];
}

+(void)showErrorAlertView:(UINavigationController *)naveController
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:[[LanguageManager sharedManager] languageStringForKey:@"Sorry! Your request couldn't be processed. Please try again later."]
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:[[LanguageManager sharedManager] languageStringForKey:@"Ok"]
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [naveController dismissViewControllerAnimated:YES completion:nil];
                               }];
    [alert addAction:okButton];
    [naveController presentViewController:alert animated:YES completion:nil];
}
+(void)showMessageAlertView:(UINavigationController *)naveController message:(NSString *)message
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    [naveController presentViewController:alert animated:YES completion:nil];
    [self performSelector:@selector(dismissAutoHideAlert:) withObject:alert afterDelay:3];
    
}

+(void)showErrorMessageAlertView:(UINavigationController *)naveController errorResponce:(NSDictionary *)responce
{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:[responce objectForKey:@"error"][0]
                                preferredStyle:UIAlertControllerStyleAlert];
    [naveController presentViewController:alert animated:YES completion:nil];
    [self performSelector:@selector(dismissAutoHideAlert:) withObject:alert afterDelay:3];
    
}

+(void)dismissAutoHideAlert:(UIAlertController *)alertView{
    [alertView dismissViewControllerAnimated:NO completion:nil];
}

+(NSString *)validateURLString:(NSString *)url
{
    if([url rangeOfString:@" "].location == NSNotFound)
        return url;
    else
    {
      return  [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
}
@end

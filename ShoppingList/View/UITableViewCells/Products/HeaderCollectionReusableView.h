//
//  HeaderCollectionReusableView.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/21/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HeaderCollectionReusableView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@end

NS_ASSUME_NONNULL_END

//
//  ProductFilterCollectionViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/21/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ProductFilterCollectionViewCell.h"

@implementation ProductFilterCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.viewBase.layer.cornerRadius = 18;
    self.viewBase.layer.masksToBounds = YES;
}

@end

//
//  ProductCollectionViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ProductCollectionViewCell.h"

@implementation ProductCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.layer.cornerRadius = VIEW_CORNER_RADIUS ;
    self.contentView.layer.borderWidth = 1.0f;
    self.contentView.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.contentView.layer.masksToBounds = YES;
    
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    self.layer.shadowRadius = VIEW_CORNER_RADIUS;
    self.layer.shadowOpacity = VIEW_SHADOW_OPACITY;
    self.layer.masksToBounds = NO;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.contentView.layer.cornerRadius].CGPath;
    
    self.imgThumb.layer.cornerRadius = VIEW_CORNER_RADIUS;
    self.layer.masksToBounds = NO;
}

-(IBAction)buttonClick:(id)sender
{
    [[CartManager sharedManager] addItemToCart:[NSString stringWithFormat:@"%li" ,self.product_id] quantity:@"1" ];
}
@end

//
//  ProductFilterCollectionViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/21/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductFilterCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic)IBOutlet UIButton *btnFilter;
@property (weak, nonatomic)IBOutlet UIImageView *imgFilter;
@property (weak, nonatomic)IBOutlet UILabel *lblFilterTitle;
@property (weak, nonatomic)IBOutlet UIView *viewBase;
@end

NS_ASSUME_NONNULL_END

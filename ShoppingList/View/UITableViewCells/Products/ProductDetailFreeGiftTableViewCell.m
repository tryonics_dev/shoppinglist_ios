//
//  ProductDetailFreeGiftTableViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/10/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ProductDetailFreeGiftTableViewCell.h"

@implementation ProductDetailFreeGiftTableViewCell
@synthesize baseView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.baseView.layer.cornerRadius = VIEW_CORNER_RADIUS ;
    self.baseView.layer.borderWidth = 1.0f;
    self.baseView.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.baseView.layer.masksToBounds = YES;
    
    self.lblTitleOne.text = [[LanguageManager sharedManager] languageStringForKey:@"Congratulations! You have a gift"];
    self.lblTitleTwo.text = [[LanguageManager sharedManager] languageStringForKey:@"Click here to view your gift"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}
@end

//
//  ProductDetailTitleTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/10/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KASlideShow.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductDetailTitleTableViewCell : UITableViewCell<KASlideShowDelegate,KASlideShowDataSource>
{
     LanguageManager *languageManager;
     NSMutableArray *arrProductImages;
}
@property (weak,nonatomic) IBOutlet KASlideShow * slideshow;
@property (weak ,nonatomic) IBOutlet UIView *viewSlideShow;
@property (weak,nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak,nonatomic) IBOutlet UILabel *lblTitle;
@property (weak,nonatomic) IBOutlet UILabel *lblOldPrice,*lblPrice,*lblNoOfPacksTitle;
@property (weak,nonatomic) IBOutlet UIButton *btnUp,*btnDown;
@property (weak,nonatomic) IBOutlet UITextField *lblItemCount;

-(void)loadProductImages:(NSArray *)arrImages image:(NSString *)image;

@end

NS_ASSUME_NONNULL_END

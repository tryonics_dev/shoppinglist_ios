//
//  ProductCollectionViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductCollectionViewCell : UICollectionViewCell
{
    
}
@property (weak,nonatomic)IBOutlet UIImageView *imgThumb,*imgGiftIcon;
@property (weak,nonatomic)IBOutlet UILabel *lblTitle;
@property (weak,nonatomic)IBOutlet UILabel *lblOldPrice,*lblPrice;
@property (weak,nonatomic)IBOutlet UIButton *btnAddProduct;
@property (nonatomic) NSInteger product_id;
@end

NS_ASSUME_NONNULL_END

//
//  ProductDetailTitleTableViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/10/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ProductDetailTitleTableViewCell.h"

@implementation ProductDetailTitleTableViewCell

@synthesize viewSlideShow,slideshow,pageControl;

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
    [self configureProductImages];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}
#pragma mark - KASlideShow datasource

- (NSObject *)slideShow:(KASlideShow *)slideShow objectAtIndex:(NSUInteger)index
{
    return arrProductImages[index];
}

- (NSUInteger)slideShowImagesNumber:(KASlideShow *)slideShow
{
    return arrProductImages.count;
}
#pragma mark - KASlideShow delegate

- (void) slideShowWillShowNext:(KASlideShow *)slideShow
{
}
- (void) slideShowWillShowPrevious:(KASlideShow *)slideShow
{
    
}
- (void) slideShowDidShowNext:(KASlideShow *)slideShow
{
    pageControl.currentPage=slideShow.currentIndex;
}

-(void) slideShowDidShowPrevious:(KASlideShow *)slideShow
{
    pageControl.currentPage=slideShow.currentIndex;
}
-(void )slideShowDidTap:(KASlideShow *)slideShow
{
    
}

#pragma mark - Userdefine methods

-(void)initUI
{
    viewSlideShow.backgroundColor = [UIColor clearColor];
    viewSlideShow.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    viewSlideShow.layer.borderWidth = 1.0;
    //viewSlideShow.layer.cornerRadius = VIEW_CORNER_RADIUS;
    viewSlideShow.layer.masksToBounds = YES;
    languageManager = [LanguageManager sharedManager];
    self.lblNoOfPacksTitle.text = [languageManager languageStringForKey:@"No of Packs"];
}
- (void)configureProductImages
{
    arrProductImages = [[NSMutableArray alloc] init];
    
    [slideshow setTransitionType:KASlideShowTransitionSlideHorizontal];
    slideshow.gestureRecognizers = nil;
    [slideshow addGesture:KASlideShowGestureSwipe];
    slideshow.delegate = self;
    slideshow.datasource = self;
    
    pageControl.currentPageIndicatorTintColor = APP_THEME_LIGHT_GREEN;
    pageControl.pageIndicatorTintColor = [UIColor groupTableViewBackgroundColor];
    
}
-(void)loadProductImages:(NSArray *)arrImages image:(NSString *)image
{
    for (NSString *imageURL  in arrImages) {
        [arrProductImages addObject:[NSURL URLWithString:imageURL]];
    }
    [arrProductImages addObject:[NSURL URLWithString:image ]];
    pageControl.numberOfPages = arrProductImages.count;
    dispatch_async(dispatch_get_main_queue(), ^{
         [self->slideshow start];
    });
   
    
}
@end

//
//  ProductDetailFreeGiftTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/10/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductDetailFreeGiftTableViewCell : UITableViewCell

@property(weak, nonatomic)IBOutlet UIView *baseView;
@property(weak, nonatomic)IBOutlet UIImageView *imgGift;
@property(weak,nonatomic)IBOutlet UILabel *lblCondition , *lblTitleOne,*lblTitleTwo;
@property (weak,nonatomic)IBOutlet UIButton *btnPopUp;
@end

NS_ASSUME_NONNULL_END

//
//  ProductDetailDescriptionTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/10/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductDetailDescriptionTableViewCell : UITableViewCell
{
    
}
@property(weak,nonatomic)IBOutlet UILabel *lblDescription;
@end

NS_ASSUME_NONNULL_END

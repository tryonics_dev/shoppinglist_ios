//
//  CategoryCollectionViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CategoryCollectionViewCell : UICollectionViewCell
{
    
}
@property (weak , nonatomic) IBOutlet UIImageView * imgCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

NS_ASSUME_NONNULL_END

//
//  ProfileInfoTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/24/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileInfoTableViewCell : UITableViewCell
{
    
}
@property(nonatomic,weak)IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UIImageView *imgLogo;
@end

NS_ASSUME_NONNULL_END

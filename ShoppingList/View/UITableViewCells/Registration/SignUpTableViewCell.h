//
//  SignUpTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/23/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignUpTableViewCell : UITableViewCell
{
     LanguageManager *languageManager;
}
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *constrainTop;
@property (weak , nonatomic)IBOutlet UILabel *lblTitle,
*lblTitleFirstName,
*lblErrorFirstName,
*lblTitleLastName,
*lblErrorLastName,
*lblTitleEmail,
*lblErrorEmail,
*lblTitleTelephone,
*lblVerifiedTelephone,
*lblTitlePassword,
*lblErrorPassword,
*lblTitleConfirmPassword,
*lblErrorConfirmPassword;
@property (weak,nonatomic)IBOutlet UITextField *txtFirstName,
*txtLastName,
*txtEmail,
*txtTelephone,
*txtPassword,
*txtConfirmPassword;
@property (weak , nonatomic)IBOutlet UIButton *btnSignUp,*btnViewPassword,*btnViewConfirmPassword;
@property(weak,nonatomic)IBOutlet UIView *viewBackground;
@end


NS_ASSUME_NONNULL_END

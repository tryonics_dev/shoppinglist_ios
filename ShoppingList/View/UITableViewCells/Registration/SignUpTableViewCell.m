//
//  SignUpTableViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/23/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "SignUpTableViewCell.h"

@implementation SignUpTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    languageManager = [LanguageManager sharedManager];
    if(IS_IPHONE_5)
    {
        self.constrainTop.constant  =5;
    }
    [self.lblTitle shoppingTitleStyle:[languageManager languageStringForKey:@"Sign Up"]];
    [self.btnSignUp shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Sign Up"]];
    
    [self.lblTitleFirstName shoppingTextLableStyle:[languageManager languageStringForKey:@"First Name"]];
    [self.txtFirstName shoppingListGreenFullRoundedCorner];
    [self.lblErrorFirstName shoppingTextErrorStyle:@""];
    
    [self.lblTitleLastName shoppingTextLableStyle:[languageManager languageStringForKey:@"Last Name"]];
    [self.txtLastName shoppingListGreenFullRoundedCorner];
    [self.lblErrorLastName shoppingTextErrorStyle:@""];
    
    [self.lblTitleEmail shoppingTextLableStyle:[languageManager languageStringForKey:@"Email"]];
    [self.txtEmail shoppingListGreenFullRoundedCorner];
    [self.lblErrorEmail shoppingTextErrorStyle:@""];
    
    [self.lblTitleTelephone shoppingTextLableStyle:[languageManager languageStringForKey:@"Telephone"]];
    [self.txtTelephone shoppingListGreenFullRoundedCorner];
    [self.lblVerifiedTelephone setText:[languageManager languageStringForKey:@"Verified"]];

    [self.lblTitlePassword shoppingTextLableStyle:[languageManager languageStringForKey:@"Password"]];
    [self.txtPassword shoppingListGreenFullRoundedCorner];
    [self.lblErrorPassword shoppingTextErrorStyle:@""];
    
    [self.lblTitleConfirmPassword shoppingTextLableStyle:[languageManager languageStringForKey:@"Confirm Password"]];
    [self.txtConfirmPassword shoppingListGreenFullRoundedCorner];
    [self.lblErrorConfirmPassword shoppingTextErrorStyle:@""];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)buttonClick:(id)sender
{
    if(sender == self.btnViewPassword)
    {
        self.txtPassword.secureTextEntry = !self.txtPassword.secureTextEntry;
        if(self.txtPassword.secureTextEntry)
            [self.btnViewPassword setTitle:[languageManager languageStringForKey:@"View"] forState:UIControlStateNormal];
        else
             [self.btnViewPassword setTitle:[languageManager languageStringForKey:@"Hide"] forState:UIControlStateNormal];
    }
    else if ( sender == self.btnViewConfirmPassword)
    {
        self.txtConfirmPassword.secureTextEntry =  !self.txtConfirmPassword.secureTextEntry;
        if(self.txtConfirmPassword.secureTextEntry)
            [self.btnViewConfirmPassword setTitle:[languageManager languageStringForKey:@"View"] forState:UIControlStateNormal];
        else
            [self.btnViewConfirmPassword setTitle:[languageManager languageStringForKey:@"Hide"] forState:UIControlStateNormal];
    }
}
@end

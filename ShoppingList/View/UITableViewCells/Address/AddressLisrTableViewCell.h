//
//  AddressLisrTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/30/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddressLisrTableViewCell : UITableViewCell

@property(nonatomic,weak)IBOutlet UILabel *lblName;
@property(nonatomic,weak)IBOutlet UITextView *txtAddress;
@property(nonatomic,weak)IBOutlet UIImageView *imgDefault;
@property(nonatomic,weak)IBOutlet UIView *baseView;
@end

NS_ASSUME_NONNULL_END

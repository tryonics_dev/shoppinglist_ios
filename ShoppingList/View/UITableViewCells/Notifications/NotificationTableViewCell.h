//
//  NotificationTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/18/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationTableViewCell : UITableViewCell

@property (weak, nonatomic)IBOutlet UIView *baseView;
@property (weak, nonatomic)IBOutlet UILabel *lblTitle,*lblDate,*lblProductTitle;
@property (weak,nonatomic)IBOutlet UIImageView *imgProduct;
@property (weak,nonatomic)IBOutlet UITextView *txtDescription;

@end

NS_ASSUME_NONNULL_END

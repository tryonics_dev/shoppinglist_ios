//
//  NotificationTableViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/18/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "NotificationTableViewCell.h"

@implementation NotificationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    self.baseView.layer.cornerRadius = VIEW_CORNER_RADIUS ;
    self.baseView.layer.borderWidth = 1.0f;
    self.baseView.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.baseView.layer.masksToBounds = YES;
    
    self.imgProduct.layer.cornerRadius = VIEW_CORNER_RADIUS ;
    self.baseView.layer.masksToBounds = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

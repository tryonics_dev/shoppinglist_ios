//
//  HomeTabTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KASlideShow.h"
#import "CategoriesModel.h"
#import "FeaturedProductListModel.h"
#import "CategoryCollectionViewCell.h"
#import "ProductCollectionViewCell.h"
#import "CategoryViewController.h"
#import "ProductSearchViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeTabTableViewCell : UITableViewCell<KASlideShowDelegate,KASlideShowDataSource,UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSMutableArray * sliderDatasource;
    NSArray *arrBanners;
    LanguageManager *languageManager;
    NetworkManager *networkManager;
}
@property (nonatomic ,weak)IBOutlet UISearchBar *searchBar;
@property (weak,nonatomic) IBOutlet KASlideShow * slideshow;
@property (weak,nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak,nonatomic) IBOutlet UIButton *btnAllCategory,*btnSpecialOffer,*btnLatestProducts;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *leftButtonSpace,*rightButtonSpace,*btnAllCategoryHeight,*btnAllCategoryWidth,*btnSpecialOfferHeight,*btnSpecialOfferWidth,*btnLatestProductsHeight,*btnLatestProductsWidth,*buttonViewHeight,*titleLeftSpace,*titleRightSpace,*titleTopSpace,*btnSearch;
@property (weak,nonatomic) IBOutlet UILabel *lblTitleAllCategory,*lblTitleSpecialOffer,*lblTitleLatestProducts,*lblTitleTopCategory,*lblTitleProducts;
@property (weak,nonatomic)IBOutlet UICollectionView *collectionViewTopCategories,*collectionViewFeaturedProducts;
@property (nonatomic ,weak)IBOutlet UIView *viewTop;

@property (strong, nonatomic) CategoriesModel *categories;
@property (strong, nonatomic)FeaturedProductListModel *productList;
@property (strong, nonatomic) UINavigationController *navController;
@end

NS_ASSUME_NONNULL_END

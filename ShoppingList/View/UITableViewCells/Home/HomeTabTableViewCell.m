//
//  HomeTabTableViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "HomeTabTableViewCell.h"

@implementation HomeTabTableViewCell

@synthesize pageControl,slideshow;
@synthesize btnAllCategory,btnSpecialOffer,btnLatestProducts;
@synthesize btnAllCategoryWidth,btnAllCategoryHeight,btnSpecialOfferWidth,btnSpecialOfferHeight,btnLatestProductsWidth,btnLatestProductsHeight,leftButtonSpace,rightButtonSpace,buttonViewHeight,titleLeftSpace,titleRightSpace,titleTopSpace,btnSearch;
@synthesize lblTitleAllCategory,lblTitleSpecialOffer,lblTitleLatestProducts;
@synthesize collectionViewTopCategories,collectionViewFeaturedProducts;
@synthesize categories,navController,viewTop;

- (void)awakeFromNib {
    [super awakeFromNib];
    networkManager = [NetworkManager sharedManager];
    [self initUI];
    [self loadTopCategories];
    [self loadFeaturedProducts];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - KASlideShow datasource

- (NSObject *)slideShow:(KASlideShow *)slideShow objectAtIndex:(NSUInteger)index
{
    return sliderDatasource[index];
}

- (NSUInteger)slideShowImagesNumber:(KASlideShow *)slideShow
{
    return sliderDatasource.count;
}

#pragma mark - KASlideShow delegate

- (void) slideShowWillShowNext:(KASlideShow *)slideShow
{
}
- (void) slideShowWillShowPrevious:(KASlideShow *)slideShow
{

}
- (void) slideShowDidShowNext:(KASlideShow *)slideShow
{
    pageControl.currentPage=slideShow.currentIndex;
}

-(void) slideShowDidShowPrevious:(KASlideShow *)slideShow
{
    pageControl.currentPage=slideShow.currentIndex;
}
-(void )slideShowDidTap:(KASlideShow *)slideShow
{
    
}
#pragma mark - CollectionView Delegate/DataSource


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(collectionView == self.collectionViewTopCategories)
    {
        if(self.categories !=nil)
            return self.categories.data.count;
    }
    else{
        if(self.productList !=nil)
            return self.productList.products.count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView == self.collectionViewTopCategories){
        [collectionView registerNib:[UINib nibWithNibName:@"CategoryCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CategoryCollectionViewCell"];
        
        CategoryCollectionViewCell *cell;
        if(cell == nil){
            cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCollectionViewCell" forIndexPath:indexPath];
        }
        
        CategoryModel *category = [self.categories.data objectAtIndex:indexPath.row];
        cell.lblTitle.text = category.name;
        
        [cell.imgCategory setShowActivityIndicatorView:YES];
        [cell.imgCategory sd_setImageWithURL:[NSURL URLWithString:category.image]
                           placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                                    options:SDWebImageRefreshCached];
       
        return cell;
    }
    else
    {
        [collectionView registerNib:[UINib nibWithNibName:@"ProductCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ProductCollectionViewCell"];
        
        ProductCollectionViewCell *cell;
        if(cell == nil){
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductCollectionViewCell" forIndexPath:indexPath];
        }
        
        FeaturedProductModel *product = [self.productList.products objectAtIndex:indexPath.row];
        cell.lblTitle.text = product.name;
        [cell.imgGiftIcon setHidden:!product.gift_flag];
        [cell.imgThumb setShowActivityIndicatorView:YES];
        [cell.imgThumb sd_setImageWithURL:[NSURL URLWithString:product.thumb]
                            placeholderImage:[UIImage imageNamed:@"imgPlaceHolder"]
                                     options:SDWebImageRefreshCached];
        
        if(product.special > 0)
        {
            [cell.lblOldPrice setHidden:NO];
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:product.price_formated];
            [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                    value:@1
                                    range:NSMakeRange(0, [attributeString length])];
            cell.lblOldPrice.attributedText =attributeString;
            
            cell.lblPrice.text = product.special_formated;
        }
        else
        {
            [cell.lblOldPrice setHidden:YES];
            cell.lblPrice.text = product.price_formated;
        }
        cell.product_id = product.product_id;
        return cell;
    }
}
-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == collectionViewFeaturedProducts)
    {
        FeaturedProductModel *product = [self.productList.products objectAtIndex:indexPath.row];
        ProductDetailViewController *ctrlProductDetails = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
        ctrlProductDetails.productId = product.product_id;
        [self.navController pushViewController:ctrlProductDetails animated:YES];
    }
    else if(collectionView == collectionViewTopCategories)
    {
        CategoryModel *category = [self.categories.data objectAtIndex:indexPath.row];
//        SubCategoriesViewController *ctrlSubCategories = [[SubCategoriesViewController alloc] initWithNibName:@"SubCategoriesViewController" bundle:nil];
//        ctrlSubCategories.parentCategoryName = category.name;
//        ctrlSubCategories.parentCategoryId = category.category_id;
//        [self.navController pushViewController:ctrlSubCategories animated:YES];
        
        ProductListViewController *ctrlProductList = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil];
        ctrlProductList.selectedCategoryName = category.name;
        ctrlProductList.selectedCategoryId = category.category_id;
       // ctrlProductList.arrFilterGroups = category.filters.filter_groups;
        [self.navController pushViewController:ctrlProductList animated:YES];
    }
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == self.collectionViewTopCategories)
        return CGSizeMake(140, 140);
    else
        return CGSizeMake(140, 200);
}
#pragma mar - Action methods

-(IBAction)buttonClick:(id)sender
{
    if(sender == btnAllCategory)
    {
        CategoryViewController *ctrlCategory = [[CategoryViewController alloc] initWithNibName:@"CategoryViewController" bundle:nil];
        [self.navController pushViewController:ctrlCategory animated:YES];
    }
    else if (sender == btnSpecialOffer)
    {
        ProductListViewController *ctrlProductList = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil];
        ctrlProductList.selectedCategoryName =[languageManager languageStringForKey:@"Special Offers"];
        ctrlProductList.isSpecials = YES;
        [self.navController pushViewController:ctrlProductList animated:YES];
    }
    else if (sender == btnLatestProducts)
    {
        ProductListViewController *ctrlProductList = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil];
        ctrlProductList.selectedCategoryName =[languageManager languageStringForKey:@"Latest Products"];
        ctrlProductList.isLatest = YES;
        [self.navController pushViewController:ctrlProductList animated:YES];
    }
    else if (sender == btnSearch)
    {
        ProductSearchViewController *ctrlSearch = [[ProductSearchViewController alloc] initWithNibName:@"ProductSearchViewController" bundle:nil];
        [self.navController pushViewController:ctrlSearch animated:(BOOL)YES];
    }
}

#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager =  [LanguageManager sharedManager];
    self.slideshow.layer.cornerRadius = 8;
    self.slideshow.layer.masksToBounds = true;
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, SCREEN_WIDTH, self.viewTop.bounds.size.height) byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){SCREEN_WIDTH/2, SCREEN_WIDTH/2}].CGPath;
    self.viewTop.layer.mask = maskLayer;
   
    self.viewTop.backgroundColor = [myAppDelegate colourBackground];
    
    if(IS_IPHONE_5)
    {
        leftButtonSpace.constant = 40;
        rightButtonSpace.constant = 40;
        
        btnLatestProductsHeight.constant = 60;
        btnLatestProductsWidth.constant = 60;
        
        btnSpecialOfferHeight.constant = 60;
        btnSpecialOfferWidth.constant = 60;
        
        btnAllCategoryHeight.constant = 60;
        btnAllCategoryWidth.constant = 60;
        
        buttonViewHeight.constant = 70;
        
        titleLeftSpace.constant = 16;
        titleRightSpace.constant = 4;
    }
    else
    {
        leftButtonSpace.constant = 45;
        rightButtonSpace.constant = 45;
        
        titleLeftSpace.constant = 40;
        titleRightSpace.constant = 32;
        titleTopSpace.constant = 5;
        
        [lblTitleLatestProducts setFont:[UIFont systemFontOfSize:14]];
        [lblTitleSpecialOffer setFont:[UIFont systemFontOfSize:14]];
        [lblTitleAllCategory setFont:[UIFont systemFontOfSize:14]];
    }
    
    self.lblTitleAllCategory.text = [languageManager languageStringForKey:@"All Categories"];
    self.lblTitleSpecialOffer.text = [languageManager languageStringForKey:@"Special Offers"];
    self.lblTitleLatestProducts.text = [languageManager languageStringForKey:@"Latest Products"];
    
    self.lblTitleTopCategory.text = [languageManager languageStringForKey:@"Top categories"];
    self.lblTitleProducts.text = [languageManager languageStringForKey:@"Featured products"];
    
    self.searchBar.placeholder = [languageManager languageStringForKey:@"Search in ShoppingList"];
}
- (void)configureBanner
{
    slideshow.datasource = self;
    slideshow.delegate = self;
    [slideshow setTransitionType:KASlideShowTransitionSlideHorizontal];
    slideshow.gestureRecognizers = nil;
    [slideshow addGesture:KASlideShowGestureSwipe];
    
    pageControl.currentPageIndicatorTintColor = APP_THEME_LIGHT_GREEN;
    pageControl.pageIndicatorTintColor = [UIColor groupTableViewBackgroundColor];
    
}

#pragma mark - Network calls

-(void)loadTopCategories
{
    [networkManager getTopCategories:^(id  _Nonnull responseData) {
        if([[responseData allKeys] containsObject:@"data"] && [responseData objectForKey:@"data"] != nil){
            NSError *error;
            self.categories =[[CategoriesModel alloc] initWithDictionary:responseData error:&error];
            [self.collectionViewTopCategories reloadData];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
    }];
}
-(void)loadFeaturedProducts
{
    [networkManager getTopFeaturedProducts:^(id  _Nonnull responseData) {
        if([[responseData allKeys] containsObject:@"data"] && [responseData objectForKey:@"data"] !=nil){
            NSError *error;
            if([[responseData objectForKey:@"data"] isKindOfClass:[NSArray class]] && [[responseData objectForKey:@"data"] count]>0){
                self.productList = [[FeaturedProductListModel alloc] initWithDictionary:[responseData objectForKey:@"data"][0] error:&error];
                [self.collectionViewFeaturedProducts reloadData];
            }
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
    }];
}
@end

//
//  HomeBannerTableViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/20/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "HomeBannerTableViewCell.h"

@implementation HomeBannerTableViewCell
@synthesize slideshow,viewTop,searchBar,pageControl;
- (void)awakeFromNib {
    [super awakeFromNib];
    networkManager = [NetworkManager sharedManager];
    [self initUI];
    [self configureBanner];
    [self loadBanner];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - KASlideShow datasource

- (NSObject *)slideShow:(KASlideShow *)slideShow objectAtIndex:(NSUInteger)index
{
    return sliderDatasource[index];
}

- (NSUInteger)slideShowImagesNumber:(KASlideShow *)slideShow
{
    return sliderDatasource.count;
}

#pragma mark - KASlideShow delegate

- (void) slideShowWillShowNext:(KASlideShow *)slideShow
{
}
- (void) slideShowWillShowPrevious:(KASlideShow *)slideShow
{
    
}
- (void) slideShowDidShowNext:(KASlideShow *)slideShow
{
    pageControl.currentPage=slideShow.currentIndex;
}

-(void) slideShowDidShowPrevious:(KASlideShow *)slideShow
{
    pageControl.currentPage=slideShow.currentIndex;
}
-(void)slideShowDidTap:(KASlideShow *)slideShow
{
   // @"https://shoppinglist.lk/image/catalog/slide-1.jpg?product_id=23";
    
    NSString *urlString =[NSString stringWithFormat:@"%@", [arrData[slideShow.currentIndex] objectForKey:@"link"]];
    if([urlString rangeOfString:@"product_id"].location != NSNotFound){
        NSArray* arrURL = [urlString componentsSeparatedByString:@"product_id="];
        if(arrURL != nil)
        {
            ProductDetailViewController *ctrlProductDetails = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
            ctrlProductDetails.productId = [arrURL[1] integerValue];
            [self.navController pushViewController:ctrlProductDetails animated:YES];
        }
    }
}
#pragma mar - Action methods

-(IBAction)buttonClick:(id)sender
{
     if (sender == self.btnSearch)
    {
        ProductSearchViewController *ctrlSearch = [[ProductSearchViewController alloc] initWithNibName:@"ProductSearchViewController" bundle:nil];
        [self.navController pushViewController:ctrlSearch animated:(BOOL)YES];
    }
}

#pragma mark - Userdefine methods

-(void)initUI
{
    languageManager =  [LanguageManager sharedManager];
    self.slideshow.layer.cornerRadius = 8;
    self.slideshow.layer.masksToBounds = true;
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, SCREEN_WIDTH, self.viewTop.bounds.size.height) byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){SCREEN_WIDTH/2, SCREEN_WIDTH/2}].CGPath;
    self.viewTop.layer.mask = maskLayer;
    
    self.viewTop.backgroundColor = [myAppDelegate colourBackground];
    self.searchBar.placeholder = [languageManager languageStringForKey:@"Search in ShoppingList"];
   // self.searchBar.layer.cornerRadius = 10;
   // self.searchBar.clipsToBounds = YES;
    if (@available(iOS 13.0, *)) {
           searchBar.searchTextField.backgroundColor = [UIColor whiteColor];
    }
}
- (void)configureBanner
{
    slideshow.datasource = self;
    slideshow.delegate = self;
    [slideshow setTransitionType:KASlideShowTransitionSlideHorizontal];
    slideshow.gestureRecognizers = nil;
    [slideshow addGesture:KASlideShowGestureAll];
    
    pageControl.currentPageIndicatorTintColor = APP_THEME_LIGHT_GREEN;
    pageControl.pageIndicatorTintColor = [UIColor groupTableViewBackgroundColor];
    
}
#pragma mark - Network calls

-(void)loadBanner
{
    [networkManager getHomeScreenBanners:^(id  _Nonnull responseData) {
        if([[responseData allKeys] containsObject:@"data"] && [responseData objectForKey:@"data"] != nil){
            self->arrImageURL=[[NSMutableArray alloc] init];
            for (NSDictionary *dicImages  in [responseData objectForKey:@"data"]) {
                [self->arrImageURL addObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[Utility validateURLString:[dicImages objectForKey:@"image"]]]]];
            }
            self->arrData = [responseData objectForKey:@"data"];
            self->sliderDatasource = self->arrImageURL ;
            self->pageControl.numberOfPages =self->arrImageURL.count;
            [self->slideshow start];
        }
    } failure:^(NSString * _Nonnull failureReason, NSInteger statusCode) {
        
    }];
}

@end


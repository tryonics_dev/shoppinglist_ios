//
//  HomeBannerTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/20/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KASlideShow.h"
#import "ProductSearchViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeBannerTableViewCell : UITableViewCell<KASlideShowDelegate,KASlideShowDataSource>
{
    NSMutableArray * sliderDatasource;
    NSArray *arrBanners;
    LanguageManager *languageManager;
    NetworkManager *networkManager;
    NSMutableArray *arrImageURL;
    NSArray *arrData;
}

@property (nonatomic ,weak)IBOutlet UISearchBar *searchBar;
@property (weak,nonatomic) IBOutlet KASlideShow * slideshow;
@property (weak,nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak,nonatomic) IBOutlet UIButton *btnSearch;
@property (nonatomic ,weak)IBOutlet UIView *viewTop;
@property (strong, nonatomic) UINavigationController *navController;

@end

NS_ASSUME_NONNULL_END

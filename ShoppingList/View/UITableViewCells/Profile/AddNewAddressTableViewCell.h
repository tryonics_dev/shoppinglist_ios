//
//  AddNewAddressTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/28/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddNewAddressTableViewCell : UITableViewCell
{
     LanguageManager *languageManager;
}
@property (weak , nonatomic)IBOutlet UILabel 
*lblTitleFirstName,
*lblErrorFirstName,
*lblTitleLastName,
*lblErrorLastName,
*lblErrorCompany,
*lblErrorAddress1,
*lblErrorAddress2,
*lblErrorCity,
*lblErrorPostCode,
*lblErrorClosestLocation,
*lblTitleCompany,
*lblTitleAddress1,
*lblTitleAddress2,
*lblTitleCity,
*lblTitlePostCode,
*lblTitleClosestLocation;

@property (weak,nonatomic)IBOutlet UITextField *txtFirstName,
*txtLastName,*txtCompany,*txtAddress1,*txtAddress2,*txtCity,*txtPostCode,*txtClosestLocation;

@property (weak,nonatomic) IBOutlet UIButton *btnSearch,*btnSave,*btnDefault;
@property(weak,nonatomic)IBOutlet UIView *viewBackground;

@end

NS_ASSUME_NONNULL_END

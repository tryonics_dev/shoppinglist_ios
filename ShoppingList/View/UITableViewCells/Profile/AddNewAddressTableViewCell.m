//
//  AddNewAddressTableViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/28/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "AddNewAddressTableViewCell.h"

@implementation AddNewAddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
     languageManager = [LanguageManager sharedManager];
    [self.lblTitleFirstName shoppingTextLableStyle:[languageManager languageStringForKey:@"First Name"]];
    [self.lblTitleLastName shoppingTextLableStyle:[languageManager languageStringForKey:@"Last Name"]];
     [self.lblTitleCompany shoppingTextLableStyle:[languageManager languageStringForKey:@"Company"]];
     [self.lblTitleAddress1 shoppingTextLableStyle:[languageManager languageStringForKey:@"Address 1"]];
     [self.lblTitleAddress2 shoppingTextLableStyle:[languageManager languageStringForKey:@"Address 2"]];
     [self.lblTitlePostCode shoppingTextLableStyle:[languageManager languageStringForKey:@"Post Code"]];
     [self.lblTitleCity shoppingTextLableStyle:[languageManager languageStringForKey:@"City"]];
     [self.lblTitleClosestLocation shoppingTextLableStyle:[languageManager languageStringForKey:@"Closest Landmark"]];
    
    [self.lblErrorFirstName shoppingTextErrorStyle:@""];
    [self.lblErrorLastName shoppingTextErrorStyle:@""];
    [self.lblErrorCompany shoppingTextErrorStyle:@""];
    [self.lblErrorAddress1 shoppingTextErrorStyle:@""];
    [self.lblErrorAddress2 shoppingTextErrorStyle:@""];
    [self.lblErrorPostCode shoppingTextErrorStyle:@""];
    [self.lblErrorClosestLocation shoppingTextErrorStyle:@""];
    [self.lblErrorCity shoppingTextErrorStyle:@""];
    
    [self.txtFirstName shoppingListGreenFullRoundedCorner];
    [self.txtLastName shoppingListGreenFullRoundedCorner];
    [self.txtCity shoppingListGreenFullRoundedCorner];
    [self.txtCompany shoppingListGreenFullRoundedCorner];
    [self.txtAddress1 shoppingListGreenFullRoundedCorner];
    [self.txtAddress2 shoppingListGreenFullRoundedCorner];
    [self.txtPostCode shoppingListGreenFullRoundedCorner];
    [self.txtClosestLocation shoppingListGreenFullRoundedCorner];
    
    
    [self.btnSave shoppingGreenRoundedCorner:[languageManager languageStringForKey:@"Add"]];
    
    [self.btnSearch shoppingGrayRoundedCorner:[languageManager languageStringForKey:@"Search"]];
    [self.btnDefault setTitle:[NSString stringWithFormat:@"  %@",[languageManager languageStringForKey:@"Default"]] forState:UIControlStateNormal];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

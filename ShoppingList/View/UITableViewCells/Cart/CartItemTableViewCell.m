//
//  CartItemTableViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/15/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "CartItemTableViewCell.h"

@implementation CartItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblTotalTitle.text = [[LanguageManager sharedManager] languageStringForKey:@"Total"];
    
    self.baseView.layer.cornerRadius = VIEW_CORNER_RADIUS ;
    self.baseView.layer.borderWidth = 1.0f;
    self.baseView.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.baseView.layer.masksToBounds = YES;
    
    [self.btnUp shoppingClearGrayBorder:@"+"];
    [self.btnDown shoppingClearGrayBorder:@"-"];
    
    self.baseView.layer.masksToBounds = NO;
    self.baseView.layer.shadowOffset = CGSizeMake(-0.1, 0.1);
    self.baseView.layer.shadowRadius = VIEW_CORNER_RADIUS;
    self.baseView.layer.shadowOpacity = 0.02;
    self.baseView.layer.shadowPath = [UIBezierPath bezierPathWithRect: self.baseView.bounds].CGPath;
    
    self.imgThumb.layer.cornerRadius = VIEW_CORNER_RADIUS;
    self.imgThumb.layer.masksToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  CartItemTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/15/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CartItemTableViewCell : UITableViewCell

@property (weak,nonatomic)IBOutlet UIView *baseView;
@property (weak,nonatomic)IBOutlet UIImageView *imgThumb;
@property (weak,nonatomic)IBOutlet UILabel *lblTitle,*lblUnitPrice,*lblTotal,*lblItemCount,*lblTotalTitle;
@property (weak,nonatomic)IBOutlet UIButton *btnUp,*btnDown;
@end

NS_ASSUME_NONNULL_END

//
//  ReceiptDetailTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReceiptDetailTableViewCell : UITableViewCell

@property(nonatomic,weak)IBOutlet UIView
*viewOrderDetailTitle,
*viewAddressTitle,
*viewShippingAddressTitle,
*viewProductTitle;

@property (nonatomic, weak) IBOutlet UILabel *lblSubTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTotalText;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingCost;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingCostText;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalText;

@property(nonatomic,weak)IBOutlet UILabel
*lblOrderDetailTitle,
*lblOrderId,*lblOrderDate,
*lblBillingAddressTitle,
*lblBillingAddress,
*lblShippingAddressTitle,
*lblShippingAddress,
*lblProductTitle;

@property(nonatomic,weak)IBOutlet UITextView *txtPaymentMethod;
@end

NS_ASSUME_NONNULL_END

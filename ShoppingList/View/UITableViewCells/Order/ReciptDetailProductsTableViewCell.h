//
//  ReciptDetailProductsTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReciptDetailProductsTableViewCell : UITableViewCell

@property(nonatomic,weak)IBOutlet UILabel *lblProduct,*lblUnitPrice,*lblTotal;
@end

NS_ASSUME_NONNULL_END

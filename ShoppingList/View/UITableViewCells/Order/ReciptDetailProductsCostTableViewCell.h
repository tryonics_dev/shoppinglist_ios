//
//  ReciptDetailProductsCostTableViewCell.h
//  ShoppingList
//
//  Created by Dimal Govinnage on 11/18/20.
//  Copyright © 2020 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReciptDetailProductsCostTableViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel *lblCode,*lblValue;
@end

NS_ASSUME_NONNULL_END

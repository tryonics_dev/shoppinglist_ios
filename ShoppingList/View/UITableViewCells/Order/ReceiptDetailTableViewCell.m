//
//  ReceiptDetailTableViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/4/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "ReceiptDetailTableViewCell.h"

@implementation ReceiptDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.viewOrderDetailTitle.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){10.0, 10.}].CGPath;
    self.viewOrderDetailTitle.backgroundColor = [myAppDelegate colourBackground];
    self.viewOrderDetailTitle.layer.mask = maskLayer;
    
    CAShapeLayer * maskLayerBilling = [CAShapeLayer layer];
     maskLayerBilling.path = [UIBezierPath bezierPathWithRoundedRect: self.viewOrderDetailTitle.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){10.0, 10.}].CGPath;
    self.viewAddressTitle.backgroundColor = [myAppDelegate colourBackground];
    self.viewAddressTitle.layer.mask= maskLayerBilling;
    
    CAShapeLayer * maskLayerShipping = [CAShapeLayer layer];
    maskLayerShipping.path = [UIBezierPath bezierPathWithRoundedRect: self.viewOrderDetailTitle.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){10.0, 10.}].CGPath;
    self.viewShippingAddressTitle.backgroundColor = [myAppDelegate colourBackground];
    self.viewShippingAddressTitle.layer.mask= maskLayerShipping;
    
    LanguageManager *languageManager = [LanguageManager sharedManager];
    self.lblOrderDetailTitle.text = [languageManager languageStringForKey:@"Order Details"];
    self.lblBillingAddressTitle.text = [languageManager languageStringForKey:@"Billing Address"];
    self.lblShippingAddressTitle.text = [languageManager languageStringForKey:@"Delivery Address"];
    self.lblProductTitle.text = [languageManager languageStringForKey:@"Product(s)"];
    
    CAShapeLayer * maskLayerProduct = [CAShapeLayer layer];
    maskLayerProduct.path = [UIBezierPath bezierPathWithRoundedRect: self.viewOrderDetailTitle.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){10.0, 10.}].CGPath;
    self.viewProductTitle.backgroundColor = [myAppDelegate colourBackground];
    self.viewProductTitle.layer.mask= maskLayerProduct;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

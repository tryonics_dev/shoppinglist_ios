//
//  OrderListTableViewCell.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/2/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderListTableViewCell : UITableViewCell

@property(nonatomic,weak)IBOutlet UILabel
*lblDate,
*lblStatus,
*lblOrderId,
*lblDeliveredToTitle,
*lblDeliveredTo,
*lblTotal,
*lblProductCount;
@property(nonatomic,weak)IBOutlet UIView *viewBase,*viewDate;
@property(nonatomic,weak)IBOutlet UIButton *btnCancel,*btnReOrder;
@end

NS_ASSUME_NONNULL_END

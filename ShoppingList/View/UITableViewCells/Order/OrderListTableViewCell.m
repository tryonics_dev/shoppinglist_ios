//
//  OrderListTableViewCell.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 9/2/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "OrderListTableViewCell.h"

@implementation OrderListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.viewBase.layer.cornerRadius = VIEW_CORNER_RADIUS ;
    self.viewBase.layer.borderWidth = 2.0f;
    self.viewBase.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.viewBase.layer.masksToBounds = YES;
    self.viewBase.backgroundColor = [UIColor clearColor];
    
    self.lblDeliveredToTitle.text = [[LanguageManager sharedManager] languageStringForKey:@"Delivered To"];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  AppColours.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#ifndef AppColours_h
#define AppColours_h

#define APP_THEME_LIGHT_GREEN [UIColor colorWithRed:28.0/255.0 green:163.0/255.0 blue:11.0/255.0 alpha:1]
#define APP_VIEW_BACKGROUND_CLOUR [UIColor colorWithRed:13.0/255.0 green:156.0/255.0 blue:12.0/255.0 alpha:1]
#define APP_VIEW_BACKGROUND_OFF_WHITE_CLOUR [UIColor colorWithRed:246.0/255.0 green:247.0/255.0 blue:246.0/255.0 alpha:1]
#define APP_DARK_RED_CLOUR [UIColor colorWithRed:246.0/255.0 green:42.0/255.0 blue:39.0/255.0 alpha:1]
#endif /* AppColours_h */

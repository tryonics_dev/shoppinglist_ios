//
//  TabTitleHandler.m
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#import "TabTitleHandler.h"

@implementation TabTitleHandler

-(instancetype)init
{
    self=[super init];
    if(self)
    {
        
    }
    return self;
}
+(void)handleTabTitle:(UITabBarController *)tabBarController selectedIndex:(int )index
{
    tabBarController.tabBar.tintColor = APP_THEME_LIGHT_GREEN;
   
//    if(index == 0)
//    {
        tabBarController.tabBar.items[0].title = [[LanguageManager sharedManager] languageStringForKey:@"Home"];
        tabBarController.tabBar.items[1].title =[[LanguageManager sharedManager] languageStringForKey:@"Orders"];
       tabBarController.tabBar.items[2].title =[[LanguageManager sharedManager] languageStringForKey:@"Cart"];
        tabBarController.tabBar.items[3].title =[[LanguageManager sharedManager] languageStringForKey:@"Notifications"];
       tabBarController.tabBar.items[4].title =[[LanguageManager sharedManager] languageStringForKey:@"Profile"];
    
//
//    }
//    else if(index == 1)
//    {
//        tabBarController.tabBar.items[0].title = @"";
//        tabBarController.tabBar.items[1].title =[[LanguageManager sharedManager] languageStringForKey:@"Orders"];
//        tabBarController.tabBar.items[2].title = @"";
//        tabBarController.tabBar.items[3].title = @"";
//        tabBarController.tabBar.items[4].title = @"";
//    }
//    else if(index == 2)
//    {
//        tabBarController.tabBar.items[0].title = @"";
//        tabBarController.tabBar.items[1].title = @"";
//        tabBarController.tabBar.items[2].title =[[LanguageManager sharedManager] languageStringForKey:@"Cart"];
//        tabBarController.tabBar.items[3].title = @"";
//        tabBarController.tabBar.items[4].title = @"";
//    }
//    else if(index == 3)
//    {
//        tabBarController.tabBar.items[0].title = @"";
//        tabBarController.tabBar.items[1].title = @"";
//        tabBarController.tabBar.items[2].title = @"";
//        tabBarController.tabBar.items[3].title =[[LanguageManager sharedManager] languageStringForKey:@"Notifications"];
//        tabBarController.tabBar.items[4].title = @"";
//    }
//    else if(index == 4)
//    {
//        tabBarController.tabBar.items[0].title = @"";
//        tabBarController.tabBar.items[1].title = @"";
//        tabBarController.tabBar.items[2].title = @"";
//        tabBarController.tabBar.items[3].title = @"";
//        tabBarController.tabBar.items[4].title =[[LanguageManager sharedManager] languageStringForKey:@"Profile"];
//    }
    
}

@end

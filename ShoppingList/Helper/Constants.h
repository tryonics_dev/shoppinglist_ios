//
//  Constants.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define myAppDelegate (AppDelegate *)[[UIApplication sharedApplication] delegate]
#define OS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_ZOOMED (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_X  (IS_IPHONE && SCREEN_HEIGHT == 812.0)
#define IS_IPHONE_XS  (IS_IPHONE && SCREEN_HEIGHT == 812.0)
#define IS_IPHONE_XR (IS_IPHONE && [UIScreen mainScreen].nativeBounds.size.height == 1792)
#define IS_IPHONE_XS_MAX (IS_IPHONE && [UIScreen mainScreen].nativeBounds.size.height == 2688)

#define VIEW_CORNER_RADIUS 12.0f
#define VIEW_SHADOW_OPACITY 0.1f

#define BUTTON_CORNER_RADIUS 20.0f
#define BUTTON_FONT_SIZE 16.0

#define LANGUAGE_CODE_ENGLISH @"EN"
#define LANGUAGE_CODE_SINHALA @"SN"
#define LANGUAGE_CODE_TAMIL @"TA"

#define COLLECTION_VIEW_CELL_SIZE_IPHONE5 CGSizeMake((SCREEN_WIDTH/2)-15, (SCREEN_WIDTH/2)-15)
#define COLLECTION_VIEW_CELL_SIZE CGSizeMake((SCREEN_WIDTH/2)-15, (SCREEN_WIDTH/2)-15)
#define COLLECTION_VIEW_CELL_PRODUCTS_SIZE_IPHONE5 CGSizeMake((SCREEN_WIDTH/2)-15, (SCREEN_WIDTH/2)+40)
#define COLLECTION_VIEW_CELL_PRODUCTS_SIZE CGSizeMake((SCREEN_WIDTH/2)-15, (SCREEN_WIDTH/2)+65)

#define ITEMS_PER_PAGE  10

// keys
#define SELECTED_LANGUAGE_KEY @"selected_language"
#define APP_SESSION_KEY @"app_session"
#define LOGGED_USER_KEY @"logged_user"
#define ORDER_STATUS_UPDATED_NOTIFICATION @"order_status_updated_notification"

#define ORDER_PENDING_STATUS 1
#define ORDER_PREPARE_STATUS 2
#define ORDER_READY_DELIVER_STATUS 3
#define ORDER_DISPATCH_STATUS 4
#define ORDER_ON_ROUTE_STATUS 5
#define ORDER_DELIVERED_STATUS 6
#define ORDER_RETURN_STATUS 7
#define ORDER_CANCEL_STATUS 8
#define ORDER_FAILED_STATUS 9

#endif /* Constants_h */

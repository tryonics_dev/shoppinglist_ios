//
//  TabTitleHandler.h
//  ShoppingList
//
//  Created by Mohamed Roshan on 8/3/19.
//  Copyright © 2019 Mohamed Roshan. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TabTitleHandler : NSObject
{
    
}
-(instancetype)init;
+(void)handleTabTitle:(UITabBarController *)tabBarController selectedIndex:(int )index;
@end

NS_ASSUME_NONNULL_END

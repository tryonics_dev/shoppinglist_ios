//
//  Customer.swift
//  sdk
//
//  Created by Kamal Sampath Upasena on 3/2/18.
//  Copyright © 2018 PayHere. All rights reserved.
//

import Foundation

public class Customer : NSObject {
    
   @objc public var firstName : String?
   @objc public var lastName : String?
   @objc public var email : String?
   @objc public var phone : String?
   @objc public var address : Address?
   @objc public var deliveryAddress : Address?
    
    public override init(){
        
    }
    
}

//
//  Address.swift
//  sdk
//
//  Created by Kamal Sampath Upasena on 3/2/18.
//  Copyright © 2018 PayHere. All rights reserved.
//

import Foundation

public class Address : NSObject{
    
    @objc public var address : String?
    @objc public var city : String?
    @objc public var country : String?
    
    public override init(){}
    
    
}
